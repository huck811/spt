<?php
/**
 * @package		AdsManager
 * @copyright	Copyright (C) 2010-2011 JoomPROD.com. All rights reserved.
 * @license		GNU/GPL
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.application.component.model');
JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsmanager'.DS.'tables');

require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_adsmanager'.DS.'models'.DS.'field.php');
require_once(JPATH_SITE.DS.'components'.DS.'com_adsmanager'.DS.'helpers'.DS.'field.php');
/**
 * @package		Joomla
 * @subpackage	Contact
 */
class AdsmanagerModelContent extends JModel
{
	function getPendingContent($contentid) {
		#__adsmanager_pending_ads WHERE contentid = ".(int)
		$sql = " SELECT a.*,u.username as user,u.name as fullname FROM #__adsmanager_pending_ads as a "
			 . " LEFT JOIN #__users as u ON a.userid = u.id "
			 . " WHERE a.contentid = ".(int)$contentid;
		$this->_db->setQuery($sql);
    	$pending = $this->_db->loadObject();

    	if ($pending == null) 
    		return null;
    		
		$pending->data = json_decode($pending->content);
		
		
		$content = new stdClass();
		$content->id = $contentid;
		$content->userid = $pending->userid;
		foreach($pending->data->fields as $name => $value) {
			$content->$name= $value;	
		}
		$content->user = $pending->user;
		$content->fullname = $pending->fullname;
		
		$content->pending = 1;
		
		$this->_db->setQuery(" SELECT * FROM #__adsmanager_config ");
		$conf = $this->_db->loadObject();
		
		$nbimages = $conf->nb_images;
		if (function_exists("getMaxPaidSystemImages"))
		{
			$nbimages += getMaxPaidSystemImages();
		}
		
		$baseurl = JURI::base();
		
		$content->catsid = array();
		$content->cats = array();
		if (is_array($pending->data->categories)) {
			foreach($pending->data->categories as $cat) {
				$content->catsid[] = $cat;
	    			$content->catid = $cat;
				$category = new stdClass();
				$category->catid = $cat;
				$content->cats[] = $category;
			}
		} else {
		    	$content->catid = $pending->data->categories;
		    	$content->catsid[] = $pending->data->categories;
			$category = new stdClass();
			$category->catid = $cat;
			$content->cats[] = $category;
		} 
		
		$images = array();
		for($i=1;$i < $nbimages + 1;$i++)
		{
			$ext_name = chr(ord('a')+$i-1);
			$pic = JPATH_BASE."/images/com_adsmanager/ads/".$contentid.$ext_name.".jpg";
			if (file_exists($pic)) {
				$images[$pic] = $baseurl."images/com_adsmanager/ads/".$contentid.$ext_name.".jpg";	
			}
		}
		
		foreach($pending->data->delimages as $image) {
			if (isset($images[$image])) {
				unset($images[$image]);
			}
		}
		
		if ($pending->data->paid->delimages) {
	    	foreach($pending->data->paid->delimages as $image) {
		    	if (isset($images[$image])) {
					unset($images[$image]);
				}
			}
		}
		
		$dir = $baseurl."/images/com_adsmanager/ads/tmp/";
			
		foreach($pending->data->images as $image) {
			$images[] = $dir.$image->tmpfilename;
		}
	
		foreach($pending->data->paid->images as $image) {
			$images[] = $dir.$image->tmpfilename;
		}
		$content->images = $images;
		
		if ($pending->data->paid->featured) {
			$content->featured = $pending->data->paid->featured;
		}
		
		if ($pending->data->paid->top) {
			$content->top = $pending->data->paid->top;
		}
		
		if ($pending->data->paid->highlight) {
			$content->highlight = $pending->data->paid->highlight;
		}
		$content->duration = @$pending->data->duration;

		return $content;
	}
	
	
	function getContent($contentid,$onlyPublished = true) {
		if ((JRequest::getInt('pending','') != 0)&&(function_exists("getMaxPaidSystemImages"))) {
			$this->_db->setQuery("SELECT count(*) FROM #__adsmanager_pending_ads WHERE contentid=".$contentid);
			$total = $this->_db->loadResult();
			if ($total > 0) {
				return $this->getPendingContent($contentid);
			}
		}
		
		$sql = "SELECT a.*, p.name as parent, p.id as parentid, c.name as cat, c.id as catid,u.username as user,u.name as fullname ".
			" FROM #__adsmanager_ads as a ".
			" INNER JOIN #__adsmanager_adcat as adcat ON adcat.adid = a.id ".
			" LEFT JOIN #__users as u ON a.userid = u.id ".
			" INNER JOIN #__adsmanager_categories as c ON adcat.catid = c.id ".
			" LEFT JOIN #__adsmanager_categories as p ON c.parent = p.id ";
    
  		$sql .= " WHERE a.id = ".(int)$contentid;
  		
  		if ($onlyPublished == true)
			$sql .= " AND c.published = 1 AND a.published = 1 ";
			
		if (function_exists("updateQuery")) {
			updateQuery($sql);
		}
			
    	$this->_db->setQuery($sql);
    	$contents = $this->_db->loadObjectList();

    	if (count($contents) > 0) {
    		$content = $contents[0];
    		$content->cats = array();
    		$content->catsid = array();
    		foreach($contents as $key => $c) {
    			$cat = new stdClass();
    			$cat->parentid = $c->parentid;
    			$cat->parent = $c->parent;
    			$cat->cat = $c->cat;
    			$cat->catid = $c->catid;
    			$content->cats[] = $cat;
    			$content->catsid[] = $c->catid;
    			$content->catid = $c->catid;
    		}
    		return $content;
    	}
    	else
    		return null;			
    }
    
	function _recurseSearch ($rows,&$list,$catid){
		if(isset($rows))
		{
			foreach($rows as $row) {
				if ($row->parent == $catid)
				{
					$list[]= $row->id;
					$this->_recurseSearch($rows,$list,$row->id);
				} 
			}
		}
	}
    
    function _getSQLFilter($filters){
   		 /* Filters */
    	$search = "";
    	if (isset($filters))
    	{
	    	foreach($filters as $key => $filter)
	    	{
	    		if ($search == "")
	    			$temp = " WHERE ";
	    		else
	    			$temp = " AND ";
	    		switch($key)
	    		{
	    			case 'category':
	    				$catid = $filter;
	    				$this->_db->setQuery( "SELECT c.id, c.name,c.parent ".
						" FROM #__adsmanager_categories as c ".
						 "WHERE c.published = 1 ORDER BY c.parent,c.ordering");
						 
						$listcats = $this->_db->loadObjectList();
						$list[] = $catid;
						$this->_recurseSearch($listcats,$list,$catid);
						$listids = implode(',', $list);
	    				$search .= $temp."c.id IN ($listids) ";break;
	    			case 'user':
	    				$search .= $temp."u.id = ".(int)$filter;break;
	    			case 'username':
	    				$search .= $temp."u.username LIKE '%".$this->_db->getEscaped($filter,true)."%'";break;
	    			case 'publish':
	    				$search .= $temp."a.published = ".(int)$filter;break;
	    			case 'fields':
	    				$search .= $temp.$filter;break;
	    			case 'search':
	    				if (intval($filter) != 0) {
	    					$filter = JString::strtolower($filter);
	    					$id = intval($filter);
	    					$search .= $temp."(a.id = $id OR LOWER(a.ad_headline) LIKE '%".$this->_db->getEscaped($filter,true)."%' OR LOWER(a.ad_text) LIKE '%".$this->_db->getEscaped($filter,true)."%')";
	    				} else {
	    					$filter = JString::strtolower($filter);
	    					$search .= $temp."(LOWER(a.ad_headline) LIKE '%".$this->_db->getEscaped($filter,true)."%' OR LOWER(a.ad_text) LIKE '%".$this->_db->getEscaped($filter,true)."%')";
	    				}
	    				break;
	    		}
	    	}
    	}
    	return $search;
    }
    
	function getContents($filters = null,$limitstart=null,$limit=null,$filter_order=null,$filter_order_Dir=null,$admin=0)
    {
    	$sql = "SELECT a.*, p.name as parent, p.id as parentid, c.name as cat, c.id as catid,u.username as user,u.name as fullname ".
			" FROM #__adsmanager_ads as a ".
			" INNER JOIN #__adsmanager_adcat as adcat ON adcat.adid = a.id ";
		if (COMMUNITY_BUILDER == 1)
    		$sql .=	" LEFT JOIN #__comprofiler as cb ON cb.user_id = a.userid ";
		$sql .=	" LEFT JOIN #__users as u ON a.userid = u.id ".
			" INNER JOIN #__adsmanager_categories as c ON adcat.catid = c.id ".
			" LEFT JOIN #__adsmanager_categories as p ON c.parent = p.id ";
    
  		$sql .= $this->_getSQLFilter($filters);

  		if (function_exists("editPaidAd")) {	
	  		if (JRequest::getInt( "ad_featured",0) == 1) {
				$sql .= " AND adext.featured = 1 ";
			}
	
			if (JRequest::getInt( "ad_highlight",0) == 1) {
				$sql .= " AND adext.highlight = 1 ";
			}
			
	  		if (JRequest::getInt( "ad_top",0) == 1) {
				$sql .= " AND adext.top = 1 ";
			}
  		}

		if (JRequest::getInt( "ad_commentaires",0) == 1) {
			$sql .= " AND a.id IN (SELECT DISTINCT contentid FROM #__adsmanager_reviews) ";
		}

		if (JRequest::getInt( "ad_photos",0) == 1) {
			$sql .= " AND (a.images != '' AND a.images != '[]') ";
		}

		if (COMMUNITY_BUILDER == 1) {
			$usertype = JRequest::getVar( "usertype","");
			if ($usertype != "") {
				$sql .= " AND cb.cb_typeproprietaire = ".$this->_db->Quote($usertype);
			}
   		}
    	
    	$sql .= " GROUP BY a.id ORDER BY $filter_order $filter_order_Dir ";
    	
    	if (($admin == 0)&&(function_exists("updateQueryWithReorder")))
    		updateQueryWithReorder($sql);
    	else if (($admin == 1)&&(function_exists("updateQuery")))
    		updateQuery($sql);

    //	echo $sql;
    		
    	$this->_db->setQuery($sql,$limitstart,$limit);
    	$products = $this->_db->loadObjectList();
    	
		return $products;	
    }
    
	function getNbContents($filters = null)
    {
    	$sql = "SELECT a.id ".
			" FROM #__adsmanager_ads as a ".
			" INNER JOIN #__adsmanager_adcat as adcat ON adcat.adid = a.id ".
			" LEFT JOIN #__users as u ON a.userid = u.id ";
		if (COMMUNITY_BUILDER == 1) 
    		$sql .=	" LEFT JOIN #__comprofiler as cb ON cb.user_id = a.userid ";
		$sql .=	" INNER JOIN #__adsmanager_categories as c ON adcat.catid = c.id ".
			" LEFT JOIN #__adsmanager_categories as p ON c.parent = p.id ";
    
  		/* Filters */
    	$sql .= $this->_getSQLFilter($filters);

    	if (function_exists("editPaidAd")) {
			if (JRequest::getInt( "ad_featured",0) == 1) {
				$sql .= " AND adext.featured = 1 ";
			}
	
			if (JRequest::getInt( "ad_highlight",0) == 1) {
				$sql .= " AND adext.highlight = 1 ";
			}
	
			if (JRequest::getInt( "ad_top",0) == 1) {
				$sql .= " AND adext.top = 1 ";
			}
    	}

		if (JRequest::getInt( "ad_commentaires",0) == 1) {
			$sql .= " AND a.id IN (SELECT DISTINCT contentid FROM #__adsmanager_reviews) ";
		}

		if (JRequest::getInt( "ad_photos",0) == 1) {
			$sql .= " AND (a.images != '' AND a.images != '[]') ";
		}

		$usertype = JRequest::getVar( "usertype","");
		if ($usertype != "") {
			$sql .= " AND cb.cb_typeproprietaire = ".$this->_db->Quote($usertype);
		}

    	$sql .= " GROUP BY a.id";

		if (function_exists("updateQueryWithReorder"))
				updateQueryWithReorder($sql);

    	$this->_db->setQuery($sql);
    	
    	$result = $this->_db->loadObjectList();
    	$nb = count($result);
		return $nb;	
    }
    
	function increaseHits($contentid)
    {
		$sql = "UPDATE #__adsmanager_ads SET views = LAST_INSERT_ID(views+1) WHERE id = ".(int)$contentid;
		$this->_db->setQuery($sql);
		$this->_db->query();
    }
    
    function getLatestContents($nbcontents,$sort_type=0,$catselect="no")
    {
		switch($sort_type)
		{
			/* Popular */
			case 2:
				$order_sql = "ORDER BY a.views DESC,a.date_created DESC ,a.id DESC ";
				break;
				
			/* Random */
			case 1:
				$order_sql = "ORDER BY RAND() ";
				break;
				
			/* Latest */
			case 0: 
			default:
				$order_sql = "ORDER BY a.date_created DESC ,a.id DESC ";
				break;
		}
		
		$cat_query = "";
		switch($catselect)
		{
			case "no";
				break;
			
			case "-1":
				$catid = JRequest::getInt('catid', 0 );
				$this->_db->setQuery( "SELECT c.id, c.name,c.parent ".
								 " FROM #__adsmanager_categories as c ".
								 " WHERE c.published = 1 ORDER BY c.parent,c.ordering");			 
				$listcats = $this->_db->loadObjectList();
				//List	
				$list = array();
				$list[] = $catid;
				$this->_recurseSearch($listcats,$list,$catid);
				$listids = implode(',', $list);
				if (($catid != 0)&&($catid != -1))
				{
					$cat_query = "adcat.catid IN ($listids) AND ";
				}
				break;
			default:
				$cat_query = "adcat.catid = $catselect AND ";
				break;
		}
		
		$sql =  " SELECT a.*,p.id as parentid,p.name as parent,c.id as catid, c.name as cat,u.username as user ".
			    " FROM #__adsmanager_ads as a ".
				" INNER JOIN #__adsmanager_adcat as adcat ON adcat.adid = a.id ".
				" LEFT JOIN #__users as u ON a.userid = u.id ".
				" INNER JOIN #__adsmanager_categories as c ON adcat.catid = c.id ".
				" LEFT JOIN #__adsmanager_categories as p ON c.parent = p.id ".
				" WHERE $cat_query c.published = 1 and a.published = 1 GROUP BY a.id $order_sql LIMIT 0, $nbcontents";

		if (function_exists("updateQuery"))
    		updateQuery($sql);
    		
    	$this->_db->setQuery($sql);
    	
		$contents = $this->_db->loadObjectList();
		return $contents;
    }
	
	function getNbContentsOfUser($userid) {
		$this->_db->setQuery("SELECT count(*) FROM #__adsmanager_ads as a WHERE a.userid =".(int)$userid  );
		$nb = $this->_db-> loadResult();
		return $nb;
	}
    
	function renewContent($contentid,$ad_duration)
	{		
		$this->_db->setQuery( "SELECT expiration_date FROM #__adsmanager_ads WHERE id = ".(int)$contentid);
		$expiration_date = $this->_db->loadResult();
		$time = strtotime($expiration_date);
		if ($time < time())
		{
			$time = time();
		}
		$time = $time + ( $ad_duration * 3600 *24); 
		$newdate = date("Y-m-d",$time);
	
		$this->_db->setQuery( "UPDATE #__adsmanager_ads SET expiration_date = '$newdate', date_created = CURDATE(),recall_mail_sent=0,published=1 WHERE id=".(int)$contentid." and recall_mail_sent = 1");
		$this->_db->query();
	}
	
	function sendExpirationEmail($content,$conf)
	{
		$user = JFactory::getUser($content->userid);
		$uri	= JURI::getInstance();
		$root	= $uri->toString( array('scheme', 'host', 'port'));
		$link = $root.TRoute::_("index.php?option=com_adsmanager&view=expiration&id=".$content->id,false);
		$body = str_replace('{link}',$link,$conf->recall_text);

		return $this->sendMailToUser($conf->recall_subject,$body,$user,$content,$conf);
	}
	
	function manage_expiration($plugins,$conf)
	{
		if ($conf->expiration == 1)
		{
			if ($conf->recall == 1)
			{
				$this->_db->setQuery( "SELECT id,email,ad_headline,userid FROM #__adsmanager_ads WHERE DATE_SUB(expiration_date, INTERVAL ".$conf->recall_time." DAY) < CURDATE() AND recall_mail_sent = 0 AND published = 1");
				$contents = $this->_db->loadObjectList();
				
				if (isset($contents))
				{
					foreach($contents as $content)
					{
						$this->sendExpirationEmail($content,$conf);
					}
				}
				$this->_db->setQuery( "UPDATE #__adsmanager_ads SET recall_mail_sent = 1 WHERE DATE_SUB(expiration_date, INTERVAL ".$conf->recall_time." DAY) < CURDATE() AND recall_mail_sent = 0 AND published = 1");
				$this->_db->query();

				$this->_db->setQuery( "SELECT * FROM #__adsmanager_ads WHERE recall_mail_sent = 1 AND expiration_date < CURDATE() AND published = 1");
				$idsarray = $this->_db->loadObjectList();
			}	
			else
			{		
				$this->_db->setQuery( "SELECT * FROM #__adsmanager_ads WHERE expiration_date < CURDATE() AND published = 1");
				$idsarray = $this->_db->loadObjectList();
			}
			
			if (isset($idsarray) && count($idsarray) > 0) {
				foreach($idsarray as $c)
				{
					$id = $c->id;
					$userid = $c->userid;
					
					switch($conf->after_expiration) {		
						default:
						case "delete":
							$content = JTable::getInstance('contents', 'AdsmanagerTable');
							$content->delete($id,$conf,$plugins);
							break;
							
						case "unpublish":
							$this->_db->setQuery( "UPDATE #__adsmanager_ads SET published=0,recall_mail_sent = 0 WHERE id = $id");
							$this->_db->query();
							break;
							
						case "archive":
							$this->_db->setQuery( "UPDATE #__adsmanager_ads SET published=0,recall_mail_sent = 0 WHERE id = $id");
							$this->_db->query();
							
							$this->_db->setQuery( "DELETE FROM #__adsmanager_adcat WHERE adid =$id");
							$this->_db->query();
							
							$this->_db->setQuery( "INSERT INTO #__adsmanager_adcat (adid,catid) VALUES ($id,$conf->archive_catid)");
							$this->_db->query();
							break;
					}
					
					if ($conf->send_email_on_expiration_to_user == 1) {
						$body = $conf->expiration_text;
						if ($conf->after_expiration == "unpublish") {
							$uri	= JURI::getInstance();
							$root	= $uri->toString( array('scheme', 'host', 'port'));
							$link = $root.TRoute::_("index.php?option=com_adsmanager&view=expiration&id=".$content->id);
							
							$link_text = '<a href="'.$link.'">'.$link.'</a>';
							$body = str_replace('{link}',$link_text,$body);
						}
					}
			
					$user = JFactory::getUser($userid);
					$this->sendMailToUser($conf->expiration_subject,$body,$user,$c,$conf);
				}
			}
		}
		$last_cron_date = date("Ymd");
		$Fnm = JPATH_BASE .'/components/com_adsmanager/cron.php';
	    jimport( 'joomla.filesystem.file' );
	    $content = '<?php $last_cron_date='.$last_cron_date.';?>';
		JFile::write( $Fnm, $content );
	}
	
	function getListOrder($order,&$filter_order,&$filter_order_Dir)
    {
	    if ($order != 0)
		{
			$this->_db->setQuery( "SELECT f.name,f.sort_direction,f.type FROM #__adsmanager_fields AS f WHERE f.fieldid=".(int)$order." AND f.published = 1" );
			$sort = $this->_db->loadObject();
			if (($sort->type == "number")||($sort->type == "price")) {
				$filter_order = "a.".$sort->name." * 1";
				$filter_order_Dir= $sort->sort_direction;
			}
			else {
				$filter_order = "a.".$sort->name;
				$filter_order_Dir= $sort->sort_direction;
			}
		}
		else 
		{
			$filter_order = "a.date_created DESC ,a.id ";
			$filter_order_Dir= "DESC";
		}
		
    }
    
    function prepareMail(&$subject,&$body,$user,$content,$conf) {
		$config	=JFactory::getConfig();
		$from		= $config->getValue('mailfrom');
		$fromname	= $config->getValue('fromname');
		$sitename	= $config->getValue('sitename');
		
		$fieldmodel	    = new AdsmanagerModelField();
		$fields 		= $fieldmodel->getFields();
		$field_values 	= $fieldmodel->getFieldValues();
		$plugins = $fieldmodel->getPlugins();
		$baseurl = JURI::base();
		$field = new JHTMLAdsmanagerField($conf,$field_values,1,$plugins,'',$baseurl,null);
		
		foreach($fields as $f) {
			$fvalue = "";
			if (strpos($subject,"{".$f->name."}") !== false) {
				$fvalue = str_replace(array("<br/>","<br />","<br>"),"",$field->showFieldValue($content,$f));
				$subject = str_replace("{".$f->name."}",$fvalue,$subject);
			}
			if (strpos($body,"{".$f->name."}") !== false) {
				if ($fvalue == "")
					$fvalue = str_replace(array("<br/>","<br />","<br>"),"",$field->showFieldValue($content,$f));
				$body = str_replace("{".$f->name."}",$fvalue,$body);
			}
		}
		$subject = str_replace("{id}",$content->id,$subject);
		$body = str_replace("{id}",$content->id,$body);
		$subject = str_replace("{username}",$user->username,$subject);
		$body = str_replace("{username}",$user->username,$body);
		$subject = str_replace("{name}",$user->name,$subject);
		$body = str_replace("{name}",$user->name,$body);
		
		$subject = str_replace("{sitename}",$sitename,$subject);
		$body = str_replace("{sitename}",$sitename,$body);
		
		$uri	= JURI::getInstance();
		$root	= $uri->toString( array('scheme', 'host', 'port'));
		$link = $root."/".TRoute::_("index.php?option=com_adsmanager&view=details&catid=".$content->catid."&id=".$content->id,false);
		$body = str_replace('{link}',$link,$body);
		
		$subject = str_replace("{expiration_date}",strftime(JText::_('ADSMANAGER_DATE_FORMAT_LC'),strtotime($content->expiration_date)),$subject);
		$body = str_replace("{expiration_date}",strftime(JText::_('ADSMANAGER_DATE_FORMAT_LC'),strtotime($content->expiration_date)),$body);
    }
    
    function updateContentDate($adid) {
    	$this->_db->setQuery( "UPDATE #__adsmanager_ads SET date_created = NOW() WHERE id=".(int)$adid);
    	$this->_db->query();
    }
    
    function sendMailToAdmin($subject,$body,$user,$content,$conf) {
    	$config	=JFactory::getConfig();
		$from		= $config->getValue('mailfrom');
		$fromname	= $config->getValue('fromname');
		$sitename	= $config->getValue('sitename');
		
		$this->prepareMail($subject,$body,$user,$content,$conf);

		if (!JUtility::sendMail($from, $fromname, $from, $subject, $body,1))
		{
			$this->setError(JText::_('ADSMANAGER_ERROR_SENDING_MAIL'));
			return false;
		}
		return true;
    }
    
	function sendMailToUser($subject,$body,$user,$content,$conf) {	
		$config	=JFactory::getConfig();
		$from		= $config->getValue('mailfrom');
		$fromname	= $config->getValue('fromname');
		$sitename	= $config->getValue('sitename');
		
		$content = $this->getContent($content->id,false);
		
		$this->prepareMail($subject,$body,$user,$content,$conf);

		if ($user->email == '') {
			$mail = $content->email;
		} else {
			$mail = $user->email;
		}
		if ($mail != '') {
			if (!JUtility::sendMail($from, $fromname, $mail, $subject, $body,1))
			{
				$this->setError(JText::_('ADSMANAGER_ERROR_SENDING_MAIL'));
				return false;
			}
		}
		return true;
	}
}


