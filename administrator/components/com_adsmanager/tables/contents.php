<?php
/**
 * @package		AdsManager
 * @copyright	Copyright (C) 2010-2011 JoomPROD.com. All rights reserved.
 * @license		GNU/GPL
 */

defined('_JEXEC') or die();

jimport( 'joomla.filesystem.file' );

class AdsmanagerTableContents extends JTable
{
	var $id= null;
	var $userid= null; 
	var $ad_headline=null;
	var $ad_text=null;
	var $email=null;
	var $date_created = null;
	var $expiration_date = null;
	var $recall_mail_sent = null;
	var $metadata_description = null;
	var $metadata_keywords = null;
	var $published = null;
	
	var $errors = null;
	var $data = null;
			
    function __construct(&$db)
    {
    	parent::__construct( '#__adsmanager_ads', 'id', $db );
    }
    
	function replaceBannedWords($text,$bannedwords,$replaceword) {
    	foreach($bannedwords as $bannedword) {
    		$text = str_replace($bannedword,$replaceword,$text);
    	}
    	return $text;
    }

	function getErrors() {
    	return $this->errors;
    }
    
	function getData() {
    	return $this->data;
    }
    
	function bindContent($post,$files,$conf,$model,$plugins)
    {
    	$app = JFactory::getApplication();
    	
    	$this->bind($post);
    	
    	if ($this->id == 0) {
    		$this->new_ad = true;
    	}
    	
    	$this->data = array();
    	$this->errors = array();
    			
		if (function_exists("getMaxCats"))
			$maxcats = getMaxCats($conf->nbcats);
		else
			$maxcats = $conf->nbcats;
			
		if ($maxcats > 1)
		{
			$selected_cats = $post["selected_cats"];
			if (count($selected_cats) > $maxcats)
			{
				$selected_cats = array_slice ($selected_cats, 0, $maxcats);
			}
			$this->data['categories'] = $selected_cats;
		}
		else
		{
			$category = $post["category"];
			$this->data['categories'] = array();
			$this->data['categories'][0] = intval($category);
		}
			
		//get fields
		$this->_db->setQuery( "SELECT * FROM #__adsmanager_fields WHERE published = 1");
		$fields = $this->_db->loadObjectList();
		if ($this->_db -> getErrorNum()) {
			$this->errors[] = $this->_db -> stderr();
			return false;
		}	
		
		$query = "UPDATE #__adsmanager_ads ";
		
		$bannedwords = str_replace("\r","",$conf->bannedwords);
		$bannedwords = explode("\n",$bannedwords);
		$replaceword = $conf->replaceword;
		
		$data['fields'] = array();
		foreach($fields as $field)
		{ 	
			if ($field->type == "multiselect")
			{	
				$value = JRequest::getVar($field->name, array());
				$this->data['fields'][$field->name] = $value;
			}
			else if (($field->type == "multicheckbox")||($field->type == "multicheckboximage"))
			{
				$value = $value = JRequest::getVar($field->name, array());
				$this->data['fields'][$field->name] = $value;
			}
			else if ($field->type == "file")
			{
				if (isset($files[$field->name]) and !$files[$field->name]['error'] ) {
					jimport( 'joomla.filesystem.file' );
				
					$filename = $files[$field->name]['name'];
					
					$extension = JFile::getExt($filename);
					$name = md5(rand(1,100000).$filename);
					
					if (strpos($extension,"php") !== false) {
						$extension = 'txt';
					}
					$filename = $name.".".$extension;
					
					JFile::move($files[$field->name]['tmp_name'],
								JPATH_SITE."/images/com_adsmanager/files/".$filename);	
										
					$this->data['fields'][$field->name] = $filename;					
				} else {
					continue;
				}
			}
			else if ($field->type == "editor")
			{
				$value = JRequest::getVar($field->name, '', 'post', 'string', JREQUEST_ALLOWHTML);
				$this->data['fields'][$field->name] = $this->replaceBannedWords($value,$bannedwords,$replaceword);
			}
			//Plugins
			else if (isset($plugins[$field->type]))
			{
				$value = $plugins[$field->type]->onFormSave($this,$field);
				if ($value !== null)
					$this->data['fields'][$field->name] = $value;
			}
			else
			{
				$value = JRequest::getVar($field->name, '');
				$this->data['fields'][$field->name] =$this->replaceBannedWords($value,$bannedwords,$replaceword);
			}	
		}
		
		$nbImages = $conf->nb_images;
		$this->data['images']= array();	
		$this->data['delimages']= array();
		
		if ($conf->plupload) { 
			if (!function_exists("savePaidAd")) {
				$free_images = array();
				for($i = 1 ;$i < $nbImages + 1; $i++)
				{	
					$ext_name = chr(ord('a')+$i-1);
					$cb_image = JRequest::getVar("cb_image$i", "" );
					
					// image1 delete
					if ( $cb_image == "delete") {
						$pict = JPATH_SITE."/images/com_adsmanager/ads/".$this->id.$ext_name."_t.jpg";
						if ( file_exists( $pict)) {
							$this->data['delimages'][] = $pict;	
						}
						$pic = JPATH_SITE."/images/com_adsmanager/ads/".$this->id.$ext_name.".jpg";
						if ( file_exists( $pic)) {
							$this->data['delimages'][] = $pic;	
						}
					} 
					$pict = JPATH_SITE."/images/com_adsmanager/ads/".$this->id.$ext_name."_t.jpg";
					if (!file_exists( $pict)) {
						$free_images[] = $ext_name;
					}
				}	

				$uploader_count = JRequest::getInt('uploader_count',0);
				$targetDir = JPATH_SITE.'/tmp/plupload';
				$image_index=0;

				$dir = JPATH_SITE."/images/com_adsmanager/ads/tmp/";

				for($i = 0 ; $i < $uploader_count ; $i++) {
		            $uploader_tmpname = JRequest::getString('uploader_'.$i.'_tmpname',0);
		            $uploader_name = JRequest::getString('uploader_'.$i.'_name',0);
		            $uploader_status = JRequest::getString('uploader_'.$i.'_status',0);

					$tmpfile = date("Ymd")."-".uniqid();
					$thumb_tmpfile = date("Ymd")."-".uniqid();
		                
		            if ($uploader_status == "done") {
		            	if ($free_images[$image_index]) {
		            		 $ext_name = $free_images[$image_index];
		            		 $image_index++;
		            		 try {
								$error = $model->createImageAndThumb($targetDir."/".$uploader_tmpname,
								   							$tmpfile,$thumb_tmpfile,
															$conf->max_width,
															$conf->max_height,
															$conf->max_width_t,
															$conf->max_height_t,
															$conf->tag,
															$dir,
															$uploader_name);
								if ($error != null) {
									$this->errors[] = $error;
								} else {
									$this->data['images'][] = array('tmpfilename' => $tmpfile,'filename' => $ext_name.".jpg");
									$this->data['images'][] = array('tmpfilename' => $thumb_tmpfile,'filename' => $ext_name."_t.jpg");
								}
							} catch (Exception $e) {
								$this->errors[] = $e->getMessage();
							}
		            	} else {
		            		break;
		            	}
		        	}
		  	 	}	  	 	
			}
			
		} 
		else 
		{
			for($i = 1 ;$i < $nbImages + 1; $i++)
			{	
				$ext_name = chr(ord('a')+$i-1);
				if (isset($post["cb_image$i"]))
					$cb_image = $post["cb_image$i"];
				else
					$cb_image = "";
				// image1 delete
				if ( $cb_image == "delete") {
					$pict = JPATH_SITE."/images/com_adsmanager/ads/".$this->id.$ext_name."_t.jpg";
					if ( file_exists( $pict)) {
						$this->data['delimages'][] = $pict;	
					}
					$pic = JPATH_SITE."/images/com_adsmanager/ads/".$this->id.$ext_name.".jpg";
					if ( file_exists( $pic)) {
						$this->data['delimages'][] = $pic;	
					}
				}
									
				if (isset( $files["ad_picture$i"])) {
					if ( $files["ad_picture$i"]['size'] > $conf->max_image_size) {
						$this->errors[] = JText::_('ADSMANAGER_IMAGETOOBIG');
					}
				}
				
				// image1 upload
				if (isset( $files["ad_picture$i"]) and !$files["ad_picture$i"]['error'] ) {
					$dir = JPATH_SITE."/images/com_adsmanager/ads/tmp/";
					$tmpfile = date("Ymd")."-".uniqid();
					$thumb_tmpfile = date("Ymd")."-".uniqid();
					try {
						$error = $model->createImageAndThumb($files["ad_picture$i"]['tmp_name'],$tmpfile,$thumb_tmpfile,
												$conf->max_width,
												$conf->max_height,
												$conf->max_width_t,
												$conf->max_height_t,
												$conf->tag,
												$dir,
												$files["ad_picture$i"]['name']);
					if ($error != null) {
						$this->errors[] = $error;
					} else {
						$this->data['images'][] = array('tmpfilename' => $tmpfile,'filename' => $ext_name.".jpg");
						$this->data['images'][] = array('tmpfilename' => $thumb_tmpfile,'filename' => $ext_name."_t.jpg");
					}
					
					} catch (Exception $e) {
						$this->errors[] = $e->getMessage();
					}
				}
			}
		}

		$app = JFactory::getApplication();
		if (($app->isAdmin() == false)&&($this->id == 0)) {
			$this->date_created = date("Y-m-d H:i:s");
			$delta = $conf->ad_duration;  
			$this->expiration_date = date("Y-m-d H:i:s",time()+($delta*24*3600)); 
			if ($conf->auto_publish == 1)
				$this->published = 1;
			else
				$this->published = 0;
		}
				
		if (count($this->errors) > 0)
			return false;
		else
			return true;
	}

	function savePending() {
    	$row = new JObject();
    	
		if ($this->id == 0) {	
    		$data = new JObject();
    		$data->date_created = $this->date_created;
    		$data->expiration_date = $this->expiration_date;
    		$data->published = false;
			$this->_db->insertObject('#__adsmanager_ads', $data);
			$this->id = (int)$this->_db->insertid();
			
			$this->data['published'] = $this->published;
		}
		
    	$row->contentid = $this->id;
    	$row->userid = $this->userid;
		$row->date = date("Y-m-d H:i:s");
		
		$this->data['metadata_description'] = $this->metadata_description;
		$this->data['metadata_keywords'] = $this->metadata_keywords;
		
		$row->content = json_encode($this->data);
		
		$query = "DELETE FROM #__adsmanager_pending_ads WHERE contentid = ".(int)$row->contentid;
		$this->_db->setQuery($query);
		$this->_db->query();

		//Insert new record.
		$this->_db->insertObject('#__adsmanager_pending_ads', $row);
		
		return $row->contentid;
    }

	function save()
    {
    	$row = new JObject();
    	
    	if ($this->id != 0) {
    		$row->id = $this->id;
    	}
    	
    	$app = JFactory::getApplication();
    	if (($app->isAdmin() == true) ||
    		(($app->isAdmin() == false)&&(@$this->new_ad == true))) {
    		$row->date_created = $this->date_created;
    		$row->expiration_date = $this->expiration_date;
    		$row->published = $this->published;
    	} 
    		
    	$row->userid = $this->userid;
    	
		foreach($this->data['fields'] as $name => $value) {
			if (is_array($value))
				$v = ','.implode(',',$value).',';
			else
				$v = $value;
			$row->$name = $v;
		}
		
		$row->metadata_description = $this->metadata_description;
		$row->metadata_keywords = $this->metadata_keywords;

		//Insert new record.
		if ($this->id == 0) {
			$ret = $this->_db->insertObject('#__adsmanager_ads', $row);
			$contentid = (int)$this->_db->insertid();
		} else {
			$ret = $this->_db->updateObject('#__adsmanager_ads', $row,'id');	
			$contentid = $this->id;
		}
		
				
		$dir = JPATH_SITE."/images/com_adsmanager/ads/tmp/";
		$dirfinal = JPATH_SITE."/images/com_adsmanager/ads/";
		
    	foreach($this->data['delimages'] as $image) {
			JFile::delete($image);
		}

		if (@$this->data['paid']['delimages']) {
	    		foreach($this->data['paid']['delimages'] as $image) {
				JFile::delete($image);
			}
		}

   		foreach($this->data['images'] as $image) {
			if (!is_array($image))
					$image = (array)$image;
			$src  =$dir.$image['tmpfilename'];
			$dest =$dirfinal.$contentid.$image['filename'];			 
			JFile::move($src,$dest);
		}
		
		if (@$this->data['paid']['images']) {
			foreach($this->data['paid']['images'] as $image) {
				if (!is_array($image))
					$image = (array)$image;
				$src  =$dir.$image['tmpfilename'];
				$dest =$dirfinal.$contentid.$image['filename'];		
				JFile::move($src,$dest);
			}
		}

		$query = "SELECT * FROM #__adsmanager_config WHERE 1";
		$this->_db->setQuery($query);
		$conf = $this->_db->loadObject();
		$nbimages = $conf->nb_images;
		if (function_exists("getMaxPaidSystemImages"))
		{
			$nbimages += getMaxPaidSystemImages();
		}
		$imagefiles = array();
		for($i=1;$i < $nbimages + 1;$i++)
		{
			$ext_name = chr(ord('a')+$i-1);
			$pic = JPATH_ROOT."/images/com_adsmanager/ads/".$contentid.$ext_name."_t.jpg";
			if (file_exists($pic)) 
				$imagefiles[] = $contentid.$ext_name."_t.jpg"; 	
		}
		
		$row = new JObject();
    	$row->id = $contentid;
    	$row->images = json_encode($imagefiles);
		$ret = $this->_db->updateObject('#__adsmanager_ads', $row,'id');	
		
		$query = "DELETE FROM #__adsmanager_adcat WHERE adid = $contentid";
		$this->_db->setQuery($query);
		$this->_db->query();
		
		foreach($this->data['categories'] as $cat) {
			$query = "INSERT INTO #__adsmanager_adcat(adid,catid) VALUES ($contentid,$cat)";
			$this->_db->setQuery($query);
			$this->_db->query();
			$this->catid = $cat;
		}
		
		if (function_exists('savePaidAd')) {
			savePaidAd($this,$contentid);
		}
		
		$this->id = $contentid;
    }
    
    function delete($adid,$conf,$plugins)
    {		
    	$adid = (int) $adid;
    	
		$this->_db->setQuery("SELECT * FROM #__adsmanager_ads WHERE id=$adid");
		$ad = $this->_db->loadObject();
		
		$this->_db->setQuery("DELETE FROM #__adsmanager_ads WHERE id=$adid");
		$this->_db->query();
		
		$this->_db->setQuery("DELETE FROM #__adsmanager_adcat WHERE adid=$adid");
		$this->_db->query();
		
		$this->_db->setQuery( "SELECT name FROM #__adsmanager_fields WHERE `type` = 'file'");
		$file_fields = $this->_db->loadObjectList();
		foreach($file_fields as $file_field)
		{
			$filename = "\$ad->".$file_field->name;
			eval("\$filename = \"$filename\";");
			JFile::delete(JPATH_SITE."/images/com_adsmanager/files/".$filename);
		}
	
		$nbImages = $conf->nb_images;
	
		for($i = 1 ;$i < $nbImages + 1; $i++)
		{	
			$ext_name = chr(ord('a')+$i-1);
			$pict = JPATH_SITE."/images/com_adsmanager/ads/".$adid.$ext_name."_t.jpg";
			if ( file_exists( $pict)) {
				JFile::delete($pict);
			}
			$pic = JPATH_SITE."/images/com_adsmanager/ads/".$adid.$ext_name.".jpg";
			if ( file_exists( $pic)) {
				JFile::delete($pic);
			}
		}
		
		foreach($plugins as $plugin)
		{
			$plugin->onDelete(0,$adid);
		}
		
		if (function_exists('deletePaidAd')){
			deletePaidAd($adid);
		}
    }
}
