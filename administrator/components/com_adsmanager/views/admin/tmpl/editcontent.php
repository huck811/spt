<?php
// Check to ensure this file is within the rest of the framework
defined('JPATH_BASE') or die();
?>
<script type="text/javascript">
<?php if(version_compare(JVERSION,'1.6.0','>=')){ ?>
Joomla.submitbutton = function(pressbutton) {
<?php } else { ?>
function submitbutton(pressbutton) {
<?php } ?>
	   if (pressbutton == 'cancel') {
		   submitform(pressbutton);	
		   return;
	   }
	   var form = document.adminForm;
	   var iserror = 0;
	   var errorMSG = '';
		
	   <?php if ($this->nbcats > 1) { ?>
			var srcList = eval( 'form.selected_cats' );
			var srcLen = srcList.length;
	  
		   if (srcLen == 0)
		   {
				errorMSG += <?php echo json_encode(JText::_('ADSMANAGER_FORM_CATEGORY')); ?>+" : "+<?php echo json_encode(JText::_('ADSMANAGER_REGWARN_ERROR')); ?>+'\n';
				srcList.style.background = "red";
				iserror=1;
			}
			else
			{
				for (var i=0; i < srcLen; i++) {
					srcList.options[i].selected = true;
				}
			}
		<?php } ?>
		
		if(iserror==1) {
			alert(errorMSG);
		} else {
		
			<?php
			if (function_exists("loadEditFormCheck")) {
				loadEditFormCheck("admin");
			}
		   ?>

		   <?php if ($this->conf->plupload) { ?>
			 var uploader = jQuery('#uploader').pluploadQueue();
				
	        // Files in queue upload them first
	        if (uploader.files.length > 0) {
	            // When all files are uploaded submit form
	            uploader.bind('StateChanged', function() {
	                if (uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) {
	                    //jQuery('#adminForm')[0].submit();
	                	submitform("save");	
	                }
	            });
	                
	            uploader.start();
	            return false;
	        }  
	        <?php } ?>
	        
		   <?php if ($this->nbcats > 1) { ?>
			srcList.name = "selected_cats[]"; 
		   <?php } ?>
		   submitform(pressbutton);
		}
   }

function updateFields() {
	var form = document.adminForm;
	var singlecat = 0;
	var length = 0;
	
	if ( typeof(document.adminForm.category ) != "undefined" ) {
		singlecat = 1;
		length = 1;
	}
	else
	{
		length = form.selected_cats.length;
	}
	
	<?php
	foreach($this->fields as $field)
	{ 
		if (strpos($field->catsid, ",-1,") === false)
		{
			$name = $field->name;
			if (($field->type == "multicheckbox")||($field->type == "multiselect"))
				$name .= "[]";
		?>
		var input = document.getElementById('<?php echo $name;?>');
		var trzone = document.getElementById('tr_<?php echo $field->name;?>');
		if (((singlecat == 0)&&(length == 0))||
		    ((singlecat == 1)&&(document.adminForm.category.value == 0)))
		{
			if (input != null)
				input.style.visibility = 'hidden';
			trzone.style.visibility = 'hidden';
			trzone.style.display = 'none';
		}
		else
		{
			for (var i=0; i < length; i++) {
				
				
				var field_<?php echo $field->name;?> = '<?php echo $field->catsid;?>';
				var temp;
				if (singlecat == 0)
					temp = form.selected_cats.options[i].value;
				else
					temp = document.adminForm.category.value;
					
				var test = field_<?php echo $field->name;?>.indexOf( ","+temp+",", 0 );
				if (test != -1)
				{
					if (input != null)
						input.style.visibility = 'visible';
					trzone.style.visibility = 'visible';
					trzone.style.display = '';
					break;
				}
				else
				{
					if (input != null)
						input.style.visibility = 'hidden';
					trzone.style.visibility = 'hidden';
					trzone.style.display = 'none';
				}
			}
		}
	<?php
		}
	} 
	?>
}
</script>
<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
<tr>
<td><?php echo JText::_('ADSMANAGER_FORM_CATEGORY');?></td>
<td>
<?php
if ($this->nbcats == 1)
{
	if ($this->catid != 0)
		$catid = $this->catid;
	else if (isset($this->content->cats[0]))
		$catid = $this->content->cats[0]->catid;
	else 
		$catid = 0;
	//$catid = intval( mosGetParam( $_GET, 'catid', $catid ) );
	$this->displaySingleCatChooser(@$this->content->id,$this->conf,"com_adsmanager",$this->cats,$catid);
	if ($catid == 0)
		return;
	?>
	</td></tr></table>
	<form action="index.php" method="post" name="adminForm" id="adminForm" class="adminForm" enctype="multipart/form-data">
	<table border='0'>
	<?php
	echo "<input type='hidden' name='category' value='$catid' />";
	
}
else
{
	?>
	</td></tr></table>
	<form action="index.php" method="post" name="adminForm" id="adminForm" class="adminForm" enctype="multipart/form-data">
	<table border='0'><tr><td colspan="2">
	<?php
	$this->displayMultipleCatsChooser(@$this->content->catsid,$this->cats,$this->conf,"com_adsmanager");
}
?>
<?php if (isset($this->content->userid)) { $userid = $this->content->userid; } else { $userid = $this->userid; } ?>

<tr>
<td><?php echo JText::_('ADSMANAGER_TH_USER'); ?></td>
<td>
<select name="userid" id="userid">
<option value=""></option>
<?php foreach($this->users as $user) { ?>
<option value="<?php echo $user->id;?>" <?php if ($user->id == $userid) { echo "selected"; } ?>><?php echo $user->username; ?></option>
<?php } ?>
</select>
</td>
<td>&nbsp;</td>
</tr>


<tr>
<td><?php echo JText::_('ADSMANAGER_TH_DATE'); ?></td>
<td>
<?php echo JHTML::_('behavior.calendar'); 
if (!isset($this->content->id)) 
	$created_date = date("Y-m-d");
else
	$created_date = $this->content->date_created;
$time = date('H:i:s',strtotime($created_date)); 
echo JHTML::_('calendar', $created_date, "date_created", "date_created", "%Y-%m-%d $time", null); ?>
</td>
<td>&nbsp;</td>
</tr>

<?php 
if (!isset($this->content->id)) 
	$expiration_date =  date("Y-m-d",time() + $this->conf->ad_duration * 3600 * 24);
else
	$expiration_date = $this->content->expiration_date;
?>
<tr>
<td><?php echo JText::_('ADSMANAGER_TH_EXPIRATION_DATE'); ?></td>
<td>
<?php echo JHTML::_('calendar', $expiration_date, "expiration_date", "expiration_date", "%Y-%m-%d", null); ?>
</td>
<td>&nbsp;</td>
</tr>


<?php
foreach($this->fields as $field)
{
	echo "<tr id=\"tr_{$field->name}\"><td>".$this->field->showFieldLabel($field,$this->content,$this->default)."</td>";
	echo "<td>".$this->field->showFieldForm($field,$this->content,$this->default)."</td></tr>";
}
?>

<!-- fields -->
<!-- image -->
<?php	
if (!$this->conf->plupload) { 
	if ($this->conf->nb_images > 0)
	{
		echo "<tr><td colspan='2'>".JText::_('ADSMANAGER_FORM_AD_IMAGE_TEXT'); 
		echo "</td></tr>";
	}

	for($i = 1; $i < $this->conf->nb_images + 1; $i++)
	{
		$ext_name = chr(ord('a')+$i-1);
		?>
		<tr name="ad_picture<?php echo $i;?>"><td><?php echo JText::_('ADSMANAGER_FORM_AD_PICTURE')." ".$i; ?></td>
		<td><input id="ad_picture<?php echo $i;?>" type="file" name="ad_picture<?php echo $i;?>" />
		<?php
		if ($this->isUpdateMode) {
			$pic = JPATH_SITE."/images/com_adsmanager/ads/".$this->content->id.$ext_name."_t.jpg";
			if ( file_exists( $pic)) {
				echo "<img src='".$this->baseurl."images/com_adsmanager/ads/".$this->content->id.$ext_name."_t.jpg?time=".time()."' align='top' border='0' alt='image".$this->content->id."' />";
				echo "<input type='checkbox' name='cb_image$i' value='delete' />".JText::_('ADSMANAGER_CONTENT_DELETE_IMAGE');
			}
		}
		echo "</td></tr>";
	}
} else {	
?>
	<tr><td><?php echo JText::_('Pictures')?></td><td id="uploader_td"><div id="uploader"></div>
	<div><?php echo JText::_('ADSMANAGER_MAX_NUMBER_OF_PICTURES')?>: <span id="maximum"><?php echo $this->conf->nb_images?></span> / <span id="totalcount"><?php echo $this->conf->nb_images?></span></div>
	<div id="currentimages">
	<?php 
	$currentnbimages = 0;
	if (@$this->content->id) {
		for($i = 1; $i < $this->conf->nb_images + 1; $i++)
		{
			$ext_name = chr(ord('a')+$i-1);
			$pic = JPATH_SITE."/images/com_adsmanager/ads/".$this->content->id.$ext_name."_t.jpg";
			if ( file_exists( $pic)) {
				$currentnbimages++;
				echo "<div style='float:left;padding:5px;' id='div_image_$i' align='center'><img src='".$this->baseurl."images/com_adsmanager/ads/".$this->content->id.$ext_name."_t.jpg?time=".time()."' align='top' border='0' alt='image".$this->content->id."' />";
				echo "<br/><input type='checkbox' name='cb_image$i' onClick='removeImage($i)' value='delete' />".JText::_('ADSMANAGER_CONTENT_DELETE_IMAGE').'</div>';
			}
		}
	}
	?>
	<div style="clear:both"></div>
	</div>
	<script type="text/javascript">
	var current_uploaded_files_count = <?php echo $currentnbimages?>;
	var nb_files_in_queue = 0;
	var max_total_file_count =  <?php echo ($this->conf->nb_images)?>;

	function removeImage(id) {
		jQuery('#div_image_'+id).hide();
		if (typeof updatePaidCurrentFileCount != "undefined") {
	    	updatePaidCurrentFileCount(current_uploaded_files_count+nb_files_in_queue,
	    							   current_uploaded_files_count+nb_files_in_queue-1);
	    }
		current_uploaded_files_count -= 1;
		setCurrentFileCount();
	}
	
	function setCurrentFileCount() {
		jQuery('#maximum').html(current_uploaded_files_count+nb_files_in_queue);
	}
	function setTotalFileCount(number) {
		jQuery('#totalcount').html(number);
	}
	setCurrentFileCount();
	// Convert divs to queue widgets when the DOM is ready
	jQuery(function() {
		jQuery("#uploader").pluploadQueue({
			// General settings
			runtimes : 'html5,flash,html4',
			url : '<?php echo TRoute::_($this->baseurl.'index.php?option=com_adsmanager&task=upload&tmpl=component')?>',
			max_file_size : '10mb',
			chunk_size : '1mb',
			unique_names : true,
	
			// Resize images on clientside if we can
			resize : {width : <?php echo $this->conf->max_width?>, height : <?php echo $this->conf->max_height?>, quality : 90},
	
			// Specify what files to browse for
			filters : [
				{title : "Image files", extensions : "jpg,gif,png"}
			],
	
			// Flash settings
			flash_swf_url : '<?php echo $this->baseurl?>components/com_adsmanager/js/plupload/plupload.flash.swf',

			init : {
	            FilesAdded: function(up, files) {
					maxnewimages = max_total_file_count - current_uploaded_files_count;
					// Check if the size of the queue is bigger than max_file_count
				    if(up.files.length > maxnewimages)
				    {
				        // Removing the extra files
				        while(up.files.length > maxnewimages)
				        {
				            if(up.files.length > maxnewimages)
				            	up.removeFile(up.files[maxnewimages]);
				        }
				        alert('<?php echo JText::_(sprintf("Max %s Files",$this->conf->nb_images))?>');
				    }

				    if (typeof updatePaidCurrentFileCount != "undefined") {
				    	updatePaidCurrentFileCount(current_uploaded_files_count+nb_files_in_queue,
				    							   current_uploaded_files_count+up.files.length);
				    }
				    nb_files_in_queue = up.files.length;
			        setCurrentFileCount();
				},
				FilesRemoved: function(up, files) {
					if (typeof updatePaidCurrentFileCount != "undefined") {
						updatePaidCurrentFileCount(current_uploaded_files_count+nb_files_in_queue,
    							   				   current_uploaded_files_count+up.files.length);
				    }
					nb_files_in_queue = up.files.length;
			        setCurrentFileCount();
				}
			}
		});
	});
	</script>
	</td></tr>
<?php } ?>
<tr>
<td><?php echo JText::_('ADSMANAGER_TH_PUBLISH'); ?></td>
<td>
<select name="published" id="published">
<option value="1" <?php if (@$this->content->published == 1) { echo "selected"; } ?>><?php echo JText::_('ADSMANAGER_PUBLISH'); ?></option>
<option value="0" <?php if (@$this->content->published == 0) { echo "selected"; } ?>><?php echo JText::_('ADSMANAGER_NO_PUBLISH'); ?></option>
</select>
</td>
<td>&nbsp;

</td>
</tr>

<?php if (($this->conf->metadata_mode != 'nometadata')&&
		  ($this->conf->metadata_mode != 'automatic')) { ?>
<tr><td colspan='2'><strong><?php echo JText::_('ADSMANAGER_METADATA')?></strong></td></tr>
<tr>
<td><?php echo JText::_('ADSMANAGER_METADATA_DESCRIPTION'); ?></td>
<td>
<textarea cols="50" rows="10" name="metadata_description"><?php echo htmlspecialchars(@$this->content->metadata_description)?></textarea>			
</td>
</tr>

<tr>
<td><?php echo JText::_('ADSMANAGER_METADATA_KEYWORDS'); ?></td>
<td>
<textarea cols="50" rows="10" name="metadata_keywords"><?php echo htmlspecialchars(@$this->content->metadata_keywords)?></textarea>			
</td>
</tr>

<?php } ?>

<?php 
if (function_exists("editAdminPaidAd")){
	editAdminPaidAd($this->content,$this->isUpdateMode,$this->conf);
}?>
</table>
<input type="hidden" name="id" value="<?php echo @$this->content->id; ?>" />
<input type="hidden" name="option" value="com_adsmanager" />
<input type="hidden" name="c" value="contents" />
<input type="hidden" name="task" value="" />
</form>
<script type="text/javascript">
updateFields();
</script>