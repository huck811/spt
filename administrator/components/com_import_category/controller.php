<?php

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class import_categoryHelper extends JController
{
/**
	 * Configure the Linkbar.
	 *
	 * @param	string	The name of the active view.
	 *
	 * @return	void
	 * @since	1.6
	 */

	public static function addSubmenu($vName)
	{
		JSubMenuHelper::addEntry(
			JText::_('Импорт категорий'),
			'index.php?option=com_import_category',
			$vName == ''
		);

		JSubMenuHelper::addEntry(
			JText::_('Мета-данные'),
			'index.php?option=com_import_category&view=import_category&layout=metadata',
			$vName == 'import_category'
		);
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return	JObject
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$actions = JAccess::getActions('com_messages');

		foreach ($actions as $action) {
			$result->set($action->name,	$user->authorise($action->name, 'com_messages'));
		}

		return $result;
	}

	/**
	 * Get a list of filter options for the state of a module.
	 *
	 * @return	array	An array of JHtmlOption elements.
	 */
	static function getStateOptions()
	{
		// Build the filter options.
		$options	= array();
		$options[]	= JHtml::_('select.option',	'1',	JText::_('COM_MESSAGES_OPTION_READ'));
		$options[]	= JHtml::_('select.option',	'0',	JText::_('COM_MESSAGES_OPTION_UNREAD'));
		$options[]	= JHtml::_('select.option',	'-2',	JText::_('JTRASHED'));
		return $options;
	}


}




class import_categoryController extends JController

 {
  function __construct()
     {
         parent::__construct();
     } 

  public function display()

    {
       import_categoryHelper::addSubmenu(JRequest::getCmd('view', ''));
       parent::display();

    }

}