<?php
include '../../../geo/class/app/app.php';
$file = BASE."/administrator/components/com_import_category/f_meta/metadata.txt";
$user_id = $_GET['id'];
$ses_id = $_GET['session'];

$importtxt = htmlspecialchars($_POST['importtxt']);

if ($ses_id != md5($user_id.'meta'))
    exit('wrong session');

$category = new Ads_Category();
//echo '<pre>';
//print_r($arr_cat);

$categories = get_list_import($importtxt);
foreach ($categories as $id => $cat){
    $category->save(array('id' => $id,
                  'metadata_description' => $cat['Description'],
                  'metadata_keywords' => $cat['Keywords']));
}

ClearCache::categories();

exit('success***');


function get_list_import($txt_import){
   $br = chr(10);
   $arr = explode($br, $txt_import);
   
   $cats = array();
   
   $where_is = '';
   
   foreach ($arr as $item){
       $item = trim($item);
       $item = trim($item, chr(10));
       $item = trim($item, chr(13));

       $ident = get_ident($item);
       if (is_numeric($ident)){
           $cur_id = $ident;
           $where_is = '';
       }
       
       else if($ident == 'Description' || $ident == 'Keywords'){
           $where_is = $ident;
       }
       
       else{
          $cats[$cur_id][$where_is] .= $item; 
       }
   }
     
   return $cats;
}

function get_ident($str){
   $prefix = '##';
   if (substr_count($str, $prefix) == 2){
       $arr = explode($prefix,$str);
       return $arr[1];
   }

   
   return false;
}




