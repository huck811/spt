<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<?php
defined('_JEXEC') or die;

include '../geo/class/app/app.php';
$user = & JFactory::getUser();
$user_id = $user->get('id');

$file = "/administrator/components/com_import_category/f_meta/metadata.txt";
?>
<table class="import_meta">
    <tr>
        <td>Шаг №1 - будет автоматически создан файл со списком всех категорий и их метаданными</td>
        <td><input type='submit' value='выгрузить метаданные' onClick="_loadFile();" /></td>
        <td><span class="success">Данные успешно выгружены!!!</span></td>
    </tr>
    <tr>
        <td>Шаг №2 - измените метаданные и сохраните изменения (будет произведен импорт метаданных в систему)</td>
        <td><input type='submit' value='Сохранить изменения' onClick="_ImportFile();" /></td>
        <td></td>
    </tr>
</table>

<textarea id="metadata"></textarea>

<script>
   function _loadFile(){
       if (confirm("Будет создан новый файл со списком категорий. Старый файл будет заменен. Вы уверены?"))
       jQuery.post("/administrator/components/com_import_category/create_matadata.php?session=<?php echo md5($user_id.'meta')?>&id=<?php echo $user_id;?>", function(response){ // запрос на удаление
          response = response.split('***');
          if(response[0]==="success"){ // успех
              jQuery('.import_meta .success').show();
              jQuery('.import_meta .success').fadeOut(2000);
              jQuery('#metadata').val(response[1]);
          }
          else{ // удалить не получилось

          }         
       });
       
       
   } 
   
   function _ImportFile(){
       jQuery.post("/administrator/components/com_import_category/import_matadata.php?session=<?php echo md5($user_id.'meta')?>&id=<?php echo $user_id;?>", {importtxt : jQuery('#metadata').val()},
       
        function(response){ // запрос на удаление
          response = response.split('***');
          if(response[0]==="success"){ // успех
              alert('Метаданные успешно импортированны!');
          }
          else{ // удалить не получилось
              alert('Ошибка при импорте!');
          }         
       });
       
       
   } 
</script>

<style>
    input[type="submit"]:hover{
        cursor: pointer;
    }
    
    .import_meta td{
        padding-bottom: 20px;
        padding-right: 20px;
    }
    
    .success{
        color:green;
        display: none;
        padding-left: 20px;
    }
    
    #metadata{
        min-width: 50%;
        min-height: 700px;
    }
</style>
    