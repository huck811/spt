<?php
// настройки для подключения к БД
define ('SERVER', 'localhost');
define ('DB_NAME', 'spt');
define ('DB_USER', 'spt');
define ('DB_PASS', 'GNBcjsCe');
define ('TABLE_PREFIX','tsj35');

define ('SHOW_DB_ERROR',0);

define ('SHOW_TOP',false);



// Общее время кэширования
define ('CACHE_TIME', 720000);
define ('ENABLE_CACHE_FUNCTION', true);
define ('ENABLE_CACHE_MYSQL', false);
define ('BASE_URL', '/'); // Папка в которой лежит сайт (www.site.ru/papka/). Укажите (/) если файлы располагаются в корне.
define ('CACHE_FOLDER', $_SERVER['DOCUMENT_ROOT'].BASE_URL.'/cache/');
define ('CACHE_CITY_ALL_FILE', 'all_cities.cache');
define ('CACHE_CATEGORY_ALL_FILE', 'all_category.cache');
define ('CACHE_CATEGORY_LIST','category_list.cache');
define ('CACHE_CATEGORY_FILE', '_category.cache');
define ('CACHE_CITY_BY_REGION_FILE', 'cities_by_region');
define ('CACHE_PRODUCER_BY_CATEGORY_FILE', 'producer_by_category');
define ('LEVEL_CATEGORY', 3);
define ('DIR_SEP', '/');

//Настройки для загрузки изображений
// абсолютный путь к корню сайта
define('BASE', '/var/www/spt-portal/data/www/spt-portal.ru/');
$uploaddir = BASE.'images'.DIR_SEP.'com_ads'.DIR_SEP; // папка, в которой располагаются изображения
define ('UPLOAD_DIR', $uploaddir);


define ('LOG_CRON_ENABLE', true);
define ('LOG_ENABLE', true);
define ('ADMIN_EMAIL_FOR_LOG', false);
define ('LOG_FILE_CRON', BASE.'logs/cron.log');
define ('LOG_FILE_CRON_ERROR', BASE.'logs/cron_error.log');
define ('LOG_FILE', BASE.'logs/log.log');
define ('LOG_FILE_ERROR', BASE.'logs/log_error.log');

// Ограничения весовых и габаритных размеров фотографий

define('MAX_FILE_SIZE', 10000);
define('MAX_FILE_AVATAR_SIZE', 300);
define('MAX_PHOTO_ITEMS', 9);

define('MAX_WIDTH', 600);
define('MAX_HEIGHT', 800);

define('MAX_AVATAR_WIDTH', 350);
define('MAX_AVATAR_HEIGHT', 263);

define('PREVIEW_WIDTH', 350);
define('PREVIEW_HEIGHT', 263);

define('THUMB_PREFIX','small_'.PREVIEW_WIDTH.'_'.PREVIEW_HEIGHT.'_');
define ('NUM_ADS_ON_PAGE',10); // кол-во миниатюр на странице под основным изображением

//Годовой диапазон
define ('MIN_YEAR',1990);
define ('MAX_YEAR',2013);
$min_year = MIN_YEAR;
$max_year = MAX_YEAR;

define ('MIN_NARABOTKA',0);
define ('STEP_NARABOTKA',2000);
define ('MAX_NARABOTKA',50000);

define ('MIN_PROBEG',0);
define ('STEP_PROBEG',50000);
define ('MAX_PROBEG',1000000);

define ('WELLCOME','<span class="welcome">Добро пожаловать! <br/>Всегда рады Вас видеть!</span>');
define ('PROFILE_INFO','<i style="font-size:10px;">После добавления компании, вы можете изменить информацию о<br/> предоставляемых брендах на странице редактирования компании</i>');

define ('VALIDATE_FORM',1); // валидация форм
define ('DEFAULT_MIN_MAX_YEAR_OPTION','Не важно');
define ('DEFAULT_SELECT_VAL','Выбрать');
define ('DEFAULT_SELECT_VAL1','Выберите город');

define ('NEW_ITEM_VAL','новая');
define ('OLD_ITEM_VAL','б/у');
define ('NEW_ITEM_LABEL','Новая');
define ('OLD_ITEM_LABEL','Б/У');

define ('SELL_ITEM_VAL','продам');
define ('RENT_ITEM_VAL','сдам');
define ('SELL_ITEM_LABEL','Продам');
define ('RENT_ITEM_LABEL','Сдам');

define ('STATE_PASSIVE_ADS_VAL','Не активно');
define ('STATE_ACTIVE_ADS_VAL','Активно');
define ('STATE_SELLED_ADS_VAL','Продано');
define ('STATE_PASSIVE_ADS_LABEL','Не активно');
define ('STATE_ACTIVE_ADS_LABEL','Активно');
define ('STATE_SELLED_ADS_LABEL','Продано');

define ('REGION_LABEL','Регион:');
define ('CITY_LABEL','Город:');
define ('COMMENT_LABEL','Комментарий:');

define ('COST_LABEL','Цена:');
define ('COST_LABEL_1','Цена за смену:');
define ('COST_LABEL_2','Цена за час:');

define ('YEAR_BORN_LABEL','Год выпуска:');
define ('MODEL_LABEL','Модель:');
define ('CATEGORY_LABEL','Категория:');
define ('CATEGORY_SUB_LABEL','Подкатегория:');
define ('PRODUCER_LABEL','Производитель:');

define ('WHAT_WORK_HOUR_LABEL','Наработка часов:');
define ('WHAT_WORK_HOUR_TRANSPORT_LABEL','Пробег в км:');

define ('HEADLINE_LABEL','<strong><i>Заголовок:</i></strong>');
define ('BTN_LOAD_IMAGE_VAL','Загрузить');
define ('LOAD_IMAGE_LABEL','Картинка:');

define ('LINK_ADS_MAKE_UP','Поднять');
define ('LINK_ADS_MAKE_TOP','Разместить в ТОП');
define ('LINK_ADS_MAKE_SPECIAL','Выделить');

define ('MESSAGE_WRONG_IMAGE_FORMAT','В формате JPG, PNG не более %s Кб и %spx');
define ('MESSAGE_INCORRECT_EMAIL','Неверный формат почты...');

//search panel

define('SELECT_CITY', 'Все города');
define('SELECT_REGION', 'Все регионы');
define('SELECT_CATEGORY', 'Все категории');
define('SELECT_SUB_CATEGORY', 'Все подкатегории');
define('SELECT_PRODUCER', 'Производитель');
define('SELECT_MODEL', 'Модель');
define('MOTOHOUR_MIN', 0);
define('MOTOHOUR_MAX', 200000);

//list category 

define('CLIST_MOTO_HOUR', 'наработка');
define('CLIST_MOTO_HOUR_1', 'наработка / пробег');
define('CLIST_MOTO_HOUR_TRANSPORT', 'пробег в км');
define('CLIST_MOTO_HOUR_TRANSPORT_1', 'пробег');
define('CLIST_MOTO_COST', 'цена');
define('CLIST_MOTO_COST_RENT_1', 'за час');
define('CLIST_MOTO_COST_RENT_2', 'за смену');
define('CLIST_MOTO_YEAR', 'год');
define('CLIST_MOTO_MODEL', 'модель');
define('CLIST_MOTO_PRODUCER', 'производитель');
define('CLIST_MOTO_CATEGORY', 'категория');
define('CLIST_MOTO_CITY', 'город');
define('CLIST_MOTO_PHOTO', 'фото');

//Соответствие полей базы данных и формы
$profile_fiz = array('email' => 'email2', 'region' => 'region',
'ad_avatar' => 'ad_avatar', 'ad_city' => 'ad_city',
'ur_face' => 'type_user', 'fio' => 'fio',
'ad_phone' => 'ad_phone');

$profile_ur = array('company_phone' => 'company_phone', 'fax' => 'fax',
'ad_site' => 'ad_site', 'specialization' => 'specialization',
'company_name' => 'company_name', 'fact_adress' => 'fact_adress', 'brands' => 'brands', 'descript' => 'descript');

                
$ads = array('userid' => 'user_id', 'images' => 'main_image',
'aditional_image' => 'images', 'ad_headline' => 'head',
'ad_text' => 'comment', 'ad_state' => 'state',
'ad_price' => 'cost1', 'ad_price_1' => 'cost_smena',
'ad_price_2' => 'cost_hour', 'published' => 'status',
'ad_producers' => 'producer', 'ad_model' => 'model', 'ad_kindof' => 'ad_kindof',
'ad_year' => 'year', 'ad_city' => 'city', 'ad_motohour' => 'moto_hour');

define ('PROFILE_USER_FIZ_NAMES', serialize($profile_fiz));
define ('PROFILE_USER_UR_NAMES', serialize($profile_ur));
define ('ADS_NAMES', serialize($ads));

// Основные URL адреса
define('ITEMID_MY_ADS', 133);
define('URL_BASE', BASE);
define('URL_PROFILE', 'index.php?Itemid=136'); // профиль
define('URL_MY_ADS', 'index.php?Itemid='.ITEMID_MY_ADS);  // мои объявления
define('URL_ADD_ADS', 'index.php?option=com_content&view=article&id=7&Itemid=132');  // добавить объявление
define('SINGLE_ADS', 'index.php?Itemid=138');  // объявление
define('URL_SEF_MY_ADS', BASE_URL.'spisok-ob-yavlenij.html'); // мои объявления ЧПУ

//УРЛ разделов
//define(URL_SPECTECH, 'index.php?option=com_content&Itemid=138&search_category=1&id=11&lang=ru&view=article');

//При импорте новых категорий измените ИД разделов на актуальные
define('CAT_ID_SPECTECH', 1);    // ИД категории спецтехника
define('CAT_ID_KOMMTECH', 117);   // ИД категории комунальная техника
define('CAT_ID_TRASPORT', 69);   // ИД категории комунальная транспорт
define('CAT_ID_SKLADTECH', 129);  // ИД категории складская техника
define('CAT_ID_SELHOZTECH', 141);// ИД категории сельхозтехника

// Title главной страницы.
define('MAIN_PAGE_TITLE', 'Продажа спецтехники, аренда спецтехники');
// Город по умолчанию. Если IP адреса нет в списке Российский городов, то подставляется город по умолчанию
define('DEFAULT_CITY', 'Новосибирск');
// Любой IP адрес города по умолчанию
define('DEFAULT_IP', '37.193.180.76');
// Заменять локальные адреса вида 192.168... 
define('REPLACE_LOCAL_IP', true);

//****Активность пользователей
//кол-во дней неактивности пользователя после чего блокируется аккаунт
define('LIVE_TIME_USER', 90);
//через какое кол-во дней до окнончания периода неактивности слать предупреждение первый раз
define('FIRST_WARNING_BLOCK_USER', 7);
//... второй раз
define('SECOND_WARNING_BLOCK_USER', 3);
//... третий раз
define('THIRD_WARNING_BLOCK_USER', 1);

define('SUBJECT_MAIL_1', "Сообщение с портала spt-portal.ru");

//****Сроки жизни объявлений и статусы

// Время жизни (дней) объявления со статусом "Продано"
define('LIVE_TIME_ADS_SELLED', 14);

// Статус "Продано"
define('STATUS_ADS_SELLED', -2);

// Статус "Удалено" пользователем
define('STATUS_ADS_DELETED_BY_USER', -1);

// Статус "Не опубликованно"
define('STATUS_ADS_UNPUBLISHED', 0);

// Статус "Опубликованно"
define('STATUS_ADS_PUBLISHED', 1);

//****Стоимость и сроки размещения объявлений

// включение системы платежей
define('PAYMENTS_ENABLE', true);

//Стоимость размещения в топе в рублях
define('COST_IN_TOP', 250);
//Стоимость выделения цветом в рублях
define('COST_SELECT_ADS', 100);
//Стоимость поднятия в выдаче в рублях
define('COST_UP_ADS', 150);

//Время размещения в топе в днях
define('TIME_IN_TOP', 7);
//через какое кол-во дней до окнончания ТОПа слать предупреждение первый раз
define('FIRST_WARNING_TOP', 3);
//... второй раз
define('SECOND_WARNING_TOP', 2);
//... третий раз
define('THIRD_WARNING_TOP', 1);

//Время выделения цветом в днях
define('TIME_SELECT_ADS', 7);

//********Тестовый период

// включение (true) / выключение (false) тестового периода
define('TEST_PERIOD_ENABLE', false);

// количество объявлений которые можно разместить бесплатно в тестовом режиме
define('TEST_PERIOD_NUM_ADS', 1);

// количество дней размещения в тестовом режиме
define('TEST_PERIOD_DAYS', 30);


?>