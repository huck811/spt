--
-- Triggers `tsj35_users`
--
DROP TRIGGER IF EXISTS `delete_user`;
DELIMITER //
CREATE TRIGGER `delete_user` AFTER DELETE ON `tsj35_users`
 FOR EACH ROW BEGIN 
   DELETE FROM tsj35_adsmanager_ads WHERE userid = old.id; 
END
//
DELIMITER ;