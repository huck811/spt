<?php

class Payments_Payment{
    private static $db;
    private static $qb;
    private static $payment;
    private static $cost_select_ads;
    private static $cost_in_top;
    private static $cost_up_ads;
    private static $select_note;
    private static $in_top_note;
    private static $up_ads_note;
    private static $sort_field;
    private static $PayOnlineObj;
    
    public function __construct(){
       $this->db = new Db_connect(SERVER, DB_NAME, DB_USER, DB_PASS);
       $this->qb = new Db_ChainQueryBuilder(); 
    }
    
    
    /**
     * get singelton object of Payments_Payment
     * @return Payments_Payment
     */
     
    public static function instance(){
        if (!self::$payment){
            self::$cost_select_ads = TIME_AND_COST_SELECT;
            self::$cost_up_ads     = (-1)*COST_UP_ADS;
            self::$cost_in_top     = TIME_AND_COST_IN_TOP;
            self::$select_note     = 'select';
            self::$in_top_note     = 'top';
            self::$up_ads_note     = 'up';
            self::$sort_field      = 'date';
            self::$payment = new Payments_Payment(); 
        }
        
        return self::$payment;
    }
    
    
    
    /**
     * Add payment into the payment history and change user account 
     * @param int   $user_id
     * @param int   $amount - amount of payment
     * @param mixed $note - (string / boolean) payment description
     * @param bool  $RealPayment - indicates the type of payment. true - is real payment using real payment gateway such PayOnline. 
     * false - payment for user such Admin role with inner gateway
     * @return mixed - last insert id of payment history or false when it is not authorized user
     */
     
    private static function add_payment($user_id, $amount, $note = false, $RealPaynemt = false){

        if ($user_id){
            $lastId = false;
            try{
            self::$payment->db->beginTransaction(); // начало транзакции
            
            if (!$RealPaynemt)
                self::change_amount($user_id, $amount);                          
            
            $lastId = self::add_in_history($user_id, $amount, $note, !$RealPaynemt, $OrderId);
            self::$payment->db->commit(); // завершение транзакции
            }
            catch(Exception $e){
               self::$payment->db->rollBack(); // откат
               echo  "Ошибка (не возможно добавить платеж): " . $e->getMessage(); 
            }
            
            return $lastId;
        } 
        return false;       
    }
    
    /**
     * Change user account
     * @param int   $user_id
     * @param int   $amount - amount of payment
     *  
     */
    
    private static function change_amount($user_id, $amount){
        self::$payment->db
                ->bind_param(array( 'uid' => $user_id, 'amount' => $amount))
                ->execute('INSERT INTO #__user_schet VALUES (:uid, :amount) ON DUPLICATE KEY UPDATE amount=amount+:amount', false); 
    }
    
    /**
     * verification type of select (ad highlighted in the list)
     * @param  string $note - payment description
     * @return bool
     */
    
    public static function is_select_note($note){
        return substr_count($note, self::$select_note.'#') ? true : false;    
    }
    
    /**
     * verification type of up (ad raised in the list)
     * @param  string $note - payment description
     * @return bool
     */
    
    public static function is_up_note($note){
        return substr_count($note, self::$up_ads_note.'#') ? true : false;    
    }
    
    /**
     * verification type of top (top listing added)
     * @param  string $note - payment description
     * @return bool
     */
    
    public static function is_top_note($note){
        return substr_count($note, self::$in_top_note.'#') ? true : false;    
    }
    
    /**
     * adds fee for ad highlighted
     * @param int   $user_id
     * @param int   $ads_id
     * @return mixed - last insert id of payment history or false when it is not authorized user 
     */
    
    public static function add_payment_select($user_id, $ads_id, $period){
        $price = Ads_Price::init(self::$cost_select_ads)->get_price_by_time($period);
        return self::$payment->add_payment($user_id, (-1)*$price, self::$select_note.'#'.$ads_id);    
    }
    
    /**
     * adds fee for ad raised
     * @param int   $user_id
     * @param int   $ads_id
     * @return mixed - last insert id of payment history or false when it is not authorized user 
     */
    
    public static function add_payment_up($user_id, $ads_id){
        return self::$payment->add_payment($user_id, self::$cost_up_ads, self::$up_ads_note.'#'.$ads_id);    
    }
    
    /**
     * adds fee for top listing
     * @param int   $user_id
     * @param int   $ads_id
     * @return mixed - last insert id of payment history or false when it is not authorized user 
     */
    
    public static function add_payment_top($user_id, $ads_id, $period){
        $price = Ads_Price::init(self::$cost_in_top)->get_price_by_time($period);
        return self::$payment->add_payment($user_id, (-1)*$price, self::$in_top_note.'#'.$ads_id);   
    }
    
    /**
     * refill
     * @param int   $user_id
     * @param int   $amount - amount of payment
     * @param bool  $RealPayment - indicates the type of payment. true - is real payment using real payment gateway such PayOnline. 
     * false - payment for user such Admin role with inner gateway
     */
     
    public static function RechargeAccount($user_id, $amount, $realPayment = false){
  
        if ($user_id && $amount && $amount > 0){
            if ($realPayment){
                
                // регистрируем платеж в системе
                $OrderId = self::$payment->add_payment($user_id, $amount, false, $realPayment);
                if ($OrderId){    
                    //Показываем ссылку на оплату           
                    $result = self::CreatePayOnlineObject($OrderId, $amount, $user_id)->GetPaymentURL();
                    //exit($result);
                    exit("<meta http-equiv='refresh'  content=\"0; URL=".$result."\">");
                }
            }
            else
                Message_User::payment_complete($amount, $user_id); 
            
            return self::$payment->add_payment($user_id, $amount, false);  
        } 
        return false;
    }
    
    /**
     * method create PayOnline object which it is real payment gateway 
     * @param int $OrderId
     * @param int $amount - amount of payment
     * @param int $user_id
     * @return Payments_PayOnline
     */
    function CreatePayOnlineObject ($OrderId, $amount, $user_id){
        if (!self::$PayOnlineObj){
            //Создаем класс
            $pay = new Payments_PayOnline;
            //Указываем локализацию (доступно ru | en | fr)
            $pay->Language = "ru";
            // Указываем идентификатор мерчанта
            $pay->MerchantId='57832';
            //Указываем приватный ключ (см. в ЛК PayOnline в разделе Сайты -> настройка -> Параметры интеграции)
            $pay->PrivateSecurityKey='14e4ea2a-7c8a-4164-9a80-8c9a71838793';
            //Валюта (доступны следующие валюты | USD, EUR, RUB)
            $pay->Currency='RUB';
            //Описание заказа (не более 100 символов, запрещено использовать: адреса сайтов, email-ов и др.) необязательный параметр
            $pay->OrderDescription = "Пополнение счета №$user_id на сумму $amount Рублей";
            //Срок действия платежа (По UTC+0) необязательный параметр
            //$pay->ValidUntil="2013-10-10 12:45:00";
            //В случае неуспешной оплаты, плательщик будет переадресован, на данную страницу.
            $pay->FailUrl   = "http://spt-portal.ru/popolnenie-scheta.html?paymentFail";
            // В случае успешной оплаты, плательщик будет переадресован, на данную страницу. 
            $pay->ReturnUrl = "http://spt-portal.ru/popolnenie-scheta.html?paymentIsok";
            self::$PayOnlineObj = $pay;
        }
        
        //Номер заказа (Строка, макс.50 символов)
        self::$PayOnlineObj->OrderId = $OrderId;
        
        //Сумма к оплате (формат: 2 знака после запятой, разделитель ".")
        self::$PayOnlineObj->Amount = number_format($amount, 2, '.', '');
        
        return self::$PayOnlineObj;
    }
    
    

    /**
     * add payment into the history
     * @param int   $user_id
     * @param int   $amount - amount of payment
     * @param mixed $note - (string / boolean) payment description
     * @param bool  $published
     * @return mixed - last insert id of payment history or false when it is not authorized user
     */
    public static function add_in_history($user_id, $amount, $note = false, $published = false, $OrderId = false){
        if ($user_id){
            $qb = self::$payment->qb;
            $q = $qb 
                    ->insertInto('#__user_payment_history')
                    ->fields(array('user_id', 'amount', 'note', 'published'))
                    ->values(array(':uid', ':amount', ':note', ':pub'))
                    ->build();      
            self::$payment->db->bind_param(
                array( 
                'uid' => $user_id,
                'amount' => $amount,
                'note' => $note,
                'pub' => $published)
                )
            ->execute($q, false); 
            
            return self::$payment->db->lastInsertId();
        }        
        return false;
    }
    
    /**
     * Call when payment gateway return successfull data
     * @param array $Date - an array of data from the payment gateway
     * @return bool
     */ 
    public static function PaymentCallBackOk($Date){
        
        $OrderId       = $Date['OrderId'];
        $TransactionID = $Date['TransactionID'];
        $history       = self::get_payment_history(false, (int)$OrderId); 
        
        if ($OrderId && self::isValidParamsRequest($Date) && !$history[0]['published'] ){
            $qb = self::$payment->qb;
            $q = $qb
                   ->update('#__user_payment_history')
                   ->where('id','=',(int)$OrderId)
                   ->fields(array('published', 'transactionid', 'date'))
                   ->values(array(1, $TransactionID, 'NOW()'))
                   ->build();
            self::$payment->db->execute($q, false);                            
            self::change_amount($history[0]['user_id'], $history[0]['amount']); 
             
            Message_User::payment_complete($history[0]['amount'], $history[0]['user_id']);  
            return true;
                  
        }
        return false;
    }
    
    /**
     * Call when payment gateway return fail data
     * @param array $Date - an array of data from the payment gateway
     * @return bool
     */ 
    public static function PaymentCallBackFail($Date){
        
        $OrderId = $Date['OrderId'];
        $history = self::get_payment_history(false, (int)$OrderId); 
        
        if ($OrderId && self::isValidParamsRequest($Date) && !$history[0]['published'] ){
            $qb = self::$payment->qb;
            $q = $qb
                   ->deleteFrom('#__user_payment_history')
                   ->where('id','=',(int)$OrderId)
                   ->limit(1)
                   ->build();
            self::$payment->db->execute($q, false);                        
            return true;
                  
        }
        return false;
    }
    
    /**
     * check payment data
     * @param array $Date - an array of data from the payment gateway
     * @return bool
     */
    public function isValidParamsRequest($Date){
        return self::CreatePayOnlineObject($Date['OrderId'], $Date['Amount'])->isValidResponse($Date);
    }
    
    /**
     * check payment session
     * @param int $OrderId
     * @return bool
     */
    public function isValidPaymenSession($OrderId, $ses_id){
        if ($ses_id && $OrderId){
            $PayObj = self::CreatePayOnlineObject($OrderId, 0, 0);
            $Valid_sess_id = $PayObj->get_session_id();
            if ($ses_id == $Valid_sess_id)
                return true;
        }
        return false;
    }
    
    /**
     * get amount for user account
     * @param int   $user_id
     * @return int 
     */
    public static function get_payment($user_id){
        $qb = self::$payment->qb;
        $q = $qb 
                ->select(array('amount'))
                ->from('#__user_schet')
                ->where('user_id', '=', ':uid')
                ->limit(1)
                ->build();
                
        $res = self::$payment->db->bind_param(array( 'uid' => $user_id))->execute($q); 
        if ($res)
            return $res['amount']; 
        else
            return 0;           
    }
    
    /**
     * set sort field
     * @param string $sf
     * @return Payments_Payment  
     */
    public static function set_sort_field($sf){
        self::$sort_field = $sf; 
        return self;           
    }
    
    /**
     * get payment history
     * @param int   $user_id
     * @param int   $id - payment id
     * @return array $res
     */
    public static function get_payment_history($user_id, $id = false){
        $qb = self::$payment->qb;
            $qb 
                ->select(array('*'))
                ->from('#__user_payment_history');
                
        if ($user_id){
            $qb        
                ->where('user_id', '=', (int)$user_id)
                ->andWhere('published', '=', 1);   
        }        
        else if ($id) 
            $qb        
                ->where('id', '=', (int)$id);    
                
        
                        
        $q = $qb    
                ->orderBy(self::$sort_field)
                ->order('DESC')
                ->build();      
        $res = self::$payment->db->fetchAll($q); 
        return $res;           
    }
    
    /**
     * check for sufficient money in the account
     * @param int $cost - amount of payment
     * @param int   $user_id
     * @return bool
     *  
     */
    public static function valid_pay($cost, $user_id){
       if (self::get_payment($user_id) >= $cost) 
            return true;
       else 
            return false;      
    }
}

