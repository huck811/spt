<?php
require_once '../class/app/app.php';
require_once 'cron_config.php';

class ads_time_check{
    private $db;
    private $qb;
    
    function __construct(){
        $this->qb = new Db_ChainQueryBuilder();
        $this->db = new db_connect(SERVER, DB_NAME, DB_USER, DB_PASS);
    }
    
    function check($live_time, $period, $status = array(STATUS_ADS_UNPUBLISHED, STATUS_ADS_PUBLISHED)){
        
        $first_data = MyDateTime::GetDeltaTime('', $live_time - $period);
        
        for ($i=0; $i<count($status); $i++)
            $status[$i] = 'am.published = '.$status[$i];

        $q = 
        'SELECT u.id, am.id AS adsid, am.date_recall, am.date_up'.
        ' FROM #__adsmanager_ads AS am, #__users AS u'.
        ' WHERE ('.implode(' OR ',$status).')'.
        ' AND (am.date_recall IS NULL OR am.date_recall <= "'.$first_data.'")'.
        ' AND am.date_up <= "'.$first_data.'"'.
        ' AND u.id = am.userid';        

        $res = $this->db->fetchAll($q); 
         
        //echo $q;
        if ($period)
            Message_Message::instance()->set_user_mail_message(ADS_TIME_END,$period,$res, 'ads_time_end');
        else{
            
            return $res; 
        }       
        
    } 
    
    function block_ads($ads){
        $id = $ads['adsid'];
        //блокируем объявление
        $q = $this->qb
                    ->update('#__adsmanager_ads')
                    ->set(array('published' => STATUS_ADS_SYSTEM_DELETE))
                    ->where('id', '=', $id)
                    ->limit(1)
                    ->build();
        //echo $q;
        try{   
            $this->db->execute($q, true);
            $mess = 'Объявление '.$id.' заблокированно.';
            System_Log::set($mess, LOG_FILE_CRON, false, LOG_CRON_ENABLE); 
        }
        catch(Exception $e){
            $mess = "Ошибка (не возможно заблокировать объявление ".$id."): " . $e->getMessage() . " Функция: ".__METHOD__;
            System_Log::set($mess, LOG_FILE_CRON_ERROR, ADMIN_EMAIL_FOR_LOG, LOG_CRON_ENABLE); 
        } 
    }
    
    function delete_ads($ads){
        $id = $ads['adsid'];
        //блокируем объявление
        $q = $this->qb
                    ->deleteFrom('#__adsmanager_ads')
                    ->where('id', '=', $id)
                    ->limit(1)
                    ->build();
        //echo $q;
        try{   
            $this->db->execute($q, true);
            $mess = 'Объявление '.$id.' удалено.';
            System_Log::set($mess, LOG_FILE_CRON, false, LOG_CRON_ENABLE); 
        }
        catch(Exception $e){
            $mess = "Ошибка (не возможно удалить объявление ".$id."): " . $e->getMessage() . " Функция: ".__METHOD__;
            System_Log::set($mess, LOG_FILE_CRON_ERROR, ADMIN_EMAIL_FOR_LOG, LOG_CRON_ENABLE); 
        } 
    }
    
 
} 

$ua = new ads_time_check();

// блокируем объявления
$ads_is_over = $ua->check(LIVE_TIME_ADS, 0);

/*echo '<pre>';
print_r($ads_is_over);
echo '</pre>';*/

if (count($ads_is_over))
    array_map(array($ua,"block_ads"), $ads_is_over);

// удаляем объявления
$ads_delete = $ua->check(TIME_ADS_DELETE, 0, array(STATUS_ADS_DELETED_BY_USER, STATUS_ADS_SELLED, STATUS_ADS_SYSTEM_DELETE));
/*echo '<pre>';
print_r($ads_delete);
echo '</pre>';*/
if (count($ads_delete))
    array_map(array($ua,"delete_ads"), $ads_delete);