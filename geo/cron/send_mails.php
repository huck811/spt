<?php
require_once '../class/app/app.php';
require_once 'cron_config.php';


class send_mails{
    private $mail;
    
    function __construct(){
        $this->mail = new Mail_Mailer();
    }

   
    function send(){
      $messages = Mail_Mailouts::get_all(NUM_SEND_MAILS);  
      foreach($messages as $msg) {
        //print_r($msg);
          
        try {  
        	/*$this->mail->setFrom($msg['from_name'], $msg['from_address']);
        	$this->mail->addRecipient($msg['to_name'],$msg['to_address']);
        	$this->mail->fillSubject($msg['subject']);
        	$this->mail->fillMessage($msg['content']);*/
            $this->mail->clearAllRecipients();
            $this->mail->setFrom($msg['from_name'], $msg['from_address']);
            //Set who the message is to be sent to
            $this->mail->addRecipient($msg['to_name'], $msg['to_address']);
            //Set the subject line
            $this->mail->fillSubject($msg['subject']);
            //Read an HTML message body from an external file, convert referenced images to embedded,
            //convert HTML into a basic plain-text alternative body
            $this->mail->fillMessage($msg['content'], true);
      	
        	// отправляем письмо
        	$this->mail->send();
        } catch (Exception $e) {
        	echo $e->getMessage();
            // тут еще нужно писать в лог...
        	exit(0);
        }
        // удаляем письмо из очереди
        Mail_Mailouts::delete($msg['id']);
      }  
        
    }
    

} 

$sm = new send_mails();

$sm->send();