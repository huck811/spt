<?
 
 class Resize_Image
  {
  private $image;
  private $width;
  private $height;
  private $w_crop;
  private $h_crop;
  private $type;
  var $process;
 
  function __construct( $file )
   {
   if ( ! file_exists( $file ) ) {
    exit( '##File does not exist' );
    }
   if ( ! $this->setType( $file ) ) {
    exit( '##File is not an image' );
    }
   $this->openImage( $file ); 
   $this->setSize(  );
   }
 
  private function setType( $file )
   {
   $pic = @getimagesize( $file );
   switch( $pic['mime'] )
    {
    case 'image/jpeg':
     $this->type = 'jpg';
     return true;
    case 'image/png':
     $this->type = 'png';
     return true;
    case 'image/gif':
     $this->type = 'gif';
     return true;
    default:
     return false;
    }
   }
 
  private function openImage( $file )
   {
   switch( $this->type )
    {
    case 'jpg':
     $this->image = @imagecreatefromJpeg( $file );
     break;
    case 'png':
     $this->image = @imagecreatefromPng( $file );
     break;
    case 'gif':
     $this->image = @imagecreatefromGif( $file );
     break;
    default:
     exit( '##File is not an image' );
    }
   }
 
  private function setSize(  )
   {
   $this->width	= imageSX( $this->image );
   $this->height	= imageSY( $this->image );
   }
   
  public function getSize( )
  {
   $this->setSize(); 
   return array('width' => $this->width, 'height' => $this->height);
  } 
 
  function resize( $width = false, $height = false )
   {
    
    /*if (($height > $width && $this->height <  $this->width) || ($height < $width && $this->height >  $this->width)){
        $tmp = $height;
        $height = $width;
        $width = $tmp; 
    }*/
    
    
    if (/*$this->width <= $width &&*/ $this->height <= $height)
        return $this;
        
    //$this->w_crop = $width < $this->width ? $width :  $this->width;
    //$this->h_crop = $height < $this->height ? $height :  $this->height;
    if ($width && $height){
        if ($this->width > $this->height)
            $width = 0;
        else
            $height = 0;
            
    }
   //echo $width;
   if ( is_numeric( $width ) && is_numeric( $height ) && $width > 0 && $height > 0 ) {
    $newSize = $this->getSizeByFramework( $width, $height );
    }
    elseif ( is_numeric( $height ) && $height > 0 ) {
    $newSize = $this->getSizeByHeight( $height );
    
    }
   elseif ( is_numeric( $width ) && $width > 0 ) {
    $newSize = $this->getSizeByWidth( $width );
    }
   
   else {
    $newSize = array( $this->width, $this->height );
    }
    
   
   //print_r($newSize);
   $newImage = imagecreatetruecolor( $newSize[0], $newSize[1] );
   imagecopyresampled( $newImage, $this->image, 0, 0, 0, 0, $newSize[0], $newSize[1], $this->width, $this->height );
   $this->image = $newImage;
   $this->setSize(  );
   
   return $this;
   }
  
  public function crop($w_o, $h_o){
    // crop
   //$w_o = $this->w_crop;
   //$h_o = $this->h_crop;
   //echo $w_o.'--'.$h_o.'**'.$this->width;
   $x_o = 0;
   if ($this->width > $w_o){
      $x_o = round(($this->width - $w_o)/2);
      $y_o = 0;
         
   }
   
   
   if ($this->height > $h_o){
      $y_o = round(($this->height - $h_o)/2); 
   }
   //echo $x_o.'--'.$y_o;
   if ($x_o || $y_o){
       $newImage = imagecreatetruecolor($w_o, $h_o);
       imagecopy($newImage, $this->image, 0, 0, $x_o, $y_o, $w_o, $h_o);
       $this->image = $newImage;
       $this->setSize( );
   }
   return $this;
  } 
 
  private function getSizeByFramework( $width, $height )
   {
   /*if ( $this->width <= $width && $this->height <= $height ) {
    return array( $this->width, $this->height );
    }
   if ( $this->width / $width > $this->height / $height ) {
    $newSize[0] = $width;
    $newSize[1] = round( $this->height * $width / $this->width );
    }
   else {
    $newSize[0] = round( $this->width * $height / $this->height );
    $newSize[1] = $height;
    }*/
    
    if ($this->width > $this->height){
        $newSize[1] = $width;
        $newSize[0] = round( $this->height * $width / $this->width );
    }
    else{
        $newSize[0] = $height;
        $newSize[1] = round( $this->width * $height / $this->height );
    }
   return $newSize;
   }
 
  private function getSizeByWidth( $width )
   {
   if( $width >= $this->width ) {
    return array( $this->width, $this->height );
    }
   $newSize[0] = $width;
   $newSize[1] = round( $this->height * $width / $this->width );
   return $newSize;
   }
 
  private function getSizeByHeight( $height )
   {
   if ( $height >= $this->height ) {
    return array( $this->width, $this->height );
    }
   $newSize[1] = $height;
   $newSize[0] = round( $this->width * $height / $this->height );
   return $newSize;
   }
 
  function save( $path = '', $fileName, $quality = 100 )
   {
    
   if ( trim( $fileName ) == '' || $this->image === false ) {
    return false;
   }
   if ( ! is_dir( $path ) ) {
    return false;
    }
   if ( ! is_numeric( $quality ) || $quality < 0 || $quality > 100) {
    $quality = 100;
    }
   $savePath = $path.trim( $fileName ).'.jpg';
   $this->process = imagejpeg( $this->image, $savePath, $quality );
   $this->process = array('result' => $this->process, 'new_file_path' => $savePath);
   return true;
   }
   
   function watermark($wm = 'images/watermark.png')
   {
     $stamp = imagecreatefromPng( $wm );
     $sx = imageSX( $stamp );
     $sy = imageSY( $stamp );
     $marge_right  = ($this->width - $sx)/2;
     $marge_bottom = ($this->height - $sy)/2;
     imagealphablending( $stamp, true );
     imagealphablending( $this->image, true );
     imagecopy( $this->image, $stamp, $this->width - $sx - $marge_right, $this->height - $sy - $marge_bottom, 0, 0, $sx, $sy );
     return $this;
   }
   
   function get_name($file){
        
      $fn = explode('.',$file);
      unset($fn[count($fn)-1]);
      $fn = implode('.',$fn);
      return $fn;
   }
   
   function is_ok(){
        if($this->process['result'] && $this->process['new_file_path']) {
           return true;
        }
        return false;
   }
   
   function result (){
        return $this->process;
   }
  }
 ?> 