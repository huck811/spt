<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';
require_once "geo/cache.php";

$user =& JFactory::getUser();
$user_id = $user->get('id');



$ads_user = new ads_user();
$ads_user->get_user_data($user_id);
if (!$user_id || !$ads_user->is_user_register()){
    if (!$user_id)
        echo 'Чтобы подать объявление нужно авторизоваться на сайте!';
    else    
        echo '<strong>Вы не сможете подать объявление пока не заполните все обязательные поля в профиле! (<a href="'.URL_PROFILE.'" >Профиль</a>)</strong>';
}
else
{
    
    
    $form_c = new FormConstructor();
    $city = new Cities();
    $category = new Ads_Category();
    $ads = new Ads_AdsManage();
    $ads_id = htmlspecialchars($_GET['ads']);
    $ads->get_ads($user_id, $ads_id);
    
    $info = $category->get_arr_category_tree_cat_id($ads->category);
    $razdel_id = $info[0]['id_tree'];
    $main_img = '';
    
    if ($ads->main_thumb_image){
        $main_img = '<img src="/images/com_ads/'.$ads->main_thumb_image.'" file="'.$ads->main_image.'" /><a href="#" class="del" onClick="_delete(jQuery(this)); return false;">Удалить</a>';
    
        $thumb_images = $ads->thumb_images;
        $images = $ads->images;
        
        $arr_img = array();
    
        for($i=0; $i<count($thumb_images); $i++){
            $arr_img[$i] = '<img src="/images/com_ads/'.$thumb_images[$i].'" file="'.$images[$i].'" /><a href="#" class="del" onClick="_delete(jQuery(this)); return false;">Удалить</a>';
        }

    }    
    $year = array();
    $narabotka = array();
    $probeg = array();
    $year[]=DEFAULT_SELECT_VAL;
    for ($i=MAX_YEAR; $i>MIN_YEAR; $i--)
        $year[]=$i;
    
    for ($i=MIN_NARABOTKA; $i<=MAX_NARABOTKA; $i+=STEP_NARABOTKA)
        $narabotka[]=$i; 

    for ($i=MIN_PROBEG; $i<=MAX_PROBEG; $i+=STEP_PROBEG)
        $probeg[]=$i;
    
    $nar_label = WHAT_WORK_HOUR_LABEL;
        
    if ($razdel_id == CAT_ID_TRASPORT){
        $narabotka = $probeg;
        $nar_label = WHAT_WORK_HOUR_TRANSPORT_LABEL;
    }           
    //echo $city->get_select_list_cities();
    ?>
    
    <script type="text/javascript">
        <?php include "validate.js";?>
        <?php include "upload.js";?>
        <?php include "script.js";?>
    </script>
    <table cellpadding="0" cellspacing="0" class="t_form_add_ads" width="100%" style="min-width: 815px;">
        <tr>
            <td valign="top">
                <div class="img_cont">
                   		<ul id="files" >
                                <li class="<?php if ($main_img==''){?>no_photo<?php }?> main"><?=$main_img?></li>
                                <?php for($i=0; $i<(MAX_PHOTO_ITEMS-1); $i++){?>
                                    <li class="<?php if (!isset($arr_img[$i])){?>no_photo<?php }?> small <?php if ($i%2){?> right<?php }?>"><?=$arr_img[$i]?></li>
                                <?php }?>
                        </ul>
                </div>
                <span id="status" ></span>
            </td>
            <td valign="top" width="529">
                <form class="form_add_ads" method="POST" id="form_add_ads">
                 <?php 
                 echo $form_c
                             ->type()
                             ->label(HEADLINE_LABEL)
                             ->name('head')
                             ->init($ads->head)
                             ->attr('maxlength="50"')
                             ->valid('Заполните заголовок...')
                             ->insert();
 
                 echo $form_c
                             ->name('load')
                             ->type('INPUT','button')
                             ->init(BTN_LOAD_IMAGE_VAL)
                             ->_class('upload')
                             ->label(LOAD_IMAGE_LABEL)
                             ->id('upload')
                             ->insert();
                 ?>
                  
                 <div class="block_in info" style="color: #cc0000;"><i><?php echo sprintf(MESSAGE_WRONG_IMAGE_FORMAT,MAX_FILE_SIZE,MAX_WIDTH)?></i></div>
                 <?php echo $category->get_list_category('category',CATEGORY_LABEL,'category','',array(DEFAULT_SELECT_VAL),$ads->parent);?>
                  <?php if ($ads_id) echo $category->get_list_sub_category('subcategory',CATEGORY_SUB_LABEL,'subcategory','',array(DEFAULT_SELECT_VAL),$ads->category,$ads->parent);?>
                 
                 <?php 
                 if (!$ads_id)
                    echo $form_c
                                ->type('SELECT')
                                ->init(DEFAULT_SELECT_VAL)
                                ->label(CATEGORY_SUB_LABEL)
                                ->valid('Выберите подкатегорию...')
                                ->name('subcategory')
                                ->insert();
                 
                 if ($ads->producer){
                    echo $category->get_list_producer('','Производитель:','producer','', array('Выбрать'),$ads->category,$ads->producer,'Выберите производителя...');
                 }   
                 else
                    echo $form_c
                                ->type('SELECT')
                                ->init(DEFAULT_SELECT_VAL)
                                ->label(PRODUCER_LABEL)
                                ->valid('Выберите производителя...')
                                ->name('producer')
                                ->insert();
                 ?> 
                 <?php 
                    echo $form_c
                                ->type()
                                ->label(MODEL_LABEL)
                                ->init($ads->model)
                                ->name('model')
                                ->attr('maxlength="20"')
                                ->valid('Укажите модель...')
                                ->insert();
                 ?>

                 <div class="block_before_category"> 
                 <div class="left radiobox">
                     <?php 
                     $form_c
                            ->type('INPUT','radio')
                            ->init(RENT_ITEM_VAL)
                            ->_class('superstyle')
                            ->label(RENT_ITEM_LABEL)
                            ->nowrapp()
                            ->lright()
                            ->valid('Укажите цену аренды...','v')
                            ->name('ad_kindof')
                            ->id('rent_radio');
    
                     if ($ads->ad_kindof=='сдам'){
                        $form_c->checked();
                        $hide = '';
                        $hide1 = 'hide';
                     }   
                     echo $form_c->insert();
                     
                     $form_c
                            ->type('INPUT','radio')
                            ->init(SELL_ITEM_VAL)
                            ->_class('superstyle')
                            ->label(SELL_ITEM_LABEL)
                            ->nowrapp()->lright()
                            ->valid('Укажите цену...','v1')
                            ->name('ad_kindof')
                            ->id('sell_radio');
                     
                     if ($ads->ad_kindof!='сдам'){
                        $form_c->checked();
                        $hide1 = '';
                        $hide = 'hide';
                     }   
      
                     echo $form_c->insert();
                     
                     ?>
                     
                 </div>
                 <div id="cost_sell">
                    <?php 
                    echo $form_c
                                ->type()
                                ->_class($hide1.' cost left')
                                ->init($ads->cost1)
                                ->label(COST_LABEL)
                                ->valid('Не верный формат цены...','v1','NUMERIC')
                                ->name('cost1')
                                ->insert();
                    ?>
                 </div>  
                 <div id="cost_rent">
                    <?php 
                    echo $form_c
                                ->type()
                                ->_class($hide.' cost cost1 left')
                                ->init($ads->cost_smena)
                                ->label(COST_LABEL_1)
                                ->valid('Не верный формат цены...','v','NUMERIC')
                                ->name('cost_smena')
                                ->insert();
                    ?>
                    <?php 
                    echo $form_c
                                ->type()
                                ->_class($hide.' cost cost2 left')
                                ->init($ads->cost_hour)
                                ->label(COST_LABEL_2)
                                ->valid('Не верный формат цены...','v','NUMERIC')
                                ->name('cost_hour')
                                ->insert();
                    ?>
                 </div> 
                 <hr />
                 <br />  
                 <?php 
                    if ($ads->state)
                        $hide3 = '';
                    else 
                        $hide3 = 'hide';       
                 ?> 
                 <div class="left radiobox new_bu <?php echo $hide1?>">
                     <?php
                      $form_c
                              ->type('INPUT','radio')
                              ->init(NEW_ITEM_VAL)
                              ->_class('superstyle')
                              ->label(NEW_ITEM_LABEL)
                              ->name('state')
                              ->id('new')
                              ->nowrapp()
                              ->lright();
                      
                      if (!$ads->state)
                        $form_c->checked();
                        
                      echo $form_c->insert();
                      $form_c
                             ->type('INPUT','radio')
                             ->init(OLD_ITEM_VAL)
                             ->_class('superstyle')
                             ->label(OLD_ITEM_LABEL)
                             ->name('state')
                             ->valid('Укажите наработку в часах...','v3')
                             ->id('bu')
                             ->nowrapp()
                             ->lright();
                             
                      if ($ads->state)
                        $form_c->checked();
                        
                      echo $form_c->insert();
                      
                      if ($ads->state){ 
                        echo '<script>jQuery(document).ready(function(){jQuery("#bu").click();});</script>';
                      }
                      ?>
                 </div>
                 <div id="moto_hour" class="<?php echo $hide1?> group_2">
                    <?php echo $form_c
                                      ->clr()
                                      ->type('INPUT')
                                      ->_class('left moto '.$hide3)
                                      ->init($ads->moto_hour)
                                      ->label($nar_label)
                                      ->valid('Не верный формат наработки...','v3','NUMERIC')
                                      ->name('moto_hour')
                                      ->insert();
                    ?>
                    
                    <?php echo $form_c
                                      ->clr()
                                      ->type('SELECT')
                                      ->_class('left probeg hide')
                                      ->init($probeg)
                                      ->num_format(1)
                                      ->name('probeg')
                                      ->insert();
                    ?>
                 </div> 
                 <?php echo $form_c
                                   ->type('SELECT')
                                   ->init($year)
                                   ->_class('year left group_2 '.$hide3)
                                   ->selected($ads->year)
                                   ->label(YEAR_BORN_LABEL)
                                   ->valid('Выберите год выпуска...')
                                   ->name('year')
                                   ->insert();
                 ?>
                 <?php 
                 $region = $city->get_ar_region_by_city_id($ads->city);
                 echo $city
                           ->set_valid_region('Выберите регион...')
                           ->get_ar_regions('',REGION_LABEL,"region",'', array(DEFAULT_SELECT_VAL),$region); 
                 ?>
                 <?=$city
                         ->set_valid_city('Выберите город...')
                         ->get_cities_by_region('', CITY_LABEL, "city", "", array(DEFAULT_SELECT_VAL),$region,$ads->city,'Выберите город...')
                 ?>
                 <?php echo $form_c
                                   ->type('TEXTAREA')
                                   ->_class('comment')
                                   ->init($ads->comment)
                                   ->label(COMMENT_LABEL)
                                   ->name('comment')
                                   ->insert();
                 ?> 
                 
                 <div class="block_in left radiobox status">
                     <span style="font-size: 13px;">Статус объявления:<label></label></span>
                     <?php 
                     $form_c
                            ->type('INPUT','radio')
                            ->init(STATE_ACTIVE_ADS_VAL)
                            ->_class('superstyle')
                            ->label(STATE_ACTIVE_ADS_LABEL)
                            ->nowrapp()
                            ->lright()
                            ->name('status')
                            ->checked();
                                                        
                     if (($ads_id && $ads->status) || !$ads_id)
                        $form_c->checked();
                     echo $form_c->insert();
                     
                     $form_c
                            ->type('INPUT','radio')
                            ->init(STATE_PASSIVE_ADS_VAL)
                            ->_class('superstyle')
                            ->label(STATE_PASSIVE_ADS_LABEL)
                            ->nowrapp()
                            ->lright()
                            ->name('status');
                            
                     if (($ads_id && !$ads->status))
                        $form_c->checked();
                     echo $form_c->insert();
                     ?>
                     
                     <span style="display: inline-block; width: 80px; height: 20px;"></span>
                 </div>
                 <!--<div class="block_in">
                    <a href="#"><?=LINK_ADS_MAKE_UP?></a> »<a href="#"><?=LINK_ADS_MAKE_TOP?></a> »<a href="#"><?=LINK_ADS_MAKE_SPECIAL?></a> »
                 </div>-->
                 </div>
                 <div class="block_in submit">
                     <?php 
                     echo $form_c
                                 ->type('INPUT','submit')
                                 ->init('Сохранить')
                                 ->label()->nowrapp()
                                 ->insert();
                     ?>
                     <?php 
                     echo $form_c
                                  ->type('INPUT','reset')
                                  ->init('Удалить')
                                  ->label()
                                  ->nowrapp()
                                  ->insert();
                     ?>
                 </div>
                 <input type="hidden" name="user_id" value="<?php echo $user_id;?>" />
                 <input type="hidden" name="images" value="" />
                 <input type="hidden" name="ads_id" value="<?=$ads_id?>" />
                </form>
            </td>
        </tr>
    </table>
    <?php include "script.js.php";?>
    
<?php }?>