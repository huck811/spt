<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';
require_once "geo/cache.php";

$user = & JFactory::getUser();
$user_id = $user->get('id');

$isAdmin = $user->get('isRoot');

$ads_id = htmlspecialchars($_GET['ads']);

$ads = new Ads_AdsManage();


$ads_user = new ads_user();
$ads_user->get_user_data($user_id);
if (!$user_id || !$ads_user->is_user_register()) {
    if (!$user_id){
        header('Location: '.BASE_URL.'log-in.html?addads'); 
        exit();
    }    
    else
        echo '<strong>Вы не сможете подать объявление пока не заполните все обязательные поля в профиле! (<a href="' . URL_PROFILE . '" >Профиль</a>)</strong>';
}
else if ($ads->ads_is_selled(false, $ads_id)) {
    echo '<strong>Объявление продано! Редактирование не возможно!</strong>';
} else {


    $form_c = new FormConstructor();
    $city = new Cities();
    $category = new Ads_Category();


    if ($isAdmin)
        $ads->get_ads(false, $ads_id);
    else
        $ads->get_ads($user_id, $ads_id);

    $info = $category->get_arr_category_tree_cat_id($ads->category);
    $razdel_id = $info[0]['id_tree'];



    $main_img = '';

    if ($ads->main_thumb_image) {
        $main_img = '<img src="/images/com_ads/' . $ads->main_thumb_image . '" file="' . $ads->main_image . '" /><a href="#" class="del" onClick="_delete(jQuery(this)); return false;">Удалить</a>';

        $thumb_images = $ads->thumb_images;
        $images = $ads->images;

        $arr_img = array();

        for ($i = 0; $i < count($thumb_images); $i++) {
            $arr_img[$i] = '<img src="/images/com_ads/' . $thumb_images[$i] . '" file="' . $images[$i] . '" /><a href="#" class="del" onClick="_delete(jQuery(this)); return false;">Удалить</a>';
        }
    }
    $year = array();
    $narabotka = array();
    $probeg = array();
    $year[] = DEFAULT_SELECT_VAL;
    for ($i = MAX_YEAR; $i > MIN_YEAR; $i--)
        $year[] = $i;

    for ($i = MIN_NARABOTKA; $i <= MAX_NARABOTKA; $i+=STEP_NARABOTKA)
        $narabotka[] = $i;

    for ($i = MIN_PROBEG; $i <= MAX_PROBEG; $i+=STEP_PROBEG)
        $probeg[] = $i;

    $nar_label = WHAT_WORK_HOUR_LABEL;

    if ($razdel_id == CAT_ID_TRASPORT) {
        $narabotka = $probeg;
        $nar_label = WHAT_WORK_HOUR_TRANSPORT_LABEL;
    }
    //echo $city->get_select_list_cities();
    ?>
    <script type="text/javascript">
    <?php include "validate.js"; ?>
    <?php include "upload.js"; ?>
    <?php include "script.js"; ?>
    </script>
    <?php
    $hide3 = '';
    $head = $form_c
            ->type()
            ->label(HEADLINE_LABEL)
            ->name('head')
            ->init($ads->head)
            ->attr('maxlength="50"')
            ->valid('Заполните заголовок...')
            ->insert();
    $btn_load = $form_c
            ->name('load')
            ->type('INPUT', 'button')
            ->init(BTN_LOAD_IMAGE_VAL)
            ->_class('upload')
            ->label(LOAD_IMAGE_LABEL)
            ->id('upload')
            ->insert();
    $list_category = $category->get_list_category('category', CATEGORY_LABEL, 'category', '', array(DEFAULT_SELECT_VAL), $ads->parent);

    if ($ads_id)
        $list_sub_category = $category->get_list_sub_category('subcategory', CATEGORY_SUB_LABEL, 'subcategory', '', array(DEFAULT_SELECT_VAL), $ads->category, $ads->parent);
    else
        $list_sub_category = $form_c
                ->type('SELECT')
                ->init(DEFAULT_SELECT_VAL)
                ->label(CATEGORY_SUB_LABEL)
                ->valid('Выберите подкатегорию...')
                ->name('subcategory')
                ->insert();


    if ($ads->producer) {
        $list_producer = $category->get_list_producer('', 'Производитель:', 'producer', '', array('Выбрать'), $ads->category, $ads->producer, 'Выберите производителя...');
    } else
        $list_producer = $form_c
                ->type('SELECT')
                ->init(DEFAULT_SELECT_VAL)
                ->label(PRODUCER_LABEL)
                ->valid('Выберите производителя...')
                ->name('producer')
                ->insert();

    $np_label = '';
    $new_producer = '';
    $np_text = '';
    if (!$ads_id) {
        $new_producer = $form_c
                ->name('new_producer')
                ->type('INPUT', 'button')
                ->init(NEW_PRODUCER_VALUE)
                ->attr('maxlength="30"')
                ->_class('new_producer')
                ->id('new_producer')
                ->insert();
        $np_text = "<a class='hide_new_producer' href='#' style='position:relative; top:-23px; left:-5px; font-size:11px; color:#000; display:none;'>[скрыть]</a>";
        $np_label = '<div class="block_in info info1" style="color: #cc0000;"><i>' . NEW_PRODUCER_LABEL . '</i></div>';
    }


    $model = $form_c
            ->type()
            ->label(MODEL_LABEL)
            ->init($ads->model)
            ->name('model')
            ->attr('maxlength="20"')
            ->valid('Укажите модель...')
            ->insert();
    $form_c
            ->type('INPUT', 'radio')
            ->init(RENT_ITEM_VAL)
            ->_class('superstyle')
            ->label(RENT_ITEM_LABEL)
            ->nowrapp()
            ->lright()
            ->valid('Укажите цену аренды...', 'v')
            ->name('ad_kindof')
            ->id('rent_radio');
    if ($ads->ad_kindof == 'сдам') {
        $form_c->checked();
        $hide = '';
        $hide1 = 'hide';
    }

    $rent_checkbox = $form_c->insert();

    $form_c
            ->type('INPUT', 'radio')
            ->init(SELL_ITEM_VAL)
            ->_class('superstyle')
            ->label(SELL_ITEM_LABEL)
            ->nowrapp()->lright()
            ->valid('Укажите цену...', 'v1')
            ->name('ad_kindof')
            ->id('sell_radio');

    if ($ads->ad_kindof != 'сдам') {
        $form_c->checked();
        $hide1 = '';
        $hide = 'hide';
    }

    $sell_checkbox = $form_c->insert();

    $cost_sell = $form_c
            ->type()
            ->_class($hide1 . ' cost left')
            ->init($ads->cost1)
            ->label(COST_LABEL)
            ->valid('Не верный формат цены...', 'v1', 'NUMERIC')
            ->name('cost1')
            ->insert();
    $indogovor = $form_c
            ->type('INPUT', 'checkbox')
            ->_class('superstyle')
            ->label('По договоренности')
            ->init(1)
            ->name('indogovor')
            ->id('indogovor')
            ->nowrapp()
            ->lright()
            ->insert();

    $cost_rent = $form_c
            ->type()
            ->_class($hide . ' cost cost1 left')
            ->init($ads->cost_smena)
            ->label(COST_LABEL_1)
            ->valid('Не верный формат цены...', 'v', 'NUMERIC')
            ->name('cost_smena')
            ->insert();
    $cost_rent2 = $form_c
            ->type()
            ->_class($hide . ' cost cost2 left')
            ->init($ads->cost_hour)
            ->label(COST_LABEL_2)
            ->valid('Не верный формат цены...', 'v', 'NUMERIC')
            ->name('cost_hour')
            ->insert();
    /* if ($ads->state)
      $hide3 = '';
      else
      $hide3 = 'hide'; */

    $form_c
            ->type('INPUT', 'radio')
            ->init(NEW_ITEM_VAL)
            ->_class('superstyle')
            ->label(NEW_ITEM_LABEL)
            ->name('state')
            ->id('new')
            ->nowrapp()
            ->lright();

    if (!$ads->state)
        $form_c->checked();

    $radiobox_new = $form_c->insert();

    $form_c
            ->type('INPUT', 'radio')
            ->init(OLD_ITEM_VAL)
            ->_class('superstyle')
            ->label(OLD_ITEM_LABEL)
            ->name('state')
            ->valid('Укажите наработку в часах...', 'v3')
            ->id('bu')
            ->nowrapp()
            ->lright();

    if ($ads->state)
        $form_c->checked();

    $radiobox_bu = $form_c->insert();

    if ($ads->state) {
        $radiobox_bu .= PHP_EOL . '<script>jQuery(document).ready(function(){jQuery("#bu").click();});</script>';
    }

    $motoh = $form_c
            ->clr()
            ->type('INPUT')
            ->_class('left moto ' . $hide3)
            ->init($ads->moto_hour)
            ->label($nar_label)
            ->valid('Не верный формат наработки...', 'v3', 'NUMERIC')
            ->name('moto_hour')
            ->insert();
    $probeg = $form_c
            ->clr()
            ->type('SELECT')
            ->_class('left probeg hide')
            ->init($probeg)
            ->num_format(1)
            ->name('probeg')
            ->insert();
    $year_field = $form_c
            ->type('SELECT')
            ->init($year)
            ->_class('year left group_2 ' . $hide3)
            ->selected($ads->year)
            ->label(YEAR_BORN_LABEL)
            ->valid('Выберите год выпуска...')
            ->name('year')
            ->insert();
    $region = $city->get_ar_region_by_city_id($ads->city ? $ads->city : $ads_user->ad_city);
    $region_field = $city
            ->set_valid_region('Выберите регион...')
            ->get_ar_regions('', REGION_LABEL, "region", '', array(DEFAULT_SELECT_VAL), $region);

    $city_field = $city
            ->set_valid_city('Выберите город...')
            ->get_cities_by_region('', CITY_LABEL, "city", "", array(DEFAULT_SELECT_VAL), $region, $ads->city ? $ads->city : $ads_user->ad_city, 'Выберите город...');

    $comment_field = $form_c
            ->type('TEXTAREA')
            ->_class('comment')
            ->init($ads->comment)
            ->label(COMMENT_LABEL)
            ->attr('maxlength="1500"')
            ->name('comment')
            ->insert();

    if (($ads_id && !$ads->on_moderate()) || !$ads_id) {
        $form_c
                ->type('INPUT', 'radio')
                ->init(STATE_ACTIVE_ADS_VAL)
                ->_class('superstyle')
                ->label(STATE_ACTIVE_ADS_LABEL)
                ->nowrapp()
                ->lright()
                ->name('status')
                ->checked();

        if (($ads_id && $ads->status) || !$ads_id)
            $form_c->checked();
        $status_active_field = $form_c->insert();


        $form_c
                ->type('INPUT', 'radio')
                ->init(STATE_PASSIVE_ADS_VAL)
                ->_class('superstyle')
                ->label(STATE_PASSIVE_ADS_LABEL)
                ->nowrapp()
                ->lright()
                ->name('status');

        if (($ads_id && !$ads->status))
            $form_c->checked();
        $status_passive_field = $form_c->insert();
    }
    else {
        $status_passive_field = '<span class="red">Объявление находится на модерации!!!</span>';
    }



    $submit_btn = $form_c
            ->type('INPUT', 'submit')
            ->init('Сохранить')
            ->_class('button_g')
            ->label()
            ->nowrapp()
            ->insert();
    $delete_btn = $form_c
            ->type('INPUT', 'reset')
            ->init('Удалить')
            ->_class('button_y')
            ->label()
            ->nowrapp()
            ->insert();

    include 'default.phtml';
    include "script.js.php";
    ?>   


<?php
}?>