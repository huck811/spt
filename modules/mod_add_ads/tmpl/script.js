    // использование Math.round() даст неравномерное распределение!
    function getRandomInt(min, max)
    {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function num_photo(){
        return jQuery('#files li').length - jQuery('#files li.no_photo').length;
    }
    
    function restruct_img(){
      var imgs = new Array();
      var num_li = jQuery('#files li').length;
      
      jQuery('#files li').each(function(){
        if (jQuery(this).html()){
            imgs[imgs.length] = jQuery(this).html();
            jQuery(this).html('');
            jQuery(this).addClass('no_photo');
         }   
      });
      
      for (i=0; i < imgs.length; i++){
        jQuery('#files li').eq(i).removeClass('no_photo').html(imgs[i]);  
      }
      
        
    }
    
    function _delete(obj){ // Удаляем картинку по клику.
      var status=jQuery('#system-message-container'); 
      var li = obj.parent(); // запоминаем контейнер картинки
      src = li.find('img:first').attr('src'); // получаем путь к файлу
      var first_li_small = jQuery('#files li.small img:first');

      jQuery.get("/modules/mod_add_ads/upload/delete-file.php?img="+src+'&rand='+getRandomInt(1,10000), function(response){ // запрос на удаление
          response = response.split('##');
          if(response[0]==="success"){ // успех
                li.html(''); // очищаем содержимое контейнера с фото
                li.addClass('no_photo');  
                restruct_img();    
          }
          else{ // удалить не получилось
            status.html('<div class="inner">'+response[0]+response[1]+'</div>');
          }
          
      });
       
      return false;
    }
    
    function add_img_to_list(folder, prefix,file, _new){
        
                
        if (num_photo()==<?php echo MAX_PHOTO_ITEMS;?>)
            return false;
        if (jQuery('#files li.main').html()==''){
            jQuery('#files li.main').html('<img src="'+_new+'" alt="" file="'+folder+'/'+file+'" /><a href="#" class="del" onClick="_delete(jQuery(this)); return false;">Удалить</a>');
            jQuery('#files li.main').removeClass('no_photo');
        }    
        else{
            var first = false;
            jQuery('#files li.small').each(function(){
                if (jQuery(this).html()=='' && !first){
                    jQuery(this).html('<img src="'+_new+'" alt="" file="'+folder+'/'+file+'" /><a href="#" class="del" onClick="_delete(jQuery(this)); return false;">Удалить</a>');
                    jQuery(this).removeClass('no_photo');
                    first = true;
                }    
            });
        } 
        return true;   
    }
    
    jQuery(document).ready(function(){
        
        jQuery.listen("click", "#files li.small img", function(){
            if (jQuery(this).parent().hasClass('small')){
                main = jQuery('#files li.main').html();
                small = jQuery(this).parent().html();
                jQuery(this).parent().html(main);
                jQuery('#files li.main').html(small);
            }
            
        });
        
        jQuery("select[name=region]").change(function(){ // при смене региона подгружаем список городов этого региона
            var status=jQuery('#system-message-container');  
            if (jQuery(this).val()=='Выбрать'){
                jQuery("select[name=city]").html('<option>Выбрать</option>');
            }
            else{
               
              jQuery.get("/modules/mod_add_ads/upload/get_list_city.php?region="+encodeURIComponent(jQuery(this).val()), function(response){ // запрос на список городов по региону
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     jQuery("select[name=city]").html(response[1]);
                  }
                  else{ // удалить не получилось
                    status.html('<div class="inner">'+response[0]+response[1]+'</div>');
                  }
                  
              });
            }    
        });
        
        // Авто-подгрузка категорий и производителей
        
        jQuery("select[name=category]").change(function(){ // при смене категории подгружаем список подкатегорий
            var status=jQuery('#system-message-container');  
            if (jQuery(this).val()=='Выбрать'){
                jQuery("select[name=subcategory]").html('<option>Выбрать</option>');
            }
            else{
              jQuery.get("/modules/mod_add_ads/upload/get_list_producer.php?category="+jQuery(this).val()+'&get_subcategory&default_val='+'<?=DEFAULT_SELECT_VAL?>&'+getRandomInt(1,50000), function(response){ // запрос списка субкатегорий
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     jQuery("select[name=subcategory]").html(response[1]);
                  }
                  else{ // не получилось
                    status.html('<div class="inner">'+response[0]+response[1]+'</div>');
                  }
                  
              });
            }  
            
            if (jQuery('option:selected',this).parent().attr('cid')==<?=CAT_ID_TRASPORT;?>){ // Если выбрана категория транспорт, по меняем название поля "наработка" на "пробег"
                jQuery('div.block_in.moto label').html('<?=WHAT_WORK_HOUR_TRANSPORT_LABEL;?>');
                jQuery('div.block_in.moto').css('padding-left','54px');
            }    
            else{
                jQuery('div.block_in.moto label').html('<?=WHAT_WORK_HOUR_LABEL;?>'); 
                jQuery('div.block_in.moto').css('padding-left','27px'); 
            }      
             
                
        });
        
        
        jQuery("select[name=subcategory]").change(function(){ // при смене категории подгружаем список производителей
            var status=jQuery('#system-message-container');  
            if (jQuery(this).val()=='<?=DEFAULT_SELECT_VAL?>'){
                jQuery("select[name=producer]").html('<option><?=DEFAULT_SELECT_VAL?></option>');
            }
            else{
              jQuery.get("/modules/mod_add_ads/upload/get_list_producer.php?category="+jQuery(this).val(), function(response){ // запрос списка производителей
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     jQuery("select[name=producer]").html(response[1].replace(/<?=SELECT_PRODUCER?>/g, '<?=DEFAULT_SELECT_VAL?>'));
                  }
                  else{ // не получилось
                    status.html('<div class="inner">'+response[0]+response[1]+'</div>');
                  }
                  
              });
            }    
        });
        
        /*function change_valid_error(type){
            if (type == 'hide'){
                ve = jQuery('.block_in.year select').attr('validate_error');
                jQuery('.block_in.year select').removeAttr('validate_error');
                jQuery('.block_in.year select').attr('validate_error1', ve);
            }    
            else{
                ve = jQuery('.block_in.year select').attr('validate_error1');
                jQuery('.block_in.year select').removeAttr('validate_error1');
                jQuery('.block_in.year select').attr('validate_error', ve);
            }
        }*/
        function hide_fields(){
            if(jQuery('#bu').prop('checked')){
                jQuery('.group_2').removeClass('hide');
                //change_valid_error('show');
            }
            else{
                jQuery('.group_2').addClass('hide');
                //change_valid_error('hide');
            }
        }
        function hide_chkbox(){
            if (jQuery('#rent_radio').prop('checked')){
                jQuery('.new_bu, .group_2').addClass('hide');
                //change_valid_error('hide');
                
            }
            else{
                jQuery('.new_bu, .group_2').removeClass('hide');
                hide_fields();
            }
                
        }
        // обрабатываем клик по радиобоксам
        jQuery(document).ready(function(){
            var v_e_year = '';
            jQuery('#rent_radio-wrapp').click(function(){
                if (jQuery(this).attr('disabled'))
                    return false;
                hide_chkbox();    
                jQuery('.block_in.cost').addClass('hide');
                jQuery('.block_in.cost1, .block_in.cost2').removeClass('hide');
                
                
                v_e_year = jQuery('.block_in.year select').attr('validate_error');
                
                            
            });
            
            jQuery('#sell_radio-wrapp').click(function(){
                if (jQuery(this).attr('disabled'))
                    return false;
                hide_chkbox();      
                jQuery('.block_in.cost').removeClass('hide');
                jQuery('.block_in.cost1, .block_in.cost2').addClass('hide');                
                jQuery('.block_in.year select').attr('validate_error', v_e_year);
            });
            
            jQuery('#bu-wrapp, #new-wrapp').click(function(){
                hide_chkbox();            
            });
            
            hide_chkbox();
        });
        // валидация и отправка формы
        jQuery('#form_add_ads').submit(function (){
            
            var status=jQuery('#system-message-container');
            jQuery('.form_add_ads input, .form_add_ads select').removeClass('red');
            error = '';
            
            jQuery('input[name=images]').val('');
            jQuery('#files li img').each(function(){
                jQuery('input[name=images]').val(jQuery('input[name=images]').val()+jQuery(this).attr('file')+'##');
            }); 
               
            if (error!='')
                status.html(error);
            else{
                jQuery('#rent_radio, #sell_radio, input[name="head"], select[name="category"], select[name="subcategory"], select[name="producer"]').removeAttr('disabled');
                jQuery('div.loading').fadeIn(200); 
                jQuery.post("/modules/mod_add_ads/upload/add_ads.php", $("#form_add_ads").serialize(),function(response){ // добавляем объявление
                  response = response.split('##');
                  //alert(response);
                  //jQuery('#rent_radio, #sell_radio').attr('disabled','disabled');
                  if(response[0]==="success"){ // успех
                     status.html('<div class="message">'+response[1]+'</div>');
                     
                     href = '<?=JRoute::_(URL_MY_ADS)?><?php echo isset($_GET['page']) ? '?page='.$_GET['page'] : ''?>';
                     if ((href.indexOf('?')+1)){
                        href += jQuery('#rent_radio:checked').length ? '&rent' : '';
                     }
                     else
                        href += jQuery('#rent_radio:checked').length ? '?rent' : '';
                     document.location.href = href;
                  }
                  else{ // добавить не получилось
                   
                    error = response[1];
                    status.html('<div class="inner">'+error+'</div>');
                    error = error.replace(new RegExp("<br/>",'g'),"");
                    alert(error);
                    if (response[2] != ''){
                        var fields_error = JSON.parse(response[2]);
                        for(i=0; i < fields_error.length; i++){
                            jQuery('.form_add_ads *[name="'+fields_error[i]+'"]').addClass('red');
                        }
                    }    

                    
                  }
                  jQuery('div.loading').fadeOut(200);
                });
                hide_chkbox();
                if (typeof(disable_top_fields) == 'function'){
                    disable_top_fields();
                }    
            }    
                
            return false;    
        });
        
        jQuery('input[name=model]').focus(function () {
                    var cache = {},
                    lastXhr;
                    jQuery(this).autocomplete({
                       minLength: 3,
                       source: function( request, response ) {
                          var term = request.term;
                          if ( term in cache ) {
                             response( cache[ term ] );
                             return;
                          }
                          
                          lastXhr = jQuery.get( "/modules/mod_add_ads/upload/get_model_dynamic.php?callback=?", request, function( data, status, xhr ) {
                             
                              cache[ term ] = data;
                             if ( xhr === lastXhr ) {
                                response( data );
                             }
                             
                          });
                           
                       },
                       select: function( event, ui ) {
                       
                      }
                    });
                 
            });
            
            function calc_num_count(_this){
                var maxLength = _this.attr('maxlength');
                var curLength = _this.val().length;

                var remaning = maxLength - curLength;
                
                if (remaning < 0) 
                    remaning = 0;
                
                if (remaning > maxLength)
                    remaning = maxLength;
                    
                jQuery('.form_add_ads .grippie .counter span').html(remaning);

            }
            
            calc_num_count(jQuery('.form_add_ads .comment textarea'));
            
            jQuery('.form_add_ads .comment textarea').bind('input propertychange',function(){
                calc_num_count(jQuery(this));
            });
            
            /*jQuery('#indogovor').change(function(){
                alert(jQuery(this).attr('value'));
            });*/
        
    });