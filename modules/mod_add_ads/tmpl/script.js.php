<?php if (!$ads_id){?>
<script type="text/javascript">
    jQuery('.block_before_category').hide();
</script>
<?php }?>
<?php if ($ads_id && !$isAdmin) { ?>    
    <script type="text/javascript">
        function disable_top_fields(){
            jQuery('input[name="head"], select[name="category"], select[name="subcategory"], select[name="producer"], #rent_radio, #sell_radio').attr('disabled', 'disabled');
        }
        disable_top_fields();
    </script>
    <?php
} else {
    ?>
    <script type="text/javascript">

        
        jQuery('select[name="category"]').change(function() {
            
            cid = jQuery('option:selected', this).parent().attr('cid');
            if (cid) {
                nar = jQuery('div.moto select[name="moto_hour"]').html();
                pr = jQuery('div.probeg select[name="probeg"]').html();
                if (cid == <?= CAT_ID_TRASPORT ?>) {
                    //alert();
                    jQuery('div.moto label').html('<?= WHAT_WORK_HOUR_TRANSPORT_LABEL ?>');
                    jQuery(this).addClass('pr');

                }
                else {
                    jQuery('div.moto label').html('<?= WHAT_WORK_HOUR_LABEL ?>');
                    jQuery(this).removeClass('');

                }

                prev_cid = jQuery(this).attr('prev_cid');

                if (prev_cid != cid && (cid == <?= CAT_ID_TRASPORT ?> || prev_cid == <?= CAT_ID_TRASPORT ?>)) {
                    jQuery('div.moto select[name="moto_hour"]').html(pr);
                    jQuery('div.probeg select[name="probeg"]').html(nar);
                }

                jQuery(this).attr('prev_cid', cid);
                jQuery('.block_before_category').show();

            }
            else
                jQuery('.block_before_category').hide();
        });

        function clik_new_prod() {

            if (jQuery('#new_producer').attr('type') == 'text')
                return false;
            val_new_prod = jQuery('#new_producer').val();
            name_new_prod = jQuery('#new_producer').attr('name');
            maxlength = jQuery('#new_producer').attr('maxlength');
            jQuery('#new_producer').parent().html('<label>Новый производитель: </label> <input type="text" id="new_producer" name="' + name_new_prod + '" maxlength="' + maxlength + '" OnClick="clik_new_prod();">');
            jQuery('#new_producer').val('');

            jQuery('select[name="producer"]').attr('disabled', 'disabled');

            jQuery('a.hide_new_producer').show();

            return val_new_prod;
        }

        jQuery(document).ready(function() {
            var val_new_prod = '';

            jQuery('#new_producer').click(function() {
                val_new_prod = clik_new_prod();
            });

            jQuery('a.hide_new_producer').click(function() {
                jQuery('#new_producer').attr('type', 'button');
                jQuery('#new_producer').val(val_new_prod);

                jQuery('#new_producer').parent().find('label').remove();

                jQuery('select[name="producer"]').removeAttr('disabled');

                jQuery('a.hide_new_producer').hide();
                return false;
            });


        });
    </script>
<?php } ?>
 <script>
    function rename_attr(_obj, _old, _new){
        _val = _obj.attr(_old);
        _obj.removeAttr(_old);
        _obj.attr(_new, _val);
    } 
    
    function disable(list){
        jQuery(list).val('').attr('disabled', 'disabled');
        jQuery(list).each(function(){
            rename_attr(jQuery(this), 'validate_error', 'validate_error1');
        }); 
    }
    
    function enable(list){
        jQuery(list).removeAttr('disabled');
        jQuery(list).each(function(){    
            rename_attr(jQuery(this), 'validate_error1', 'validate_error');
        }); 
    }
    
    function add_disabled_cost(){
         <?php if ($ads_id) { ?>
                if (jQuery('input[name="cost1"]').val() == 0 && jQuery('#sell_radio').is(':checked')){
                    disable('#cost_sell input[type="text"], #cost_rent input[type="text"]');
                    jQuery('#indogovor').click();
                } 
                
                if (jQuery('input[name="cost_hour"]').val() == 0 && jQuery('input[name="cost_smena"]').val() == 0 && jQuery('#rent_radio').is(':checked')){
                    disable('#cost_sell input[type="text"], #cost_rent input[type="text"]');
                    jQuery('#indogovor').click();
                }  
        <?php }?>   
    }
    
    jQuery(document).ready(function() {
        add_disabled_cost();   
        jQuery('#indogovor').change(function(){
            if (jQuery(this).prop('checked') == true){
                disable('#cost_sell input[type="text"], #cost_rent input[type="text"]');
            }
            else{
                enable('#cost_sell input[type="text"], #cost_rent input[type="text"]');
            }
                
        });
    });
</script>

