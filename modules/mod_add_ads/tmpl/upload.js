
jQuery(function(){
		var btnUpload=jQuery('#upload');
		var status=jQuery('#system-message-container');
		new AjaxUpload(btnUpload, {
			action: '/modules/mod_add_ads/upload/upload-file.php?upload=1&user_id='+jQuery('input[name=user_id]').val(),
			name: 'uploadfile',
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|png|jpeg)$/.test(ext))){ 
                    // extension is not allowed 
					status.text('загружать можно только JPG, PNG, вес не более '+<?php echo MAX_FILE_SIZE ?>+' Кб, не более '+<?php echo MAX_WIDTH ?>+' px');
					return false;
				}

                if (num_photo()>=<?php echo MAX_PHOTO_ITEMS;?>){
                    status.text('Максимальное количество изображений: '+<?php echo MAX_PHOTO_ITEMS;?>);
                    return false;
                }
                var is_photo = false;
                
                jQuery('#files li img').each(function(){
                    if (jQuery(this).attr('src').indexOf(file) + 1)
                        is_photo = true;
 
                });
                
                if (is_photo){
                     status.text('Такое изображение уже существует...');
                     return false;
                }
                
				status.text('Загрузка...');
			},
			onComplete: function(file, response){
				//On completion clear the status
				status.text('');
                response = response.split('##');
				//Add uploaded file to list
                //alert(response);
				if(response[0]==="success"){
                    //add_img_to_list(response[1],'/<?php echo THUMB_PREFIX;?>',file);
                    file = file.replace('.jpeg', '.jpg');
                    add_img_to_list(response[1],'/<?php echo THUMB_PREFIX;?>',response[3], response[2]);
				} else{
                    status.html(response[0]+response[1]);
				}
			}
		});
		
	});