// валидация и отправка формы
function if_num(item) 
{ 
   pattern = /^[1-9]\d*$/;
   if (pattern.test(item)) 
      return true;
   else
      return false;                 
}

function if_email(item) 
{ 
   pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
   if (pattern.test(item)) 
      return true;
   else
      return false;                 
}

function set_error_text(id_form, object, inner){
    link = object.attr('validate_link');
    high_error = jQuery(id_form+' input[type=radio][validate_link='+link+']:checked').attr('validate_error')
    if (high_error && !inner)
        error +=high_error+'<br/>'; 
    else if (object.attr('validate_error')!='') 
            error +=object.attr('validate_error')+'<br/>'; 
        object.addClass('red');
}

function set_error(object, id_form, inner){
    link = object.attr('validate_link');
    if (!link){
        set_error_text(id_form, object);
    }
    else if(jQuery(id_form+' input[type=radio][validate_link='+link+']:checked').length){
        set_error_text(id_form, object, inner);
    }
}

function validate(id_form){
    jQuery(id_form+' input,'+id_form+' select').removeClass('red');
        error = '';
        if (<?=VALIDATE_FORM;?>){
            jQuery(id_form+' *[validate_error]').each(function(){
                
                if (jQuery(this).is('input') && jQuery(this).attr('type')!='radio')
                {

                    if (jQuery(this).val()==''){
                        set_error(jQuery(this), id_form);
                    }
                    else if (jQuery(this).attr('is_num')=='1' && !if_num(jQuery(this).val()))
                    {
                        set_error(jQuery(this), id_form, 1);
                    }
                    else if (jQuery(this).attr('is_email')=='1' && !if_email(jQuery(this).val()))
                    {
                        set_error(jQuery(this), id_form, 1);
                    }
   
                }
               
                if (jQuery(this).attr('type')=='checkbox' && !jQuery(this).is(':checked'))
                {
                    error +=jQuery(this).attr('validate_error')+'<br/>';  jQuery(this).addClass('red');
                }
                //if (jQuery(this).is('select')){alert(jQuery(this).val()+'<?=DEFAULT_SELECT_VAL1?>');}
                if (jQuery(this).is('select') && (jQuery(this).val()=='<?=DEFAULT_SELECT_VAL?>' || jQuery(this).val()=='<?=DEFAULT_SELECT_VAL1?>' || jQuery(this).val()==null))
                {       
                    set_error(jQuery(this), id_form, 1);
                } 
                if (jQuery(this).is('textarea') && jQuery(this).val()=='')
                    {if (jQuery(this).attr('validate_error')!='') error +=jQuery(this).attr('validate_error')+'<br/>'; jQuery(this).addClass('red');}       
            });
            
            jQuery(id_form+' *[is_email]').not('.red').each(function(){ // проверка почтовых полей
                if (!if_email(jQuery(this).val()))
                {   if (jQuery(this).attr('validate_error') && jQuery(this).attr('validate_error')!='') 
                        error +=jQuery(this).attr('validate_error')+'<br/>';
                    else
                        error +='<?=MESSAGE_INCORRECT_EMAIL?>'+'<br/>';     
                    jQuery(this).addClass('red');
                } 
            });
        }
        return error;
}    