<?php
require_once '../../../geo/class/app/app.php';
$p = new PostData();
$ads_manage = new Ads_AdsManage();
$validate = new Validate_AdsForm();
if ($p->is_post()){
    
    if ($validate->run()){
        $np = trim($p->new_producer);
        if ($np != NEW_PRODUCER_VALUE && $np != ''){
            $add_mess = PHP_EOL.NEW_PRODUCER_MESSAGE;
        }   
        if($ads_manage->is_ads_exsist(false,'',$p->ads_id)){
            $ads_manage->update_ads($p); // обновляем объявление   
            exit('success##объявление №'.$p->ads_id.' Обновлено.'); 
        }     

        else if($id=$ads_manage->insert_ads($p)){ // добавляем объявление в базу
            if ($np != NEW_PRODUCER_VALUE && $np != ''){
                Message_User::ads_on_moderate($id);
            } 
            else{
                Message_User::ads_moderate_ok($id);
                Message_Admin::mail_new_ads($id);
            }
            exit('success##объявление №'.$id.' добавлено.'.$add_mess); 
        }    
        else      
            exit('error##Ошибка: '.implode(' / ',$ads_manage->error)); 
     }else{
        exit('error##'.$validate->getError().'##'.$validate->get_error_fields());
     }   
}
exit('error##данные не переданы');
?>