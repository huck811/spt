<?php
header("Content-Type: application/json;charset=UTF-8");
include "../../../geo/class/app/app.php";
$limit = 10; // сколько «подсказок» мы выдадим пользователю
$res = array();
$city = new cities();
 if (!empty($_GET['term']))// переменную 'term' передает виджет «Autocomplete»
 {
    $text = mb_strtoupper(trim($_GET['term']), 'UTF-8');
    $rows = $city->get_all_cities();
    //print_r($rows);
    $inarray = array();
    foreach ($rows as $v) {
        
       if (($pos = strpos(mb_strtoupper(trim($v["city"]), 'UTF-8'), $text)) !== false && $pos == 0){ 
           if (!in_array($v["city"], $inarray)){ 
                $res[] = array("id"=>$v['city_id'].'##'.$v["region"].'##'.$v["district"], "label" => $v["city"] . ' ('.$v["region"].')', "value"=>$v["city"]);
                $inarray[] = $v["city"];
           }
       }     
       else if (($pos = strpos(mb_strtoupper(trim($v["region"]), 'UTF-8'), $text)) !== false && $pos == 0){
           if (!in_array($v["region"], $inarray)){  
                $res[] = array("id"=>$v["region"].'##'.$v["region"].'##'.$v["district"], "label" => $v["region"], "value"=>$v["region"]); 
                $inarray[] = $v["region"];
           }
       }    
       
       if (count($res) >= ($limit-1))
         break;  
    }
    // формируем нужный массив и прекращаем работу скрипта
    die(json_encode($res));
    
    //print_r($res);
 }
?>