<?php
require_once '../../../geo/class/app/app.php';

$category = new ads_category();

$get_subcategory = isset($_GET['get_subcategory']) ? 1 : 0;
$def = isset($_GET['default_val']) ? $_GET['default_val'] : SELECT_SUB_CATEGORY;
if (!empty($_GET['category'])){
    if (!$get_subcategory)
        exit('success##'.$category->ajax()->get_list_producer('','Производитель:','producer','', array(SELECT_PRODUCER),$_GET['category']));
    else
        exit('success##'.$category->ajax()->get_list_sub_category('subcategory',CATEGORY_LABEL,'subcategory','',array($def),'',$_GET['category']));    
}
exit('error##нужно передать параметр');
?>