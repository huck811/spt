<?php
require_once '../images_library.php';
require_once '../../../config.php';
error_reporting(0);

if ($_GET['hash'] && ($_GET['hash']!=md5($_GET['user_id'])))
exit('Ошибка: ##такого пользователя не существует');
$max_file_size  = MAX_FILE_SIZE;
$max_height     = MAX_HEIGHT;
$max_width      = MAX_WIDTH;
$preview_width  = PREVIEW_WIDTH;
$preview_height = PREVIEW_HEIGHT;
$thumb_prefix   = THUMB_PREFIX;
$uploaddir      = UPLOAD_DIR;

if ($_GET['upload']){
    $uid = htmlspecialchars($_GET['user_id']);
    $folder_prefix = date("m_Y"); //получаем текущий месяц и год   
    if ($_GET['avatar']){
        $folder_prefix = '/avatars';
        $max_file_size = MAX_FILE_AVATAR_SIZE;
        $max_height = MAX_AVATAR_HEIGHT;
        $max_width = MAX_AVATAR_WIDTH;
    }
    
    if (!is_dir($uploaddir . $folder_prefix))
        mkdir($uploaddir . $folder_prefix,0755);
    
    if (!is_dir($uploaddir . $folder_prefix . '/' . $uid))
        mkdir($uploaddir . $folder_prefix . '/' . $uid,0755);    
        
    $folder_prefix .=  '/' . $uid;    
    
    $file_name = $_FILES['uploadfile']['name'];
    $file_tmp = $_FILES['uploadfile']['tmp_name'];
    
    
    // Проверяем есть ли такой файл
    if (!file_exists($file)){
            //exit('##Такой файл уже существует');  
        if ($file_tmp=='')
            exit('##Максимальный размер файла '.$max_file_size.' Кб');
        $im_p = getimagesize($file_tmp); 
        $size = filesize($file_tmp)/1024; 
        
        if ($size>$max_file_size)
            exit('##Максимальный размер файла '.$max_file_size.' Кб');
 
        $file_name = str_replace('.jpeg','.jpg',$file_name);  
        //$file_name = strtolower($file_name);
        //$file_name = str_replace(' ','_',$file_name);  
        $f_arr = explode('.',$file_name);
        $ext = $f_arr[count($f_arr)-1];
        $file_name = md5($file_name.rand(0, 36500)).'.'.$ext;
        $file = $uploaddir . $folder_prefix . '/' . basename($file_name); 
        
        if (move_uploaded_file($file_tmp, $file)) { 
          
          $image = new Resize_Image ($file);
          
           
          // режем фото до стандартных размеров
          //echo $file;
          
          $image->resize( false, $max_height );
          
          $size = $image->getSize();
          if ($size['width'] >= $preview_width)
            $wm = BASE.'images/watermark.png';
          else
            $wm = BASE.'images/watermark_small.png';
          
          
          $image->watermark($wm);  
          
          $image->save( $uploaddir . $folder_prefix . '/', $image->get_name($file_name), 70 );
          $process = $image->result();
          if (!$image->is_ok())
            exit('##Ужать фото не удалось');
          
          // делаем миниатюру
          $name = $image->get_name($file_name);
          //exit($file_name);
          unset($image);
          
          $image1 = new Resize_Image ($file);
          $image1->resize( $preview_width, $preview_height )->crop($preview_width, $preview_height)->watermark($wm)->save( $uploaddir . $folder_prefix . '/', $thumb_prefix . $name, 70 );
          $process = $image1->result();
          if (!$image1->is_ok())
            exit('##Миниатюра не была создана');  
          
          // удаляем файл
          //unlink($file);
          
          echo "success##".$folder_prefix."##".'/'.str_replace(BASE, '', $process['new_file_path'])."##".$name.'.jpg';   
          
        } else {
        	echo "##Ошибка загрузки файла";
        }
    }
    else
        echo "success##".$folder_prefix;
}
?>