<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
require_once 'geo/class/app/app.php';
$baseurl = JURI::base();
$ads_id = isset($_GET['ads_id']) ? $_GET['ads_id'] : false;
$cat_id = isset($_GET['search_category']) ? $_GET['search_category'] : false;
$option = isset($_GET['option']) ? $_GET['option'] : false;
$id     = isset($_GET['id']) ? $_GET['id'] : false;

$s_city      = Utils_Helpers::GetVar('search_city');
$s_region    = Utils_Helpers::GetVar('search_region');

$rent = '';
if (!$cat_id && $ads_id){
    $ads_manage = new Ads_AdsManage();
    $ads_manage->get_ads(false, $ads_id);
    $cat_id = $ads_manage->category;
    
    if ($ads_manage->ad_kindof == 'сдам'){
        $rent = '?rent';
        setcookie ("rent", "rent",time()+3600, '/');
    }
    else{
        setcookie ("rent", "",time()-3600, '/');
    }
    
}

if ($cat_id){
    $category = new ads_category();
    $info = $category->get_arr_category_tree_cat_id($cat_id);
    
    $cat_id = $info[0]['id_tree'];
}
$sef = new MySef();

$rent = isset($_GET['rent']) ? '?rent' : $rent;

//$rent = '';
//$sef->set_category(CAT_ID_SPECTECH)->get_sef_url()
//$sef->set_new_url();

$app = JFactory::getApplication();
$menu = $app->getMenu();
if ($menu->getActive() != $menu->getDefault()) { 
    Utils_Meta::setTitle(JFactory::getDocument(), $ads_id, $cat_id, $s_city, $s_region, $rent);
}

?>
<div style="margin-top: 3px;">
	<ul class="razdel">
	
			
				<li><a class="all <?php echo ($option == 'com_content' && $id == 8) ? "active" : ""?>" href="/"></a>
				</li>
                                <li><a class="spectech <?php echo ($cat_id == CAT_ID_SPECTECH) ? "active" : ""?>" href="<?php echo JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.CAT_ID_SPECTECH.'&view=list').$rent;?>"></a>
				</li>
				<li><a class="transtech <?php echo ($cat_id == CAT_ID_TRASPORT) ? "active" : ""?>" href="<?php echo JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.CAT_ID_TRASPORT.'&view=list').$rent;?>"></a>
				</li>
				<li><a class="selhoztech <?php echo ($cat_id == CAT_ID_SELHOZTECH) ? "active" : ""?>" href="<?php echo JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.CAT_ID_SELHOZTECH.'&view=list').$rent;?>"></a>
				</li>
				<li><a class="kommtech <?php echo ($cat_id == CAT_ID_KOMMTECH) ? "active" : ""?>" href="<?php echo JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.CAT_ID_KOMMTECH.'&view=list').$rent;?>"></a>
				</li>
				<li><a class="skladtech <?php echo ($cat_id == CAT_ID_SKLADTECH) ? "active" : ""?>" href="<?php echo JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.CAT_ID_SKLADTECH.'&view=list').$rent;?>"></a>
				</li>
                <li class="justify-helper"></li>

	</ul>
</div>