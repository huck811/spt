<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';
$city = new Cities();


$session =& JFactory::getSession(); 

$cat_id     = Utils_Helpers::GetVar('search_category');
$cat_id     = Utils_Helpers::GetVar('search_category_1', $cat_id);
$cat_sub_id = Utils_Helpers::GetVar('search_sub_category');

$s_region   = Utils_Helpers::GetVar('search_region');
$s_city     = Utils_Helpers::GetVar('search_city');
$s_producer = Utils_Helpers::GetVar('search_producer');
$s_model    = Utils_Helpers::GetVar('search_model');
$s_photo    = Utils_Helpers::GetVar('search_photo');
$s_price1   = Utils_Helpers::GetVar('search_price_1');
$s_price2   = Utils_Helpers::GetVar('search_price_2');
$s_stype    = Utils_Helpers::GetVar('sort_type');
$s_direct   = Utils_Helpers::GetVar('direct');

//$cat_id = 72;

$sef = new MySef();


if ($cat_id || $cat_sub_id){ // Показываем фильтр только для разделов/категорий, на главной его не должно быть
    $form_c = new FormConstructor();
    $city = new Cities();
    $category = new Ads_Category();
    
    if (is_numeric($cat_sub_id))
        $info = $category->get_arr_category_tree_cat_id($cat_sub_id);
    else if (is_numeric($cat_id))
        $info = $category->get_arr_category_tree_cat_id($cat_id);    
    $razdel_id = $info[0]['id_tree'];
    
    $num = count($info);
    
    //print_r($info);
    
    if (($num-2) >= 0 && $info[$num-2]['id'])
        $cat_id_1 = $info[$num-2]['id'];
    else 
        $cat_id_1 = false;   
        
    if (($num-3) >= 0 && $info[$num-3]['id'])
        $cat_id_2 = $info[$num-3]['id'];  
    else
        $cat_id_2 = false;      
    
    $year = array();
    for ($i = MAX_YEAR; $i >= MIN_YEAR; $i--)
        $year[]=$i;
     

        
    $motohour = array();
    $narabotka = array();
    $probeg = array();
    $year2 = $year;
    //$motohour[]='0';
    /*for ($i = MOTOHOUR_MIN; $i <= MOTOHOUR_MAX; $i+=10000)
        $motohour[]=$i;
    $mth1 = $mth2 = $motohour;*/
    
    
    for ($i=MIN_NARABOTKA; $i<=MAX_NARABOTKA; $i+=STEP_NARABOTKA)
        $narabotka[]=$i;
         
    $mth1 = $mth2 = $narabotka;

    for ($i=MIN_PROBEG; $i<=MAX_PROBEG; $i+=STEP_PROBEG)
        $probeg[]=$i;
    $max = MAX_NARABOTKA;
    $min = MIN_NARABOTKA;
        
    if ($razdel_id == CAT_ID_TRASPORT){
        $mth1 = $mth2 = $narabotka = $probeg;
        $max = MAX_PROBEG;
        $min = MIN_PROBEG;
    }           
        
    unset($mth1[0]);
    unset($mth2[count($mth2)-1]);
    
    unset($year[0]);
    unset($year2[count($year2)-1]);

       
    if ($s_city && !$s_region)
        $s_region = $city->get_ar_region_by_city_id($s_city);
    
    
    $url = $sef->set_region($s_region)->set_city($s_city)->get_sef_url();
    
    $list_region       = $city->get_ar_regions('left search_region','',"",'', array(SELECT_REGION),$s_region);
    //echo $cat_id_1.'xfg';
    $list_category     = $category->set_razdel($razdel_id)->get_list_category('category left search_category_1','','','',array(SELECT_CATEGORY),$cat_id_1);
    
    //echo $cat_id_1.'---'.$cat_id_2;
    if (!$cat_id_1)
        $list_sub_category = '<span class="search_sub_category"><select name="" disabled><option>'.SELECT_SUB_CATEGORY.'</option></select></span>';
    else    
        $list_sub_category = $category->set_razdel($razdel_id)->get_list_sub_category('subcategory left search_sub_category','','','',array(SELECT_SUB_CATEGORY),$cat_id_2, $cat_id_1);
    
    $disabled_city = false;
    if ($s_region && $s_region == SELECT_REGION)
        $disabled_city = 'disabled';
        
    $value_model =  Utils_Helpers::GetVar('search_model', SELECT_MODEL); 
     
    $ymin           = Utils_Helpers::GetVar('year_min', MIN_YEAR);
    $ymax           = Utils_Helpers::GetVar('year_max', MAX_YEAR); 
    $moto_min       = Utils_Helpers::GetVar('motoh_min', $min);
    $moto_max       = Utils_Helpers::GetVar('motoh_max', $max);
    $search_company = Utils_Helpers::GetVar('search_company', false);
    
    $select_ymin = $form_c
                          ->type('SELECT')
                          ->set_option(MIN_YEAR, DEFAULT_MIN_MAX_YEAR_OPTION)
                          ->init($year)
                          ->_class('year_min')
                          ->label('<i>от</i> ')
                          ->name('year_min')
                          ->selected($ymin)
                          ->nowrapp()
                          ->insert();
                               
    $select_ymax = $form_c
                          ->type('SELECT')
                          ->set_option(MAX_YEAR, DEFAULT_MIN_MAX_YEAR_OPTION)
                          ->init($year2)
                          ->_class('year_max')
                          ->selected($ymax)
                          ->label('<i>до</i> ')
                          ->name('year_max')
                          ->nowrapp()
                          ->insert();
    
    $select_motoh_min = $form_c
                               ->type('SELECT')
                               ->set_option($min, DEFAULT_MIN_MAX_YEAR_OPTION)
                               ->init($mth2)
                               ->num_format(1)
                               ->selected($moto_min)
                               ->_class('motoh_min')
                               ->label('<i>от</i> ')
                               ->name('motoh_min')
                               ->nowrapp()
                               ->insert();
                         
    $select_motoh_max = $form_c
                               ->type('SELECT')
                               ->set_option($max, DEFAULT_MIN_MAX_YEAR_OPTION)
                               ->init($mth1)
                               ->num_format(1)
                               ->selected($moto_max)
                               ->_class('motoh_max')
                               ->label('<i>до</i> ')
                               ->name('motoh_max')
                               ->nowrapp()
                               ->insert();
                          
    
    if ($razdel_id == CAT_ID_TRASPORT) 
        $label_motoh = CLIST_MOTO_HOUR_TRANSPORT;
    else  
        $label_motoh = CLIST_MOTO_HOUR;    
        
    $uri = str_replace(array('?rent=','&rent=','?rent','&rent'),'',$_SERVER["REQUEST_URI"]); 
   
    $uri = explode('?', $uri);
    $uri = $uri[0];
    
    $sign = '?';
    
    $rent = $sell = $uri;
    /*if (substr_count($_SERVER["REQUEST_URI"],'?'))
        $rent .= '&rent';
    else*/
        $rent .= $sign.'rent';   
        $sell .= ''; 
        
        $class_sell        = isset($_COOKIE['rent']) ? '' : 'active';  
        $class_rent        = isset($_COOKIE['rent']) ? 'active' : ''; 
    ?>
    <?php include "default.phtml";?>
    
     
       
    <script type="text/javascript">
        <?php include "script.js";?>
    </script>
<?php } else{ //главная?>
<style>div.breadcrumbs{padding: 0;}</style>
<?php }?>