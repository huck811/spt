
function set_city(region){
            jQuery.get("/modules/mod_add_ads/upload/get_list_city.php?region="+encodeURIComponent(region), function(response){ // запрос на список городов по региону
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     jQuery("select.search_city").html(response[1]).removeAttr('disabled');
                     jQuery("select.search_city option").each(function(){
                        jQuery(this).removeAttr('selected');
                        if ('<?=$search_city?>' != '')
                            selected = '<?=$search_city?>';
                        else
                            selected = '<?=$s_city?>';
                        if(jQuery(this).val()==selected)
                            jQuery(this).attr('selected','selected');
                     });
                     //set_url();
                  }
                  else{ // удалить не получилось
                  alert(response[0]);
                    //status.html(response[0]+response[1]);
                  }
                  
              });
              
        }
        
        function set_sub_category(category){
            jQuery.get("/modules/mod_add_ads/upload/get_list_producer.php?get_subcategory&category="+category, function(response){ // запрос на список городов по региону
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     //
                     jQuery(".search_sub_category select").html(response[1]).removeAttr('disabled');
                     jQuery(".search_sub_category select option").each(function(){
                        jQuery(this).removeAttr('selected');
                        if(jQuery(this).val()==jQuery('#sub_cat_id').val())
                            jQuery(this).attr('selected','selected');
                     });
                  }
                  else{ // удалить не получилось
                    //status.html(response[0]+response[1]);
                    
                  }
                  
              });
    
              
        }
        
        function set_producer(category){
            jQuery.get("/modules/mod_add_ads/upload/get_list_producer.php?category="+category, function(response){ // запрос на список городов по региону
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     jQuery("select[name=search_producer]").html(response[1]).removeAttr('disabled');
                     jQuery("select[name=search_producer] option").each(function(){
                        jQuery(this).removeAttr('selected');
                        if(jQuery(this).val()=='<?=$s_producer?>')
                            jQuery(this).attr('selected','selected');
                     });
                  }
                  else{ // удалить не получилось
                    //status.html(response[0]+response[1]);
                    
                  }
                  
              });
    
              
        }
        
         function isNumber(n) {
           return !isNaN(parseFloat(n)) && isFinite(n);
         }
         
         function set_url_and_submit(type){
            
            region = '?region=0';
            if (jQuery('.search_region select').val()!='<?=SELECT_REGION?>')
                region = '?region='+jQuery('.search_region select').val(); 
            city = '';  
            if (type!='region' && jQuery('select.search_city').val()!='<?=SELECT_CITY?>')
                city = '&city='+jQuery('select.search_city').val(); 
                 
            category = '';
                          
             
            
            if (isNumber(jQuery('#razdel').val()))
                category = '&category='+jQuery('#razdel').val();
                
            if (isNumber(jQuery('#cat_id').val()))
                category = '&category='+jQuery('#cat_id').val();   
                
            if (isNumber(jQuery('#sub_cat_id').val()))
                category = '&category='+jQuery('#sub_cat_id').val();
     
            
                
            jQuery.get("/modules/mod_ads_search/tmpl/get_url.php"+region+city+category, function(response){ // запрос на список городов по региону
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     jQuery('.search form').attr('action',response[1]);
                     //alert('cghgh');
                     jQuery('.search_panel form').submit();
                  }
             });    
        

              
        }
        
        if (jQuery('.search_region select').val()!='<?=SELECT_REGION?>')
            set_city(jQuery('.search_region select').val());
        if (jQuery(".search_category_1 select").val()!='<?=SELECT_CATEGORY?>')    
            {set_sub_category(jQuery("#cat_id").val());set_producer(jQuery("#sub_cat_id").val()); } 
   
        
        jQuery('.search_region select').change(function(){// при смене региона подгружаем список городов этого региона

            if (jQuery(this).val()=='<?=SELECT_REGION?>'){
                jQuery("select.search_city").html('<option><?=SELECT_CITY?></option>').attr('disabled','');
                //set_url('change_sub_cat');
            }
            else{
              set_city(jQuery(this).val());
              
            }   
            
        });
        
        jQuery('select.search_city').change(function(){
             //set_url(jQuery(this).val());
        });
        
        jQuery(".search_category_1 select").change(function(){ // при смене категории подгружаем список подкатегорий
            if (jQuery(this).val()=='<?=SELECT_CATEGORY?>'){
                jQuery(".search_sub_category select").html('<option><?=SELECT_SUB_CATEGORY?></option>').attr('disabled','');
                jQuery("select[name=search_producer]").html('<option><?=SELECT_PRODUCER?></option>').attr('disabled','');
                //set_url('change_sub_cat');
                jQuery('#cat_id, #sub_cat_id').val('');
            }
            else{
              set_sub_category(jQuery(this).val());
              jQuery(".search_sub_category select").html('<option><?=SELECT_SUB_CATEGORY?></option>').removeAttr('disabled');
              //set_url();
              jQuery('#cat_id').val(jQuery(this).val());
            } 
            
            
            
            if (jQuery('#razdel').val()==<?=CAT_ID_TRASPORT;?>){ // Если выбрана категория транспорт, по меняем название поля "наработка" на "пробег"
                jQuery('.motohour span').html('<?=CLIST_MOTO_HOUR_TRANSPORT;?>');
            }    
            else{
                jQuery('.motohour span').html('<?=CLIST_MOTO_HOUR;?>'); 
            }    
        });
        
        jQuery(".search_sub_category select").change(function(){ // при смене подкатегории подгружаем список производителей
            if (jQuery(this).val()=='<?=SELECT_SUB_CATEGORY?>'){
                jQuery("select[name=search_producer]").html('<option><?=SELECT_PRODUCER?></option>').attr('disabled','');
                //set_url('change_sub_cat_1');
                jQuery('#sub_cat_id').val('');
            }
            else{
                
              set_producer(jQuery(this).val());
              //set_url();
              jQuery('#sub_cat_id').val(jQuery(this).val());
            }  
              
        });
        
        jQuery('input.search_panel[type="submit"]').click(function(){
            set_url_and_submit();
            return false;
        });
        
        jQuery(document).ready(function(){
            jQuery('input[name=search_model]').focus(function () {
                    var cache = {},
                    lastXhr;
                    jQuery(this).autocomplete({
                       minLength: 3,
                       source: function( request, response ) {
                          var term = request.term;
                          if ( term in cache ) {
                             response( cache[ term ] );
                             return;
                          }
                          
                          lastXhr = jQuery.get( "/modules/mod_add_ads/upload/get_model_dynamic.php?callback=?", request, function( data, status, xhr ) {
                             
                              cache[ term ] = data;
                             if ( xhr === lastXhr ) {
                                response( data );
                             }
                             
                          });
                           
                       },
                       select: function( event, ui ) {
                       
                      }
                    });
                 
            });
        });