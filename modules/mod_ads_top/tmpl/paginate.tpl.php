<?php include_once "search_url_params.php";?>
<?php $arr_paginate = $ads_manage->arr_paginate;?>
<?php if (count($arr_paginate->pages)>1){?>
    <div class="list_ads">    
    <?php $uri = explode('?',$_SERVER['REQUEST_URI']);?>
    <div class="pagination"><a href="<?=$uri[0]?>?page=<?=$arr_paginate['previous']?><?=SEARCH_PAR_TOP?>" class="prev">«</a>
    <?php
    
    foreach ($arr_paginate['pages'] as $page){?>
     <a href="<?=$uri[0]?>?page=<?=$page?><?=SEARCH_PAR_TOP?>" <?php if ($arr_paginate['current']==$page){?>class="cur"<?php }?>><?=$page?></a>   
    <?php }
    ?>
    <a href="<?=$uri[0]?>?page=<?=$arr_paginate['next']?><?=SEARCH_PAR_TOP?>" class="next">»</a></div>
    </div>
<?php }?>