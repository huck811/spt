<?php
  define('_JEXEC', true);
  defined('_JEXEC') or die( 'Restricted access' );
  require_once '../../../geo/class/app/app.php';
  $comments = BASE . 'components/com_jcomments/jcomments.php';
  ?>
  <script type="text/javascript">
        var jcomments = new JComments(1, 'com_content','/index.php?option=com_jcomments&amp;tmpl=component');
        jcomments.setList('comments-list');
        
        function JCommentsInitializeForm()
        {
        	var jcEditor = new JCommentsEditor('comments-form-comment', true);
        	jcEditor.addButton('b','Bold','Enter text');
        	jcEditor.addButton('i','Italic','Enter text');
        	jcEditor.addButton('u','Underlined','Enter text');
        	jcEditor.addButton('s','Striked','Enter text');
        	jcEditor.addButton('img','Image','Enter full URL to the image');
        	jcEditor.addButton('url','Link','Enter full URL');
        	jcEditor.addButton('hide','Hidden text (only for registered)','Enter text to hide it from unregistered');
        	jcEditor.addButton('quote','Quote','Enter text to quote');
        	jcEditor.addButton('list','List','Enter list item text');
        	jcEditor.initSmiles('/components/com_jcomments/images/smiles');
        	jcEditor.addSmile(':D','laugh.gif');
        	jcEditor.addSmile(':lol:','lol.gif');
        	jcEditor.addSmile(':-)','smile.gif');
        	jcEditor.addSmile(';-)','wink.gif');
        	jcEditor.addSmile('8)','cool.gif');
        	jcEditor.addSmile(':-|','normal.gif');
        	jcEditor.addSmile(':-*','whistling.gif');
        	jcEditor.addSmile(':oops:','redface.gif');
        	jcEditor.addSmile(':sad:','sad.gif');
        	jcEditor.addSmile(':cry:','cry.gif');
        	jcEditor.addSmile(':o','surprised.gif');
        	jcEditor.addSmile(':-?','confused.gif');
        	jcEditor.addSmile(':-x','sick.gif');
        	jcEditor.addSmile(':eek:','shocked.gif');
        	jcEditor.addSmile(':zzz','sleeping.gif');
        	jcEditor.addSmile(':P','tongue.gif');
        	jcEditor.addSmile(':roll:','rolleyes.gif');
        	jcEditor.addSmile(':sigh:','unsure.gif');
        	jcomments.setForm(new JCommentsForm('comments-form', jcEditor));
        }
        setTimeout(JCommentsInitializeForm, 100);
        jcomments.setAntiCache(1,0,0);
    </script>
  <?php
  
  $head = $_REQUEST['head'];
  $ads_id = $_REQUEST['ads_id'];
  
  if (file_exists($comments)) {
    
    //echo $comments;
    require_once($comments);
    echo JComments::showComments($ads_id, 'com_adsmanager', $head);
    echo $comments;
  }
?>