<?php
$is_ajax = isset($_REQUEST['is_ajax']) ? 1:0; 

if(!$is_ajax){
    // Check to ensure this file is included in Joomla!
    defined('_JEXEC') or die( 'Restricted access' );  
}
else{
    require_once '../../../geo/class/app/app.php';
}


$type = isset($_COOKIE['rent']) ? 'rent' : ''; 



$city = new cities();
$category = new Ads_Category();
$ads_manage = new Ads_AdsManage(); 
$sef = new MySef();
$show = false;




//print_r(glob($uploaddir."/10_2013/108/small_*_1.jpg"));

$cat_id = isset($_REQUEST['search_category']) ? $_REQUEST['search_category'] : false;
$cat_id = isset($_REQUEST['search_category_1']) ? $_REQUEST['search_category_1'] : $cat_id;
$cat_id = isset($_REQUEST['search_sub_category']) ? $_REQUEST['search_sub_category'] : $cat_id;

$alltop = isset($_REQUEST['alltop']) ? true : false;


$OnClick = 'onClick="ajax_load(jQuery(this));"';

//echo $cat_id;
$show = true;
$razdel_id = false;

if ($alltop)
    $show = false;

if ($cat_id){
    $info = $category->get_arr_category_tree_cat_id($cat_id);
    $cat_name = $info[0]['name'];
    $razdel_id = $info[0]['id_tree'];
}



if ($razdel_id == CAT_ID_TRASPORT)
    $label_probeg = CLIST_MOTO_HOUR_TRANSPORT_1;
else
    $label_probeg = CLIST_MOTO_HOUR;    

if(!$is_ajax){
    $app = JFactory::getApplication();
    $menu = $app->getMenu();
    if ($menu->getActive() == $menu->getDefault()) { // если на главной отключаем вывод
            $show = false;
    }
}

if (!isset($_REQUEST['new']))
    $state = 1;
else
    $state = 0;    
include "list_ads_main.php";

if (!$is_ajax){
?>
<div class="list_ads">
<?php    
}    
?>

<?php
    
if (isset($dhtml)){
    echo $dhtml;
    echo '<div class="list_ads">';
      include "paginate.tpl.php";
    echo '</div>';
?>

<?php
}else if ($show){
    echo "<br/><strong><center>Нет подходящих объявлений!</center></strong><br/>";
}

if (!$is_ajax){
?>
</div>
<?php 
$u_id   = Utils_Helpers::GetVar('u_id');
$name = false;
if ($u_id){
    $ads_manage->set_user($u_id);
    $user =& JFactory::getUser($u_id);
    $name = $user->get('name');
}  
?>
<script type="text/javascript">
    
function ajax_load(_this){
    
    if (_this.attr('direct') == 'ASC')
            direct = 'DESC';
       else
            direct = 'ASC';
    _this.attr('direct', direct);
    type = _this.attr('type');
    
    jQuery.ajax({
          type: "POST",
          url: "/modules/mod_adsmanager_ads/tmpl/list_ads.php?is_ajax<?=SEARCH_PAR ?>&search_category=<?=$cat_id?>",
          data: { sort_type: _this.attr('type'), direct: direct, ajax_url: "<?php echo $_SERVER['REQUEST_URI']?>", name : "<?=$name?>", u_id : "<?=$u_id?>"},
          success: function(data){
              //head = jQuery('div.adverts.list_ads tr.head').html();
              jQuery('div.list_ads').html(data);
              //jQuery('div.adverts.list_ads tr.head').html(head);
              jQuery('span.arrow_sort[type="'+type+'"]').attr('direct', direct);
              
          }
   }); 
} 
  
</script>
<?php    
}    
?>