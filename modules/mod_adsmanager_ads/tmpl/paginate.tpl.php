<?php include_once "search_url_params.php";?>
<?php $arr_paginate = $ads_manage->arr_paginate;?>
<?php $uri = isset($_REQUEST['ajax_url']) ? explode('?',$_REQUEST['ajax_url']) : explode('?',$_SERVER['REQUEST_URI']);?>
<?php if (isset($arr_paginate['current']) && $arr_paginate['current']){?>
<div class="pagination"><a href="<?=$uri[0]?>?page=<?=$arr_paginate['previous']?><?=SEARCH_PAR?>" class="prev">«</a>
<?php

foreach ($arr_paginate['pages'] as $page){?>
 <a href="<?=$uri[0]?>?page=<?=$page?><?=SEARCH_PAR?>" <?php if ($arr_paginate['current']==$page){?>class="cur"<?php }?>><?=$page?></a>   
<?php }
?>
<a href="<?=$uri[0]?>?page=<?=$arr_paginate['next']?><?=SEARCH_PAR?>" class="next">»</a></div>
<?php }?>