<?php
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );
$ads_manage = new Ads_AdsManage(); 

$user =& JFactory::getUser();
$user_id = $user->get('id');
$ads_id = (int) htmlspecialchars($_REQUEST['ads_id']);



$ads_manage->get_ads(false,$ads_id);


$ads_user = new Ads_User();
$ads_user->get_user_data($ads_manage->user_id);

$cities = new Cities();
//get_city_by_id


$hits = $ads_manage->get_hits($ads_id);
if (!$hits)
    $hits = 0;

$category  = new Ads_Category();
$cat_info  = $category->get_category_info($ads_manage->category);
$info = $category->get_arr_category_tree_cat_id($ads_manage->category);
$razdel_id = $info[0]['id_tree'];
$prod_info = $category->get_producer_info($ads_manage->producer);


$data_ip = $cities->get_data_ip();

$doc = JFactory::getDocument();

$images = $ads_manage->images;
$main_img = '';
$thumb_images = array();
    
if ($ads_manage->main_thumb_image)
    $thumb_images[] = '<a href="/images/com_ads/'.$ads_manage->main_image.'" class="fancybox-thumb" rel="fancybox-thumb"><img src="/images/com_ads/'.$ads_manage->main_thumb_image.'" file="'.$ads_manage->main_image.'" /></a>';

for($i=0; $i<count($ads_manage->thumb_images); $i++){
    if ($ads_manage->thumb_images[$i])
        $thumb_images[] = '<a href="/images/com_ads/'.$ads_manage->images[$i].'" class="fancybox-thumb" rel="fancybox-thumb"><img src="/images/com_ads/'.$ads_manage->thumb_images[$i].'" file="'.$images[$i].'" /></a>'; 
}    
    
    
?>
<?php if($ads_manage->ads_is_visible($ads_manage->status)){?>
    <?php include "single_ads.phtml";?>
    <?php $ads_manage->set_hits($ads_id);?>
    
    <?php // Jcomments
    
      $comments = BASE . 'components/com_jcomments/jcomments.php';
      if (file_exists($comments)) {
        //echo $comments;
        require_once($comments);
        echo JComments::showComments($ads_id, 'com_adsmanager', $ads_manage->head);
        
      }
      
      if ($user_id){
      ?>
      <style type="text/css">
      #comments-form-name,  label[for="comments-form-name"]{display: none;}
      #comments-form-email,  label[for="comments-form-email"]{display: none;}
      <?php if ($ads_manage->is_ads_exsist($user_id, '', $ads_id)){?>
      #comments-form, #comments-list-footer{display: none;}
      <?php }?>
      </style>
    <?php  
      }
    ?>
<?php } else { 
    header('Location: '.BASE_URL.'404.html'); 
    exit();
    
}?>

<script>
jQuery(document).ready(function() {
	jQuery(".fancybox-thumb").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
	});

    
    //jQuery( "#comments" ).load("<?php echo $_SERVER['REQUEST_URI']?> #comments", function(data){alert(data)}); 
    
});
</script>