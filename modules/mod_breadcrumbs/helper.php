<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_breadcrumbs
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

require_once 'geo/class/app/app.php';

class modBreadCrumbsHelper
{
	public static function getList(&$params)
	{
		// Get the PathWay object from the application
		        
        $cat_id = Utils_Helpers::GetVar('search_category');
        $cat_id = Utils_Helpers::GetVar('search_category_1', $cat_id);
        $city_id = $_COOKIE['spt_portal_city'];

        $ads_id = Utils_Helpers::GetVar('ads_id');
        //$cat_id = 0;
        
        $city = new cities();
        
        if (is_numeric($city_id))
            $city_name = $city->get_city_by_id($city_id);
        else
            $city_name = $city_id;
        
        //Utils_Helpers::get_city_inflect($city_name);
        $sef = new MySef();
        $type = 'ПРОДАЖА';
        if ($ads_id){
               $ads_manage = new Ads_AdsManage();
               $item = new stdClass();
               $ads_manage->get_ads(false, $ads_id);
               $item->name = Utils_Hstring::mb_ucfirst($ads_manage->head);
               $item->link = JRoute::_('index.php?option=com_adsmanager&Itemid=138&ads_id='.$ads_id.'&view=list');
               $item_ads = $item;
               $cat_id = $ads_manage->category;               
               $type = ($ads_manage->ad_kindof != 'продам') ? 'АРЕНДА' : 'ПРОДАЖА';
        }
        
        if ($cat_id){
            $category = new Ads_Category();
            
            $items = array();
            $rs = $category->get_arr_category_tree_cat_id($cat_id);
            //print_r($rs);
            $item = new stdClass();
            $item->name =  stripslashes(htmlspecialchars('Вся техника'/*.$city_name*/, ENT_COMPAT, 'UTF-8'));
            $item->link =  '/';
            $items[] = $item;
            //print_r($item);
            $num = count($rs)-1;
            
            if (isset($_GET['rent'])) 
                $type = 'АРЕНДА';    
            
            
            
            for($i=$num; $i>=0; $i--){
               $item = new stdClass();
               $item->name =  Utils_Hstring::mb_ucfirst($rs[$i]['name']);
               $item->link = JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.$rs[$i]['id'].'&view=list');
               $item->link .= ($type == 'АРЕНДА') ? '?rent' : '';
               $items[] = $item;
               //print_r($item);
            }
            
            
            $items[count($items)-1]->name .= ' ' . $type;

            /*if ($ads_id)
                $items[] = $item_ads;*/
            //print_r($items);
            $count = count($items);
        }else{
            $app		= JFactory::getApplication();
    		$pathway	= $app->getPathway();
    		$items		= $pathway->getPathWay();
            //echo '<pre>';
            //print_r($items);
    		$count = count($items);
    		for ($i = 0; $i < $count; $i ++)
    		{
    			$items[$i]->name = Utils_Hstring::mb_ucfirst(stripslashes(htmlspecialchars($items[$i]->name, ENT_COMPAT, 'UTF-8')));
    			$items[$i]->link = JRoute::_($items[$i]->link);
    		}
    
    		if ($params->get('showHome', 1))
    		{
    			$item = new stdClass();
    			$item->name = htmlspecialchars($params->get('homeText', JText::_('MOD_BREADCRUMBS_HOME')));
    			$item->link = JRoute::_('index.php?Itemid='.$app->getMenu()->getDefault()->id);
    			array_unshift($items, $item);
    		}
        }
		return $items;
	}

	/**
	 * Set the breadcrumbs separator for the breadcrumbs display.
	 *
	 * @param	string	$custom	Custom xhtml complient string to separate the
	 * items of the breadcrumbs
	 * @return	string	Separator string
	 * @since	1.5
	 */
	public static function setSeparator($custom = null)
	{
		$lang = JFactory::getLanguage();

		// If a custom separator has not been provided we try to load a template
		// specific one first, and if that is not present we load the default separator
		if ($custom == null) {
			if ($lang->isRTL()){
				$_separator = JHtml::_('image', 'system/arrow_rtl.png', NULL, NULL, true);
			}
			else{
				$_separator = JHtml::_('image', 'system/arrow.png', NULL, NULL, true);
			}
		} else {
			$_separator = htmlspecialchars($custom);
		}

		return $_separator;
	}
}
