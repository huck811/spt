<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';
require_once "geo/cache.php";


$user =& JFactory::getUser();
$user_id = $user->get('id');

$isAdmin = $user->get('isRoot');

// Устанавливаем права пользователя
Ads_User::set_permission(JFactory::getUser());

if ($user_id){ //huck  
    $usr = new Ads_User();
    $usr->get_user_data($user_id);
    if (!$usr->is_user_register()){
        header('Location: '.BASE_URL.'edit-user-details.html'); 
        exit();        
    }    
    
}


$form_c = new FormConstructor();
$city = new Cities();
$category = new Ads_Category();
$ads_manage = new Ads_AdsManage();
$sef = new MySef();

$arr_action = array('del', 'select','top', 'up', 'selled', 'unpublish', 'publish');

$action = Utils_Helpers::GetVar('action');


$ads_id = (int)Utils_Helpers::GetVar('ads');

$type = isset($_GET['rent']) ? 'rent':''; 
$rent = isset($_GET['rent']) ? '&rent':''; 

 


if (in_array($action, $arr_action) && $user_id && $ads_id){
    include 'action/ads_'.$action.'.php';
    $p = new PostData();
    unset($p->action);
    unset($p->ads);
    header('Location: '.$p->get_url().(isset($_GET['rent']) ? '?rent' : ''));
    
}
   

if ($user_id && !$isAdmin){
    $ads_manage->set_user($user_id);
}    

$rows = $ads_manage->set_ads_unpublish()->set_ads_selled()->set_type($type)->paginate(NUM_ADS_ON_PAGE, false, false)->get_list_ads();

if ($user_id){
     if (count($rows)){
        include "list_ads.php";
     } 
     else{
        echo "<br/><strong><center>Нет подходящих объявлений!</center></strong><br/>";
     }
} 
else
    echo "Чтобы посмотреть список объявлений нужно авторизоваться на сайте!";
