<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';
include_once "geo/cache.php";

$user =& JFactory::getUser();
$user_id = $user->get('id');
$ads_id = htmlspecialchars($_GET['ads']);

$ads = new Ads_AdsManage();
$category = new ads_category();
$sef = new MySef();

$list_action = array('activate', 'delete');
$action = $_GET['action'];
$pid = $_GET['pid'];

$isAdmin = $user->get('isRoot');

if (in_array($_GET['action'],$list_action)){
    if (!$pid)
        exit('Error producer id');
        
    if (!$isAdmin) 
         exit('Only admin\'s');
    
    $uri = explode('?',$_SERVER['REQUEST_URI']);
    
    $ads->get_ads(false, false, $pid);
    $user1 =& JFactory::getUser($ads->user_id);
    
    if ($action == 'activate'){
        $ads->set_user($ads->user_id, $isAdmin)->ads_published($ads->id);
        $category->activate_producer($pid); 
        Message_User::ads_moderate_ok($ads->id);       
    }
    else if ($action == 'delete'){
        Message_User::ads_moderate_fail($ads->id); 
        $ads->set_user($ads->user_id, $isAdmin)->delete_ads($ads->id, $ads::TYPE_DELETE_REAL);
        $category->delete_producer($pid);
        
    }
    header('Location: '.$uri[0]);
    exit();
}

$list_producer = $category->get_ar_producer('', true, 0);



echo '<table width="500">';
echo '<tr><th>производитель / объявление</th><th>действие</th></tr>';
foreach ($list_producer as $prod){
    $ads->get_ads(false, false, $prod['id']);
    //$url = $sef->set_ads($ads->id)->get_sef_url();
    $url = '/sub/spt/dobavit-ob-yavlenie.html?ads='.$ads->id;
    echo '<tr>';
    echo '<td align="left">'.$prod['producer'].' | объявление - <a href="'.$url.'" target="_blank">'.$ads->head.'</a></td><td align="center"><a href="#" Onclick="activate_producer('.$prod['id'].'); return false;"><span class="green">Одобрить</span></a> | <a href="#" Onclick="delete_producer('.$prod['id'].'); return false;"><span class="red">Удалить вместе с объявлением</span></a></td>';
    echo '</tr>';
}

echo '</table>';
?>
<script>
    function activate_producer(pid){
        document.location.href = document.location.href + '?action=activate&pid='+pid;
    }
    
    function delete_producer(pid){
        document.location.href = document.location.href + '?action=delete&pid='+pid;
    }
</script>