<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
require_once 'geo/class/app/app.php';
$baseurl = JURI::base();

$user =&JFactory::getUser();
$user_id = $user->get('id');

$isAdmin = $user->get('isRoot');

$callbackok   = isset($_REQUEST['callbackok']) ? true : false;
$callbackfail = isset($_REQUEST['callbackFail']) ? true : false;
$OrderId = $_REQUEST['OrderId'];



//**********************************
// точка обработки обращений от системы PayOnline
if(PAYMENTS_PAYONLYNE_ENABLE){ 
   
   // Этот вызов происходит по запросу системы PayOnline после того
   // как платеж проходит успешно 
   if ($callbackok)
      Payments_Payment::instance()->PaymentCallBackOk($_REQUEST);
   // Этот вызов происходит по запросу системы PayOnline, после того
   // как во время совершения платежа происходит ошибка    
   else if ($callbackfail) 
      Payments_Payment::instance()->PaymentCallBackFail($_REQUEST);
   
   if ($callbackok || $callbackfail)
        exit();
}
//*******************************
?>

<?php 
//точка обращения пользователя
if ($user_id) {
    // просмотр истории платежей
    if (isset($_REQUEST['history'])){
        $list_payment = Payments_Payment::instance()->get_payment_history($user_id);
                ?>


        <i style="font-size: 14px; font-weight: bold; padding-left: 20px;">Платежи и пополнения:</i>
        <br /><br />
        <table width="100%" cellpadding="0" cellspacing="0" class="paymets">
            <tr>
                <th align="center">Дата</th>
                <th align="center">Номер платежа</th>
                <th align="center">Номер объявления</th>
                <th align="center">Вид платежа</th>
                <th align="center">Назначение</th>
                <th align="center">Оплачено до</th>
                <th align="center">Сумма</th>
            </tr>
            <?php foreach($list_payment as $payment) :?>
            <?php 
            $ads_id       = '--';
            $type         = 'Пополнение';
            $date         = '--';
            $what_payment = '--';
            if ($payment['note'] != ''){
                $details = explode('#',$payment['note']);
                $date = $payment['date'];
                if (count($details)==2){
                    $ads_id       = sprintf('№ %08d', $details[1]);
                    $type         = 'Списание';
                }
                else{
                    $ads_id       = '--';
                    $type         = 'Пополнение';
                }

                if (Payments_Payment::instance()->is_select_note($payment['note'])){
                    $what_payment = 'выделить';                    
                    $date = Utils_Helpers::date_format(MyDateTime::DatePlus($date, TIME_SELECT_ADS));
                }    
                else if(Payments_Payment::instance()->is_up_note($payment['note'])){
                    $what_payment = 'поднять';
                    $date = '--';
                }   
                else if(Payments_Payment::instance()->is_top_note($payment['note'])){
                    $what_payment = 'в ТОП';
                    $date = Utils_Helpers::date_format(MyDateTime::DatePlus($date, TIME_IN_TOP));
                }    
                else
                    $what_payment = '--';
                
                if ($date != '--' && MyDateTime::diff_date($date, date('Y-m-d'))<0)
                        $date = 'Время вышло';
            }
            ?>
            <tr>
                <td align="center"><?=Utils_Helpers::date_format($payment['date'])?></td>
                <td align="center">№ <?php printf('%08d', $payment['id'])?></td>
                <td align="center"><?=$ads_id?></td>
                <td align="center"><?=$type?></td>
                <td align="center"><?=$what_payment?></td>
                <td align="center"><?=$date?></td>
                <td align="center"><?=abs($payment['amount'])?> Руб.</td>
            </tr>
            <?php endforeach;?>
        </table>
    <?php }else{ // совершение платежей?>
        <?php $prevrand = isset($_SESSION['rand']) ? $_SESSION['rand'] : false; 
        if (isset($_REQUEST['amount']) && $prevrand != $_REQUEST['rand']){
            // Проверка на передачу параметров от форму оплаты и защита от обновления страницы
            if ($isAdmin){
                // Для админа просто пополняем счет "на халяву".
                Payments_Payment::instance()->RechargeAccount($user_id, $_REQUEST['amount']);
                Message_Message::instance()->set_message('Счет пополнен на '.$_REQUEST['amount'].' Рублей.');
                $_SESSION['rand'] = $_REQUEST['rand'];
            }
            else if (PAYMENTS_PAYONLYNE_ENABLE){
                // для остальных пользователей производим оплату через шлюз PayOnline
                Payments_Payment::instance()->RechargeAccount($user_id, $_REQUEST['amount'], true);
            }    
        } 
        else if (PAYMENTS_PAYONLYNE_ENABLE){
            $OrderId    = $_REQUEST['OrderId'];
            $session_id = $_REQUEST['session_id'];
            
            $show_message = false;
            if($OrderId && $session_id && Payments_Payment::isValidPaymenSession($OrderId, $session_id)){
               
               
               if (isset($_REQUEST['paymentIsok'])) {    
                    $payment_data = Payments_Payment::instance()->get_payment_history(false, $_REQUEST['OrderId']);
                    Message_Message::instance()->set_message('Счет пополнен на '.$payment_data[0]['amount'].' Рублей.');
                    $show_message = true;
               }                     
                    
               
            }
            else if(isset($_REQUEST['paymentFail'])){
                Message_Message::instance()->set_message("Произошла ошибка ({$_REQUEST['ErrorCode']}) при оформлении платежа");
                $show_message = true;
            }  
            
            if ($show_message){
               header('Location: '.JRoute::_(URL_USER_ADD_PAYMENT)); 
               exit();
            }  
  
        }
        
        ?>
        <div class="pay_form">
            <form method="post" action="<?=JRoute::_(URL_USER_ADD_PAYMENT)?>">
                <i>Вы можете пополнить счет любым удобным для вас способом, через систему PayOnline.ru</i>
                <br/><br/>
                Сумма платежа: <input type="text" name="amount" /><input type="submit" value="Пополнить счет" class="button_y" />
                <input type="hidden" value="<?=rand(1,50000)?>" name="rand" />
            </form>
        </div>
    <?php }?>
<?php } else {?>
Чтобы пополнить счет войдите в личный кабинет.
<?php }?>
<style type="text/css">
    table.paymets{
        border: 1px solid #000;
        border-left: 0;
        border-bottom: 0;
    }
        table.paymets th, table.paymets td{
            padding: 5px 10px;
            font-style: italic;
            border-left: 1px solid #000;
            border-bottom: 1px solid #000;
        }
   
</style>
<script>
    var prev_value = '';
jQuery('.pay_form input[type="text"]').keyup(function(){
    if ( parseInt(jQuery(this).val())!=jQuery(this).val())
        jQuery(this).val(prev_value);
    else 
        prev_value = jQuery(this).val();
});

jQuery('.pay_form form').submit(function(){
    if(jQuery('.pay_form input[type="text"]').val()=='')
        return false;
});
</script>
