<?php
/**
 * @package                Joomla.Site
 * @subpackage	Templates.beez_20
 * @copyright        Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license                GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.

defined('_JEXEC') or die;
jimport('joomla.filesystem.file');

JHtml::_('behavior.framework', true);

// get params
$doc = JFactory::getDocument();

$root = $_SERVER['DOCUMENT_ROOT'];

$doc->addStyleSheet($this->baseurl.'/templates/system/css/system.css');
$doc->addStyleSheet($this->baseurl.'/templates/beez_20/style.css');
require_once $root.$this->baseurl.'/geo/class/app/app.php';
require_once $root.$this->baseurl.'/geo/cache.php';
require_once 'remove_mootols.php';
$doc->addStyleSheet($this->baseurl.'/templates/beez_20/jquery-ui-1.10.4.custom.min.css');

//$doc->setTitle(MAIN_PAGE_TITLE);

$city = new cities();  
$countModRightColon = $doc->countModules('position-10');
?>
<!DOCTYPE html>
<html>
	<head>
        <jdoc:include type="head" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script src="/templates/beez_20/js/jquery.cookie.js"></script>
        <link rel="stylesheet" href="/templates/beez_20/jquery.formstyler.css" type="text/css" />
        <script src="/templates/beez_20/js/jquery-ui-1.10.4.custom.min.js"></script>       
        
        <?php include "google_analitics.phtml";?>
	</head>
	<body class="beez_20">
		<div id="wrapper">
                    <div id="header">
                        <div class="head_panel">
                                         <?php include "city_list.phtml";?>
                            <div class="menu_top_1">
                                        <jdoc:include type="modules" name="position-1" />
                            </div>
                        </div><!--head_panel-->
                        <div class="user_panel">
                            <a href="/" class="logo"><jdoc:include type="modules" name="position-3" /></a>
                            <div class="user_info">                     
                                <div class="login_form">
                                    <jdoc:include type="modules" name="position-4" />
                                </div>
                                <?=WELLCOME?>
                            </div><!--user_info-->
                        </div><!--user_panel-->
                        <div class="banners">
                            <div class="inner">
                                <jdoc:include type="modules" name="position-5" />
                            </div>
                        </div><!--banners-->
                        
                        <div class="search_panel">
                            <hr style="height: 6px; width: 100%; background: #434242; border: 0; margin: 0 0 1px 0;"/>
<div class="header"><a href="<?=URL_ADD_ADS?>" class="add_advert"><span><span>ПОДАТЬ ОБЪЯВЛЕНИЕ</span></span></a> </div>
                            <jdoc:include type="modules" name="position-7" />            
                        </div><!--search_panel-->
                        <div class="system_message"><jdoc:include type="message" /></div>
                    </div><!--#header-->    
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <div id="left_block" class="middle_block">
                                    <jdoc:include type="modules" name="position-8" />
                                    <div class="list_add">
                                        <jdoc:include type="modules" name="position-9" />                       
                                    </div><!--list_add-->
                                    <div style="clear: both;" ></div>
                                    <div class="line3"></div>
                                    <div id="content">
                                        <jdoc:include type="component" />
                                    </div><!--#content-->
                                </div><!--#left_block-->
                            </td>
                                
                            <td width="199" valign="top">
                                <div id="right_block" class="middle_block" count_module="<?=$countModRightColon?>">
                                        <jdoc:include type="modules" name="position-10" /> 
                                <div class="bottom"></div>
                                <script src="/templates/beez_20/js/remove_right_colon.js"></script>
                                </div><!--#right_block-->
                            </td>
                        </tr>
                    </table>      
                    
                    
                    <br class="clear">
                    <div id="footer">
                        <div class="menu">
                              <jdoc:include type="modules" name="position-1" />
                         </div>
                         <div class="info">
                            <jdoc:include type="modules" name="position-11" /> 
                         </div><!--info-->
                    </div><!--#footer-->
		</div><!--#wrapper-->
        <script src="/templates/beez_20/js/jquery.formstyler.min.js"></script>
        
	</body>
</html>