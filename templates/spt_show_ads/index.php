<?php
/**
 * @package                Joomla.Site
 * @subpackage	Templates.beez_20
 * @copyright        Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license                GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.filesystem.file');

JHtml::_('behavior.framework', true);

// get params
$doc				= JFactory::getDocument();


$doc->addStyleSheet($this->baseurl.'/templates/system/css/system.css');
$doc->addStyleSheet($this->baseurl.'/templates/beez_20/style.css');
$doc->addStyleSheet($this->baseurl.'/templates/spt_show_ads/style.css');
$doc->addStyleSheet($this->baseurl.'/templates/beez_20/jquery-ui-1.10.4.custom.min.css');
require_once 'geo/class/app/app.php';
require_once "geo/cache.php";
require_once "templates/beez_20/remove_mootols.php";

$city = new cities();
$countModRightColon = $doc->countModules('position-10');
?>
<!DOCTYPE html>
<html>
	<head>
        <jdoc:include type="head" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script src="/templates/beez_20/js/jquery.formstyler.min.js" defer="defer"></script>
        <script src="/templates/spt_show_ads/jquery.fancybox.pack.js" ></script>
        <script src="/templates/beez_20/js/jquery.cookie.js"></script>
        <script src="/templates/beez_20/js/jquery-ui-1.10.4.custom.min.js"></script>
        <link rel="stylesheet" href="/templates/beez_20/jquery.formstyler.css" type="text/css" />
        <link rel="stylesheet" href="/templates/spt_show_ads/css/jquery.fancybox.css" type="text/css" />
        <link rel="stylesheet" href="/templates/spt_show_ads/helpers/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
        <script type="text/javascript" src="/templates/spt_show_ads/helpers/jquery.fancybox-thumbs.js"></script>
        
        <link rel="stylesheet" href="/templates/spt_show_ads/helpers/jquery.fancybox-buttons.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="/modules/mod_add_ads/tmpl/style.css" type="text/css" />
        <?php include "templates/beez_20/google_analitics.phtml";?>
	</head>
	<body> 
		<div id="wrapper">
                    <div id="header">
                        <div class="head_panel">
                                        <?php include "templates/beez_20/city_list.phtml";?>
                            <div class="menu_top_1">
                                        <jdoc:include type="modules" name="position-1" />
                            </div>
                        </div><!--head_panel-->
                        <div class="user_panel">
                            <a href="/" class="logo"><jdoc:include type="modules" name="position-3" /></a>
                            <div class="user_info">                     
                                <div class="login_form">
                                    <jdoc:include type="modules" name="position-4" />
                                </div>
                                <?=WELLCOME?>
                            </div><!--user_info-->
                        </div><!--user_panel-->
                        <div class="banners">
                            <div class="inner">
                                <jdoc:include type="modules" name="position-5" />
                            </div>
                        </div><!--banners-->
                        
                        <div class="search_panel">
                            <jdoc:include type="modules" name="position-7" />            
                        </div><!--search_panel-->
                        <div class="system_message"><jdoc:include type="message" /></div>
                    </div><!--#header-->    
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                                <div id="left_block" class="middle_block">
                                    <jdoc:include type="modules" name="position-8" />
                                    <div class="list_add">
                                        <jdoc:include type="modules" name="position-9" />                       
                                    </div><!--list_add-->
                                    <jdoc:include type="component" />
                                    
                                    <div class="line3"></div>

                                </div><!--#left_block-->
                            </td>
                                
                            <td width="199" valign="top">
                                <div id="right_block" class="middle_block" count_module="<?=$countModRightColon?>">
                                        <jdoc:include type="modules" name="position-10" /> 
                                <div class="bottom"></div>
                                <script src="/templates/beez_20/js/remove_right_colon.js"></script>
                                </div><!--#right_block-->
                            </td>
                        </tr>
                    </table>      
                    
                    
                    <br class="clear">
                    <div id="footer">
                        <div class="menu">
                              <jdoc:include type="modules" name="position-1" />
                         </div>
                         <div class="info">
                            <jdoc:include type="modules" name="position-11" /> 
                         </div><!--info-->
                    </div><!--#footer-->
		</div><!--#wrapper-->
                <script>
                    jQuery('.link_sell_rent a.rent').click(function(){
                        jQuery.cookie('rent', 'rent', { expires: 36000, path: '/' });
                    });

                    jQuery('.link_sell_rent a.sell').click(function(){                        
                        jQuery.removeCookie('rent', { path: '/' });
                        
                      });
                </script>
	</body>
</html>