<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';

$user =& JFactory::getUser();
$user_id = $user->get('id');

$form_c = new form_constructor();
$city = new cities();
$category = new ads_category();

$year = array();
$year[]=DEFAULT_SELECT_VAL;
for ($i=MIN_YEAR; $i<MAX_YEAR; $i++)
    $year[]=$i;
//echo $city->get_select_list_cities();
if ($user_id){
?>
<link rel="stylesheet" href="/templates/stp_person_cab/html/com_users/profile/edit_style.css" type="text/css" />
<script type="text/javascript">
    <?php include "edit_script.js";?>
</script>
<table width="605" cellpadding="0" cellspacing="0" class="t_form_add_ads">
    <tr>
        <td valign="top">
            <form class="form_add_ads" method="POST" id="form_add_ads">
             <?php echo $form_c->type()->label(HEADLINE_LABEL)->name('head')->insert();?>
             <?php echo $form_c->type('INPUT','button')->init(BTN_LOAD_IMAGE_VAL)->label(LOAD_IMAGE_LABEL)->id('upload')->insert();?>
              
             <div class="block_in"><i><?php echo sprintf(MESSAGE_WRONG_IMAGE_FORMAT,MAX_FILE_SIZE,MAX_WIDTH)?></i></div>
             <?php echo $category->get_list_category('category',CATEGORY_LABEL,'category','',array(DEFAULT_SELECT_VAL));?>    
             <?php echo $form_c->type('SELECT')->init(DEFAULT_SELECT_VAL)->label(PRODUCER_LABEL)->name('producer')->insert();?> 
             <?php echo $form_c->type()->label(MODEL_LABEL)->name('model')->insert();?>
             <?php //echo $form_c->insert_input('SELECT','',array($year,""),'year left',YEAR_BORN_LABEL,'year');?>
             <?php echo $form_c->type('SELECT')->init($year)->_class('year left')->label(YEAR_BORN_LABEL)->name('year')->insert();?> 
             <div class="left">
                 <?php echo $form_c->type('INPUT','radio')->init(RENT_ITEM_VAL)->label(RENT_ITEM_LABEL)->nowrapp()->lright()->name('type')->id('rent_radio')->insert();?>
                 <?php echo $form_c->type('INPUT','radio')->init(SELL_ITEM_VAL)->label(SELL_ITEM_LABEL)->nowrapp()->lright()->checked()->name('type')->id('sell_radio')->insert();?>
                 
             </div>
             <div id="cost_sell"><?php echo $form_c->clr()->_class('cost left')->label(COST_LABEL)->name('cost')->insert();?></div>  
             <div id="cost_rent">
                <?php echo $form_c->clr()->_class('hide cost cost1 left')->label(COST_LABEL_1)->name('cost_smena')->insert();?>
                <?php echo $form_c->clr()->_class('hide cost cost2 left')->label(COST_LABEL_2)->name('cost_hour')->insert();?>
             </div> 
             <hr />
             <div class="left">
                 <?php echo $form_c->type('INPUT','radio')->init(NEW_ITEM_VAL)->label(NEW_ITEM_LABEL)->name('state')->id('new')->checked()->nowrapp()->lright()->insert();?>
                 <?php echo $form_c->type('INPUT','radio')->init(OLD_ITEM_VAL)->label(OLD_ITEM_LABEL)->name('state')->id('bu')->nowrapp()->lright()->insert();?>
             </div>
             <div id="moto_hour">
                <?php echo $form_c->clr()->_class('left moto hide')->label(WHAT_WORK_HOUR_LABEL)->name('moto_hour')->insert();?>
             </div> 
             <?php echo $city->get_ar_regions('',REGION_LABEL,"region",'', array(DEFAULT_SELECT_VAL)); ?>
             <?php echo $form_c->type('SELECT')->init(DEFAULT_SELECT_VAL)->label(CITY_LABEL)->name('city')->insert();?> 
             <?php echo $form_c->type('TEXTAREA')->_class('comment')->label(COMMENT_LABEL)->name('comment')->insert();?> 
             
             <div class="block_in">
                 Статус объявления:
                 <?php echo $form_c->type('INPUT','radio')->init(STATE_ACTIVE_ADS_VAL)->label(STATE_ACTIVE_ADS_LABEL)->nowrapp()->lright()->name('status')->checked()->insert();?>
                 <?php echo $form_c->type('INPUT','radio')->init(STATE_PASSIVE_ADS_VAL)->label(STATE_PASSIVE_ADS_LABEL)->nowrapp()->lright()->name('status')->insert();?>
                 <?php echo $form_c->type('INPUT','radio')->init(STATE_SELLED_ADS_VAL)->label(STATE_SELLED_ADS_LABEL)->nowrapp()->lright()->name('status')->insert();?>
             </div>
             <div class="block_in">
                <a href="#"><?=LINK_ADS_MAKE_UP?></a> »<a href="#"><?=LINK_ADS_MAKE_TOP?></a> »<a href="#"><?=LINK_ADS_MAKE_SPECIAL?></a> »
             </div>
             <div class="block_in submit">
                 <?php echo $form_c->type('INPUT','submit')->init('Сохранить')->label()->nowrapp()->insert();?>
                 <?php echo $form_c->type('INPUT','reset')->init('Удалить')->label()->nowrapp()->insert();?>
             </div>
             <input type="hidden" name="user_id" value="<?php echo $user_id;?>" />
             <input type="hidden" name="images" value="" />
            </form>
        </td>
    </tr>
    <tr>
        <td width="500"></td>
    </tr>
</table>
<?php } else{?>
Чтобы подать объявление нужно авторизоваться на сайте!
<?php }?>