<?php
/**
 * @package                Joomla.Site
 * @subpackage	Templates.beez_20
 * @copyright        Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license                GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.filesystem.file');

JHtml::_('behavior.framework', true);

// get params
$doc				= JFactory::getDocument();


$doc->addStyleSheet($this->baseurl.'/templates/system/css/system.css');
$doc->addStyleSheet($this->baseurl.'/templates/beez_20/style.css');
$doc->addStyleSheet($this->baseurl.'/templates/stp_person_cab/style.css');
require_once 'geo/class/app/app.php';
require_once 'geo/cache.php';
require_once "templates/beez_20/remove_mootols.php";
$doc->addStyleSheet($this->baseurl.'/templates/beez_20/jquery-ui-1.10.4.custom.min.css');
$city = new cities();
$countModRightColon = $doc->countModules('position-10');
$id = JRequest::getVar('id');

if ($id == 1 || $countModRightColon == 0){
    $error404 = true;
    $rightColonWidth = 0;
}    
else{
    $error404 = false;
    $rightColonWidth = 199;
}    

?>
<!DOCTYPE html>
<html>
	<head>
        <jdoc:include type="head" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="<?php echo $this->baseurl ?>/templates/stp_person_cab/js/ajaxupload.3.5.js"></script>
        <script src="<?php echo $this->baseurl ?>/templates/stp_person_cab/js/jquery.listen.js"></script>
        <script src="<?php echo $this->baseurl ?>/templates/beez_20/js/jquery.cookie.js"></script>
        <script src="/templates/beez_20/js/jquery-ui-1.10.4.custom.min.js"></script>
        <?php include "templates/beez_20/google_analitics.phtml";?>
	</head>
	<body class="article">
		<div id="wrapper">
                    <div id="header">
                        <div class="head_panel">
                                        <?php include "templates/beez_20/city_list.phtml";?>
                            <div class="menu_top_1">
                                        <jdoc:include type="modules" name="position-1" />
                            </div>
                        </div><!--head_panel-->
                        <div class="user_panel">
                            <a href="/" class="logo"><jdoc:include type="modules" name="position-3" /></a>
                            <div class="user_info">                     
                                <div class="login_form">
                                    <jdoc:include type="modules" name="position-4" />
                                </div>
                                <?=WELLCOME?>
                            </div><!--user_info-->
                        </div><!--user_panel-->
                        <div class="banners">
                            <div class="inner">
                                <jdoc:include type="modules" name="position-5" />
                            </div>
                        </div><!--banners-->
                        
                        <div class="search_panel">
                            <jdoc:include type="modules" name="position-7" />            
                        </div><!--search_panel-->
                        
                        <div class="system_message" style="margin-bottom: 6px;"><jdoc:include type="message" /></div>

                    </div><!--#header-->    
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                                <div id="left_block" class="middle_block">
                                    <jdoc:include type="modules" name="position-8" />
                                    <div class="list_add">
                                        <jdoc:include type="modules" name="position-9" />                       
                                    </div><!--list_add-->
                                    <jdoc:include type="component" />
                                    
                                    
                                </div><!--#left_block-->
                            </td>
                                
                            <td width="<?=$rightColonWidth?>" valign="top">
                                <?php if (!$error404){?>
                                    <div id="right_block" class="middle_block" count_module="<?=$countModRightColon?>">
                                        <jdoc:include type="modules" name="position-10" /> 
                                        <div class="bottom"></div>
                                        
                                    </div><!--#right_block-->
                                <?php }?>
                                <script src="/templates/beez_20/js/remove_right_colon.js"></script>  
                                <script>                                    
                                    if (!jQuery('#right_block').length)
                                        jQuery('#left_block').css('width', '100%');
                                </script>    
                            </td>
                        </tr>
                    </table>      
                    
                    
                    <br class="clear">
                    <div id="footer">
                        <div class="menu">
                              <jdoc:include type="modules" name="position-1" />
                         </div>
                         <div class="info">
                            <jdoc:include type="modules" name="position-11" /> 
                         </div><!--info-->
                    </div><!--#footer-->
		</div><!--#wrapper-->
	</body>
        <script src="<?php echo $this->baseurl ?>/templates/stp_article/style_tables.js"></script>
</html>