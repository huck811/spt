<?php
/**
 * @package                Joomla.Site
 * @subpackage	Templates.beez_20
 * @copyright        Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license                GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.filesystem.file');

JHtml::_('behavior.framework', true);

// get params
$doc				= JFactory::getDocument();


$doc->addStyleSheet($this->baseurl.'/templates/system/css/system.css');
$doc->addStyleSheet($this->baseurl.'/templates/beez_20/style.css');
$doc->addStyleSheet($this->baseurl.'/templates/stp_person_cab/style.css');
$doc->addStyleSheet($this->baseurl.'/templates/beez_20/jquery-ui-1.10.4.custom.min.css');
require_once 'geo/class/app/app.php';
require_once "templates/beez_20/remove_mootols.php";
$city = new cities();
$message = Message_Message::instance()->show(false);
$countModRightColon = $doc->countModules('position-10') + $doc->countModules('position-12');

if (substr_count($_SERVER["REQUEST_URI"], '?rent') || substr_count($_SERVER["REQUEST_URI"], '&rent'))
   $its_rent = true;
else 
   $its_rent = false; 

$url = explode('?',$_SERVER["REQUEST_URI"]);
$url = $url[0];
?>
<!DOCTYPE html>
<html>
	<head>
        <jdoc:include type="head" />
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script src="<?php echo $this->baseurl ?>/templates/stp_person_cab/js/ajaxupload.3.5.js"></script>
        <script src="<?php echo $this->baseurl ?>/templates/stp_person_cab/js/jquery.listen.js"></script>
        <script src="templates/beez_20/js/jquery.formstyler.min.js" defer="defer"></script>
        <script src="<?php echo $this->baseurl ?>/templates/beez_20/js/jquery.cookie.js"></script>
        <script src="/templates/beez_20/js/jquery-ui-1.10.4.custom.min.js"></script>
        <link rel="stylesheet" href="/templates/beez_20/jquery.formstyler.css" type="text/css" />
        <link rel="stylesheet" href="modules/mod_add_ads/tmpl/style.css" type="text/css" />
        <?php include "templates/beez_20/google_analitics.phtml";?>
	</head>
	<body class="personal_cabinet">
        <div id="load-icon" style="display: none;"></div> 
		<div id="wrapper">
                    <div id="header">
                        <div class="head_panel">
                                        <?php include "templates/beez_20/city_list.phtml";?>
                            <div class="menu_top_1">
                                        <jdoc:include type="modules" name="position-1" />
                            </div>
                        </div><!--head_panel-->
                        <div class="user_panel">
                            <a href="/" class="logo"><jdoc:include type="modules" name="position-3" /></a>
                            <div class="user_info">                     
                                <div class="login_form">
                                    <jdoc:include type="modules" name="position-4" />
                                </div>
                                <?=WELLCOME?>
                            </div><!--user_info-->
                        </div><!--user_panel-->
                        <div class="banners">
                            <div class="inner">
                                <jdoc:include type="modules" name="position-5" />
                            </div>
                        </div><!--banners-->
                        
                        
                        
                        <div class="search_panel">  
                            <div style=" margin-bottom: 2px;">
                                <jdoc:include type="modules" name="position-6" /><!--breadcrumbs--> 
                            </div><!--header-->                            
                        </div><!--search_panel-->              
                    </div><!--#header-->    
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td valign="top">
                                <div id="left_block" class="middle_block">
                                    <div class="link_sell_rent pers_cab">
                                    <table style="width: 100%;" cellpadding="0" cellspacing="0">
                                		<tbody>
                                			<tr>
                                				<td width="50%"><a href="<?=URL_PROFILE?>" class="sell"><span>ПРОФИЛЬ</span></a>
                                				</td>
                                				<td style="width: 2px;"></td>
                                				<td><a href="<?=URL_MY_ADS?>" class="rent active"><span>МОИ ОБЪЯВЛЕНИЯ</span></a>
                                				</td>
                                			</tr>
                                		</tbody>
                                	</table>
                                   </div>  
                                   <div class="system_message"><?=$message?><br/><jdoc:include type="message" /></div>
                                   <?php $itemid = JRequest::getVar('Itemid');?> 
                                   <?php if ($itemid==ITEMID_MY_ADS){?>
                                        <table width="100%" cellpadding="0" cellspacing="0"><tr><td>
                                        <span class="yelow_line"><span>
                                            <a href="<?=$url?>" <?php echo $its_rent ? "" : "class='active'"?>>Продажа</a> »&nbsp;&nbsp;
                                            <a href="<?=$url?>?rent" <?php echo $its_rent ? "class='active'" : "" ?>>Аренда</a> »</span></span>
                                         </td></tr></table>   
                                   <?php } else if($itemid != 182) {?>
                                        <table width="100%" cellpadding="0" cellspacing="0"><tr><td><a href="<?=URL_MY_ADS?>" class="yelow_line"><span>ВЕРНУТЬСЯ В МОИ ОБЪЯВЛЕНИЯ</span></a></td></tr></table>
                                    <?php }?>                                 
                                   <div id="content">
                                        <jdoc:include type="component" />
                                    </div><!--#content-->
                                    <br style="clear: both;" />
                                    <div class="line3"></div>
                                </div><!--#left_block-->
                            </td>
                            <td valign="top" width="199">
                                <div id="right_block" class="middle_block" count_module="<?=$countModRightColon?>">
                                        <jdoc:include type="modules" name="position-12" />
                                        <jdoc:include type="modules" name="position-10" /> 
                                        <div class="bottom"></div>
                                        <script src="/templates/beez_20/js/remove_right_colon.js"></script>
                                </div><!--#right_block-->
                            </td>
                        </tr>
                    </table>      
                    
                    
                    <br class="clear">
                    <div id="footer">
                        <div class="menu">
                              <jdoc:include type="modules" name="position-1" />
                         </div>
                         <div class="info">
                              <jdoc:include type="modules" name="position-11" />
                         </div><!--info-->
                    </div><!--#footer-->
		</div><!--#wrapper-->  
                <div class="loading"><div></div></div> 
	</body>
    
</html>