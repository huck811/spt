<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';


$user =& JFactory::getUser();
$user_id = $user->get('id');
$name = $user->get('name');

$form_c = new FormConstructor();
$city = new Cities();
$category = new Ads_Category();
$user2 = new Ads_User();

$user2->get_user_data($user_id);
//print_r($user2->user_data);
$email = $user2->main_email;
//print_r($user2->error);
//echo 'xfg'.$email;
//echo $city->get_select_list_cities();

if ($user_id){
    $user->set('name', $user2->name);
    $name = $user->get('name');
 
?>
<link rel="stylesheet" href="/templates/stp_profile/html/com_users/profile/edit_style.css" type="text/css" />
<script type="text/javascript">
    <?php include "edit_script.js";?>
    <?php include "upload_edit.js";?>
    <?php include "modules/mod_add_ads/tmpl/validate.js";?>
</script>
<br />
<form class="form_add_profile" action="/" method="POST" id="form_add_profile">
<table width="100%" cellpadding="0" cellspacing="0" class="t_form_add_ads">
    <tr>
        <td valign="top">
            
            <strong><i><span class="red">*</span> <span style="color: red;">- поля обязательные для заполнения</span></i></strong><br /><br />
             <div class="head">Регистрационные данные:</div>
             <table>
                <tr>
                    <td align="right"><label><span class="red">*</span>Имя:</label></td><td><?php echo $form_c->type()->init($name)->valid('Укажите имя...')->name('name')->insert();?></td>
                </tr>
                <tr>
                    <td align="right"><label><span class="red">*</span>Логин / E-mail:</label></td><td><?php echo $form_c->type()->init($email)->valid('Укажите регистрационный E-mail...')->name('email')->insert();?></td>
                </tr>
                <tr>
                    <td align="right"><label>Новый пароль:</label></td><td> <?php echo $form_c->type('INPUT','password')->name('password1')->insert();?></td>
                </tr>
                <tr>
                    <td align="right"><label>Подтвердить пароль:</label></td><td> <?php echo $form_c->type('INPUT','password')->name('password2')->insert();?></td>
                </tr>
                <tr>
                    <td></td>
                    <td> 
                        <?php 
                            $form_c->type('INPUT','radio')->init(0)->label('Физ. лицо')->_class('superstyle')->lright()->nowrapp()->name('type_user')->id('fizik');
                            if (!$user2->type_user)
                                $form_c->checked();
                            echo $form_c->insert();    
                        ?>
                        <?php 
                            $form_c->type('INPUT','radio')->init(1)->label('Юр. лицо')->_class('superstyle')->lright()->nowrapp()->name('type_user')->id('urik');
                            if ($user2->type_user)
                                $form_c->checked();
                            echo $form_c->insert();    
                        ?>
                        <br/>    
                        <?php 
                        $form_c->type('INPUT','checkbox')->init(1)->_class('superstyle')->label('Подписаться на новости сервиса')->lright()->nowrapp()->name('email_sender');
                        if ($user2->email_sender){
                            $form_c->checked();
                        }   
                        echo $form_c->insert();
                        ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><div  class="head">Контактные данные:</div></td>
                </tr>
                <tr>
                    <td align="right"><label>Контактное лицо:</label></td><td> <?php echo $form_c->type()->name('fio')->init($user2->fio)->insert();?></td>
                </tr>
                <tr>
                    <td align="right"><label>Телефон:</label></td><td> <?php echo $form_c->type()->name('ad_phone')->init($user2->ad_phone)->insert();?></td>
                </tr>
                <tr>
                    <td align="right"><label>E-mail:</label></td><td> <?php echo $form_c->type()->name('email2')->init($user2->email2)->insert();?></td>
                </tr>
                
                <tr>
                    <td colspan="2"><div  class="head" style="margin-top: 20px;">Адрес местонахождения:</div></td>
                </tr>
                <tr>
                    <td align="right"><label><span class="red">*</span><?=REGION_LABEL?></label></td>
                    <td><?php echo $city->set_valid_region('Выберите регион...')->get_ar_regions('','',"region",'', array(DEFAULT_SELECT_VAL),$user2->region); ?></td>
                </tr>
                <tr>
                    <td align="right"><span class="red">*</span>Населенный пункт:</td>
                     <td><?php echo $city->set_valid_city('Выберите город...')->get_cities_by_region('','','ad_city','',array(DEFAULT_SELECT_VAL),$user2->region,$user2->ad_city);?> </td>
                </tr>
                <tr>
                    <td colspan="2"><div  class="head" style="margin-top: 5px;"></div></td>
                </tr>
                
                <tr class="urik">
                    <td align="right"><label>Телефон компании:</label></td><td> <?php echo $form_c->name('company_phone')->init($user2->company_phone)->insert();?></td>
                </tr>
                <tr class="urik">
                    <td align="right"><label>Факс:</label></td><td> <?php echo $form_c->name('fax')->init($user2->fax)->insert();?></td>
                </tr>
                <tr class="urik">
                    <td align="right"><label>Сайт:</label></td><td> <?php echo $form_c->name('ad_site')->init($user2->ad_site)->insert();?></td>
                </tr>
             </table>
              

        </td>
        <td width="480" valign="top" align="right" class="right">
            <div>
                
                
                <table cellpadding="0" cellspacing="0" width="100%">

                    <tr>
                        <td align="left" width="150"><label>Аватар или логотип:</label></td>
                        <td align="right" class="avatar" width="250">
                        <span id="avatar_img">
                        <?php if ($user2->ad_avatar){?>
                            <img src="/images/com_ads/avatars/<?=$user_id?>/<?=$user2->ad_avatar?>" width="40" />
                        <?php }else{?>
                            <img src="/templates/stp_profile/img/no-avatar.gif" width="40" />
                        <?php }?>
                        </span>
                        <?php echo $form_c->type()->name('ad_avatar')->nowrapp()->init($user2->ad_avatar)->insert();?><?php echo $form_c->type('INPUT','button')->init('Загрузить')->name('load')->id('upload')->nowrapp()->insert();?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right"><span style="color: red;"><br /><small><i><?php echo sprintf(MESSAGE_WRONG_IMAGE_FORMAT,MAX_FILE_AVATAR_SIZE,MAX_AVATAR_WIDTH)?></i></small></span></td>
                    </tr>
                    <tr class="urik">
                        <td colspan="2"><div style="margin-top: 20px;" class="head">Реквизиты и информация о компании:</div></td>
                    </tr>
                    <tr class="urik">
                        <td align="right" valign="top" colspan="2">
                        <div style="float:left"><span class="red">*</span><label>Специализация компании:</label></div><br style="clear: both;" />
                        <?php  echo $category->get_list_category('specialization superstyle','','specialization[]','',array(DEFAULT_SELECT_VAL),$user2->specialization,array('multiple','size="18"'));?>
                        </td>
                    </tr>
                    <tr><td colspan="2"><div style="margin-top: 20px;"></div></td></tr>
                    <tr class="urik">
                        <td align="right"><span class="red">*</span><label>Юридическое название компании:</label></td>
                        <td align="right"><?php echo $form_c->type()->valid('Укажите название компании...')->name('company_name')->init($user2->company_name)->insert();?></td>
                    </tr>
                    <tr class="urik">
                        <td align="right"><label><span class="red">*</span>Фактический адрес:</label></td>
                        <td align="right"><?php echo $form_c->type()->name('fact_adress')->valid('Укажите фактический адрес юр. лица...')->init($user2->fact_adress)->insert();?></td>
                    </tr>
                    <tr class="urik">
                        <td align="right"><label>Представляемые бренды:</label></td>
                        <td align="right"><?php echo $form_c->type()->name('brands')->init($user2->brands)->insert();?></td>
                    </tr>
                    <tr class="urik">
                        <td align="right" valign="top"><span class="red">*</span><label>Описание компании:</label></td>
                        <td align="right"><?php echo $form_c->type('TEXTAREA')->valid('Задайте описание компании...')->attr('maxlength="200"')->name('descript')->init($user2->descript)->insert();?></td>
                    </tr>
                    <tr><td colspan="2"><div style="margin-top: 20px;"></div></td></tr>
                </table>
                <input type="hidden" name="user_id" value="<?=$user_id?>" />
            </div>
        </td>
    </tr>
    <tr><td colspan="2"><div style="margin-top: 20px;"></div></td></tr>
    <tr>
        <td><?php echo $form_c->type('INPUT','submit')->_class('button_y')->init('Сохранить')->label()->nowrapp()->insert();?></td>
        <td align="right"><?=PROFILE_INFO?></td>
         
    </tr>
</table>
 
 <input type="hidden" name="user_id" value="<?php echo $user_id;?>" />
 <input type="hidden" name="images" value="" />
</form>
<?php } else{?>
Чтобы редактировать профиль нужно авторизоваться на сайте!
<?php }?>