jQuery(document).ready(function(){
     
     var option_is_clicked = false;
     jQuery('option').click(function(){
        option_is_clicked = true;
     });
     jQuery('optgroup').click(function(){
        if (!option_is_clicked)
            jQuery('option',this).each(function(){jQuery(this).prop('selected','selected')});
        option_is_clicked = false;    
     });
     jQuery('select[name="specialization[]"]').attr('validate_error', 'Выберите специализацию...');   
     //jQuery('input[name=email]').attr('validate_error','Неверный формат почты...');
     jQuery('input[name=email]').attr('is_email','1');
     
     //jQuery("select[name=ad_city]").attr('validate_error','Выберите город....');
     jQuery("select[name=region]").change(function(){ // при смене региона подгружаем список городов этого региона
            var status=jQuery('#system-message-container');  
            if (jQuery(this).val()=='Выбрать'){
                jQuery("select[name=ad_city]").html('<option>Выбрать</option>');
            }
            else{
              jQuery.get("/modules/mod_add_ads/upload/get_list_city.php?region="+encodeURIComponent(jQuery(this).val()), function(response){ // запрос на список городов по региону
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     jQuery("select[name=ad_city]").html(response[1]);
                  }
                  else{ // удалить не получилось
                    status.html(response[0]+response[1]);
                  }
                  
              });
            }    
        });
        
     function change_validate_attr(attr){ // переименовываем атрибут валидации (отключем валидацию для некоторых полей)
        if (attr=='validate_error')
           newattr = 'validate_error1';
        else   
           newattr = 'validate_error';  
        
        jQuery('tr.urik *['+attr+']').each(function(){
            val = jQuery(this).attr(attr);
            jQuery(this).removeAttr(attr);
            jQuery(this).attr(newattr,val);
        });
     }
     
     if (jQuery('#fizik:checked').length){ // меняем форму для физических лиц и юридических
        jQuery('tr.urik').hide();
        change_validate_attr('validate_error');
     } 
       
     jQuery(document).ready(function(){
          jQuery('span[name=type_user-wrapp]').click(function(){
            if (jQuery(this).attr('id')=='urik-wrapp')
                {jQuery('tr.urik').show(); change_validate_attr('validate_error1');}
            else   
                {jQuery('tr.urik').hide(); change_validate_attr('validate_error');} 
         }); 
     });
       
     
     
     // валидация и отправка формы
        jQuery('#form_add_profile').submit(function (){
            var status=jQuery('#system-message-container');
            jQuery('.form_add_profile input, .form_add_profile select, .form_add_profile textarea').removeClass('red');
            error = '';
               
            if (error!=''){
                status.html(error);
                alert(error.replace(new RegExp("<br/>",'g'),"\n"));
            }    
            else{
                jQuery('div.loading').fadeIn(200);
                jQuery.post("/templates/stp_profile/html/com_users/profile/add_profile.php?uid=<?=$user_id?>&hash=<?=md5($user_id)?>", jQuery("#form_add_profile").serialize(),function(response){ // добавляем объявление
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     status.html('<div class="message">'+response[1]+'</div>');
                     alert(response[1]);
                     //setInterval(function (){document.location.href = '<?=URL_BASE.URL_SEF_MY_ADS?>';},1000);
                  }
                  else{ // добавить не получилось
                    status.html(response[1]);
                    error = response[1];
                    error = error.replace(new RegExp("<br/>",'g'),"");
                    alert(error);
                    
                    if (response[2] != ''){
                        var fields_error = JSON.parse(response[2]);
                        for(i=0; i < fields_error.length; i++){
                            jQuery('.form_add_profile *[name="'+fields_error[i]+'"]').addClass('red');
                        }
                    }  
                  }
                  jQuery('div.loading').fadeOut(200);
                });
                
            }    
  
            return false;    
        }); 
        
        
    
     
    /* if (jQuery('input[name=type_user]').attr('id')=='fizik')
        {jQuery('#fizik').click();}*/
 
});