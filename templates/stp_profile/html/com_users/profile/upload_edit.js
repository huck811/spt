jQuery(function(){
		var btnUpload=jQuery('#upload');
		var status=jQuery('#system-message-container');
		new AjaxUpload(btnUpload, {
			action: '/modules/mod_add_ads/upload/upload-file.php?upload=1&avatar=1&user_id=<?=$user_id?>&hash=<?=md5($user_id)?>',
			name: 'uploadfile',
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|png|jpeg)$/.test(ext))){ 
                    // extension is not allowed 
					status.text('загружать можно только JPG, PNG, вес не более '+$max_file_avatar_size+' Кб, не более '+$max_avatar_width+' px');
					return false;
				}
				status.text('Загрузка...');
			},
			onComplete: function(file, response){
				//On completion clear the status
				status.text('');
                response = response.split('##');
				//Add uploaded file to list
				if(response[0]==="success"){
                    jQuery('input[name="ad_avatar"]').val(response[3]);
                    jQuery('span#avatar_img').html('<img src="/images/com_ads/avatars/<?=$user_id?>/'+response[3]+'" width="40" />');
				} else{
                    status.html(response[0]+response[1]);
				}
			}
		});
		
	});